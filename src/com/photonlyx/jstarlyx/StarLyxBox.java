package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Object3DFrame;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;

public class StarLyxBox extends Surface
{
	private double x,y,z,dx=1,dy=1,dz=1;

	public StarLyxBox(String path)
	{
		super(path);
		this.setName("box0");
		this.setObjfile("box0.obj");
	}
	
	public StarLyxBox(Node father,String path)
	{
		super(father,path);
		this.setName("box0");
		this.setObjfile("box0.obj");
	}
	
	
	public  Object3DFrame getObject3DFrame()
	{
	return new Box(new Vecteur(x,y,z),dx,dy,dz);
	}

	public void save2OBJfile()
	{
		Box boxgeom=new Box(new Vecteur(x,y,z),dx,dy,dz);
		TriangleMesh ts=boxgeom.getTriangles();
		TextFiles.saveString(this.getObjfile(), ts.saveToOBJ());
	}

	public XMLElement toXML()
	{
		XMLElement el=super.toXML();

		save2OBJfile();

		return el;

	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	
	
	public double getDx() {
		return dx;
	}

	public void setDx(double x) {
		this.dx = x;
	}

	public double getDy() {
		return dy;
	}

	public void setDy(double y) {
		this.dy = y;
	}

	public double getDz() {
		return dz;
	}

	public void setDz(double z) {
		this.dz = z;
	}

}
