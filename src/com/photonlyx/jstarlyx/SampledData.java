package com.photonlyx.jstarlyx;



import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

public class SampledData  extends Node implements XMLstorable
{
	private String data;

	public SampledData()
	{
	}

	public SampledData(Node father)
	{
		super(father);
	}

	public SampledData(String name)
	{
		this.setName(name);
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}



	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("sampled_data");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("DATA", data);
		return el;
	}



	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
		data=xml.getStringAttribute("DATA");
	}



}
