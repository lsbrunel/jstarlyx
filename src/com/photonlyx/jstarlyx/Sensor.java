package com.photonlyx.jstarlyx;

import java.awt.Color;
import java.io.File;
import java.util.Enumeration;

import com.photonlyx.jstarlyx.gui.JstarlyxApp;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.ConeMesh;
import com.photonlyx.toolbox.math.geometry.CylinderMesh;
import com.photonlyx.toolbox.math.geometry.DiskMesh;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.gui.Text3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;

/**
 * sensor use for inverse path algorithm
 * @author laurent
 *
 */
public class Sensor extends Node implements XMLstorable
{
	private Vecteur pos=new Vecteur(0,0,0); //(when surface not defined by a file)
	private Vecteur viewPoint=new Vecteur(0,0,0); //(when surface not defined by a file)
	private double diameter=0;//diameter mm of the disk (when surface not defined by a file)
	private double angle=0;//angle of acceptance around the normal, rad
	private String filters="filterR filterG filterB";//name of the output file of starlyx
	private String volume="World";//volume name where is placed the sensor
	private String file=null;//mesh file if sensor is made by a surface
	private static Color c=Color.BLUE;
	private TriangleMeshFrame mesh=new TriangleMeshFrame();
	private String path;//path to the mesh file if the sensor is not a disk


	public Sensor(String path)
	{
		this.path=path;
		this.setName("sensor0");
	}

	public Sensor(Node father,String path)
	{
		super(father);
		this.path=path;
		this.setName("sensor0");
	}



	private void readMeshFile()
	{
		if (file.endsWith(".obj"))
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+file);
			mesh.readOBJ(sb.toString());
		}
		else
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+file);
			mesh.readSTL(sb.toString());
			//mesh.readSTLinBinaryFile(Global.path, file);
		}
	}


	@Override
	public Object3DColorSet getObject3D()
	{
		Object3DColorSet set=new Object3DColorSet();
		if (file.length()!=0)
		{
			this.readMeshFile();
			Object3DColorSet setc=new Object3DColorSet();		
			for (Triangle3D t:mesh.getTriangles()) 
			{
				Triangle3DColor tc=new Triangle3DColor(t,c,true);
				set.add(tc);
			}
			return set;
		}
		else
		{
			Scene scene=(Scene)this.getAncestorOfClass("Scene");
			double sceneSize=scene.getSceneSize();
			Vecteur dir=viewPoint.sub(pos).getNormalised();
			TriangleMeshFrame d=getBody();
			//the cone:
			if (angle<180)
			{
				double hcone,dcone;
				if (angle<90)
				{
					hcone=sceneSize/10;
					dcone=2*hcone*Math.tan(angle/2*Math.PI/180);
				}
				else
				{
					dcone=sceneSize/10;
					hcone=dcone/(2*Math.tan(angle/2*Math.PI/180));				
				}
				ConeMesh cone=new ConeMesh(dcone,hcone,20);//represents the probe
				cone.getFrame().buildSuchThatZaxisIs(dir.scmul(-1));
				cone.getFrame()._translate(pos);
				cone.getFrame()._translate(cone.getFrame().z().scmul(-hcone));
				cone.updateGlobal();	
				set.add(new Object3DColorInstance(cone,c));
			}
			set.add(new Object3DColorInstance(d,c));
			set.add(new Point3DColor(pos,c,5));
			set.add(new Segment3DColor(pos,pos.addn(d.getFrame().getAxis(2)),c));

			Text3DColor t=new Text3DColor(this.getName(),pos.addn(d.getFrame().getAxis(2).scmul(-0.3)),c);
			set.add(t);
		}

		return set;
	}

	private TriangleMeshFrame getBody()
	{
		DiskMesh d=new DiskMesh(diameter,20);//represents the probe
		d.getFrame().getCentre().copy(pos);
		d.updateGlobal();
		d.getFrame().rotateSuchZpoints(viewPoint);
		d.updateGlobal();
		return d;
	}

	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("sensor");

		el.setAttribute("name", this.getName());
		el.setDoubleAttribute("ANGLE", angle);
		el.setAttribute("filters", filters);
		el.setAttribute("volume", volume);
		if (file!=null)
			el.setAttribute("FILE", file);
		else
		{
			el.setDoubleAttribute("DIAMETER", diameter);
			//add pos:
			XMLElement elLens=pos.toXML();
			elLens.setName("pos");
			el.addChild(elLens);
			//add view point:
			XMLElement elViewPoint=viewPoint.toXML();
			elViewPoint.setName("viewPoint");
			el.addChild(elViewPoint);
		}
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		diameter=xml.getDoubleAttribute("DIAMETER",0);
		angle=xml.getDoubleAttribute("angle",0);
		filters=xml.getStringAttribute("filters");
		volume=xml.getStringAttribute("volume","World");
		file=xml.getStringAttribute("file","");

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("pos"))
			{
				pos.updateFromXML(el);
			}
			if (el.getName().contains("viewPoint"))
			{
				viewPoint.updateFromXML(el);
			}

		}
	}



	public Vecteur getPos()
	{
		return pos;
	}

	public Vecteur getViewPoint()
	{
		return viewPoint;
	}
	public double getX() {
		return pos.x();
	}

	public void setX(double x) {
		pos.setX( x);
	}

	public double getY() {
		return pos.y();
	}

	public void setY(double y) {
		pos.setY (y);
	}

	public double getZ() {
		return pos.z();
	}

	public void setZ(double z) {
		pos.setZ (z);
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public void setPos(Vecteur pos) {
		this.pos = pos;
	}


	public void setViewPoint(Vecteur viewPoint) {
		this.viewPoint = viewPoint;
	}

	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (int k=0;k<3;k++) 
		{
			double r=pos.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}			
		getBody().checkOccupyingCube(cornerMin, cornerMax);


	}

	//	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	//	{
	//		for (int k=0;k<3;k++) 
	//		{
	//			double r=pos.coord(k);
	//			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
	//			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
	//		}
	//		
	//	}
	//




	public static void main(String[] args)
	{
		JstarlyxApp app =new JstarlyxApp();
		//	Global.path="./";

		//remove all:
		app.getScene().free();

		//add sensor
		Sensor p=new Sensor("/home/laurent/temp");
		p.setPos(new Vecteur(-5,0,0));
		p.setViewPoint(new Vecteur());
		p.setDiameter(1);
		p.setAngle(0);
		app.getScene().add(p);


		app.updateParamPanel();
		app.update();
		//app.getPanel3D() .update3D();
		app.getPanelXML().setText(app.getScene().getStringXML());
	}






}
