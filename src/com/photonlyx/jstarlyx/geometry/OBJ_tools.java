package com.photonlyx.jstarlyx.geometry;

import java.io.File;

import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;

public class OBJ_tools 
{
/**
 * Place a layer on a plate. The layer is towards x<0 (in front of the BS sphere)
 * 
 * @param path directory where are stored the scene and geometry
 * @param x x position of the interface between glass and layer
 * @param layerThickness thickness of the layer
 * @param plateThickness thickness of the plate
 * @param platey size of the plate in y axis
 * @param platez size of the plate in z axis
 * @param layery size of the layer in y axis
 * @param layerz size of the layer in z axis
 */
	public static void createLayerOnPlateObjFiles2(String path,double x,double layerThickness,double plateThickness,
			double platey,double platez,double layery,double layerz)
	{
//		double platey=150;//mm
//		double platez=100;//mm
//		double layery=100;
//		double layerz=40;
		Vecteur p1,p2,p3,p4;

		//create the product-air surface:
		//create open box:
		Object3DSet set1=new Object3DSet();
		//add front:
		addFront(set1,x-layerThickness,layery,layerz);
		//add sides:
		addSides(set1,x-layerThickness,x,layery,layerz);
		//save to file:
		String s1=set1.transformToTriangles().saveToOBJ();
		TextFiles.saveString(path+File.separator+"air_product.obj",s1,false);

		//create the product-glass surface:
		Object3DSet set2=new Object3DSet();
		//add front:
		addFront(set2,x,layery,layerz);
		//save to file:
		String s2=set2.transformToTriangles().saveToOBJ();
		TextFiles.saveString(path+File.separator+"product_glass.obj",s2,false);

		//create the glass-air surface:
		Object3DSet set3=new Object3DSet();
		//add sides:
		addSides(set3,x,x+plateThickness,platey,platez);
		//add back:
		addFront(set3,x+plateThickness,platey,platez);
		// front side top
		p1=new Vecteur(x,platey/2,platez/2);
		p2=new Vecteur(x,platey/2,layerz/2);
		p3=new Vecteur(x,-platey/2,layerz/2);
		p4=new Vecteur(x,-platey/2,platez/2);
		set3.add(new Quadrilatere3D(p1,p2,p3,p4));
		// front side down
		p1=new Vecteur(x,platey/2,-platez/2);
		p2=new Vecteur(x,platey/2,-layerz/2);
		p3=new Vecteur(x,-platey/2,-layerz/2);
		p4=new Vecteur(x,-platey/2,-platez/2);
		set3.add(new Quadrilatere3D(p1,p2,p3,p4));
		// front side left
		p1=new Vecteur(x,-platey/2,layerz/2);
		p2=new Vecteur(x,-platey/2,-layerz/2);
		p3=new Vecteur(x,-layery/2,-layerz/2);
		p4=new Vecteur(x,-layery/2,layerz/2);
		set3.add(new Quadrilatere3D(p1,p2,p3,p4));
		// front side right
		p1=new Vecteur(x,platey/2,layerz/2);
		p2=new Vecteur(x,platey/2,-layerz/2);
		p3=new Vecteur(x,layery/2,-layerz/2);
		p4=new Vecteur(x,layery/2,layerz/2);
		set3.add(new Quadrilatere3D(p1,p2,p3,p4));
		//save to file:
		String s3=set3.transformToTriangles().saveToOBJ();
		TextFiles.saveString(path+File.separator+"glass_air.obj",s3,false);



	}
	
	
	

	public static void addFront(Object3DSet set1,double x,double dy,double dz)
	{
		Vecteur p1,p2,p3,p4;
		//front quad
		p1=new Vecteur(x,dy/2,dz/2);
		p2=new Vecteur(x,dy/2,-dz/2);
		p3=new Vecteur(x,-dy/2,-dz/2);
		p4=new Vecteur(x,-dy/2,dz/2);
		set1.add(new Quadrilatere3D(p1,p2,p3,p4));		
	}

	public static void addSides(Object3DSet set1,double x1,double x2,double dy,double dz)
	{
		Vecteur p1,p2,p3,p4;
		//top quad
		p1=new Vecteur(x1,dy/2,dz/2);
		p2=new Vecteur(x1,-dy/2,dz/2);
		p3=new Vecteur(x2,-dy/2,dz/2);
		p4=new Vecteur(x2,dy/2,dz/2);
		set1.add(new Quadrilatere3D(p1,p2,p3,p4));
		//down quad
		p1=new Vecteur(x1,dy/2,-dz/2);
		p2=new Vecteur(x1,-dy/2,-dz/2);
		p3=new Vecteur(x2,-dy/2,-dz/2);
		p4=new Vecteur(x2,dy/2,-dz/2);
		set1.add(new Quadrilatere3D(p1,p2,p3,p4));
		//side left quad
		p1=new Vecteur(x1,-dy/2,dz/2);
		p2=new Vecteur(x1,-dy/2,-dz/2);
		p3=new Vecteur(x2,-dy/2,-dz/2);
		p4=new Vecteur(x2,-dy/2,dz/2);
		set1.add(new Quadrilatere3D(p1,p2,p3,p4));
		//side right quad
		p1=new Vecteur(x1,dy/2,dz/2);
		p2=new Vecteur(x1,dy/2,-dz/2);
		p3=new Vecteur(x2,dy/2,-dz/2);
		p4=new Vecteur(x2,dy/2,dz/2);
		set1.add(new Quadrilatere3D(p1,p2,p3,p4));
	}

}
