package com.photonlyx.jstarlyx;

import java.util.Iterator;
import java.util.Vector;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;

import nanoxml.XMLElement;

/**
 * node of a tree of system objets
 * @author laurent
 *
 */
public   class Node implements XMLstorable, Iterable<Node>
{
	private Vector<Node> sons=new Vector<Node>();
	private Node father;
	private String name="";


	public Iterator<Node> iterator(){return  sons.iterator();}

	public  Object3DColorSet getObject3D()
	{
		return null;
	}

	public Node()
	{

	}

	public Node(Node father)
	{
		setFather(father);
	}

	protected void setFather(Node n)
	{
		father=n;
	}

	protected Node getFather()
	{
		return father;
	}


	public Vector<Node> getSons()
	{
		return sons;
	}

	public void add(Node n)
	{
		sons.add(n);
		n.setFather(this);
	}


	protected void addSon(Node n)
	{
		sons.add(n);
	}


	public Node getNodeOfClassContaining(String type)
	{
		for (Node n:sons)
		{
			if (n.getClass().toString().contains(type)) return n;
		}
		return null;
	}

	public Vector<Node> getNodesOfClass(String classNamePart)
	{
		Vector<Node> list=new Vector<Node>();
		for (Node n:sons)
		{
			if (n.getClass().toString().contains(classNamePart)) list.add(n);
		}
		return list;
	}


	/**
	 * recursively get the ancestor of a given type
	 * @param type
	 * @return
	 */
	public Node getAncestorOfClass(String type)
	{
		Node father=this.getFather();
		if (father==null) return null;
		else if (father.getClass().toString().contains(type)) return father;
		else return father.getAncestorOfClass(type);
	}


	/**
	 * free the tree of nodes:
	 */
	public void free()
	{
		for (Node n:sons) n.free();
		sons.removeAllElements();
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("node");
		for (Node n:this.getSons()) 
		{
			el.addChild(n.toXML());
		}
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		this.free();
		if (xml==null) 
		{
			return;
		}

	}

	public void removeSon(Node n)
	{
		sons.remove(n);
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
	}

}
