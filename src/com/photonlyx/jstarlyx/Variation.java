package com.photonlyx.jstarlyx;

import nanoxml.XMLElement;

public class Variation extends Node
{
	private	String name="variation";
	private	String  object="";//name of the object having the attribute to vary
	private	String  attribute="";//name of the object attribute to vary
	private	double min=0.1;//min value of the attribute
	private	double max=10;//max value of the attribute
	private	int log=0;//if 0 vary attribute linearly, if 1 vary in log
	private	int dim=10;//nb of MC calculations
	private	double lambda=650;//wavelength

	public Variation()
	{
		super(null);
	}
	public Variation(Node n)
	{
		super(n);
	}
	
	
	

	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Variation");
		el.setAttribute("NAME", name);
		el.setAttribute("OBJECT", object);
		el.setAttribute("ATTRIBUTE", attribute);
		el.setDoubleAttribute("MIN", min);
		el.setDoubleAttribute("MAX", max);
		el.setIntAttribute("LOG", log);
		el.setIntAttribute("DIM", dim);
		el.setDoubleAttribute("LAMBDA", lambda);
		for (Node n:this.getSons()) 
		{
			el.addChild(n.toXML());
		}
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		this.free();
		if (xml==null) 
		{
			return;
		}
		name=xml.getStringAttribute("NAME", "");
		object=xml.getStringAttribute("OBJECT", "");
		attribute=xml.getStringAttribute("ATTRIBUTE", "");
		min=xml.getDoubleAttribute("MIN", 0.1);
		max=xml.getDoubleAttribute("MAX", 10);
		log=xml.getIntAttribute("LOG", 0);
		dim=xml.getIntAttribute("DIM", 10);
		lambda=xml.getDoubleAttribute("LAMBDA", 650);

	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public String getAttribute() {
		return attribute;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public double getMin() {
		return min;
	}
	public void setMin(double min) {
		this.min = min;
	}
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
	public int getLog() {
		return log;
	}
	public void setLog(int log) {
		this.log = log;
	}
	public int getDim() {
		return dim;
	}
	public void setDim(int dim) {
		this.dim = dim;
	}
	public double getLambda() {
		return lambda;
	}
	public void setLambda(double lambda) {
		this.lambda = lambda;
	}
	
	
	

}
