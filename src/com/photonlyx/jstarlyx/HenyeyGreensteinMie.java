package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

/**
 * scatterer defined by Henyey-Greenstein phase function and spherical particle size , volume fraction and refractive index
 * @author laurent
 *
 */
public class HenyeyGreensteinMie extends Node implements XMLstorable
{
	private String phi="0.01";//volume fraction (not in %)
	private String d_um="1";//sphere diameter (in um)
	private String nr="1.5";//real part of refractive index spectrum of the spherical particle
	private String ni="0";//imaginary part of refractive index spectrum of the spherical particle
	private String sigma_d_um="0";//polydispersity
	private String distrib_shape="normal";//shape of the size distribution curve, possible values are: "normal" (gauss) or "log normal"

	public HenyeyGreensteinMie()
	{
	}
	public HenyeyGreensteinMie(Node father)
	{
		super(father);
	}

	public HenyeyGreensteinMie(String name)
	{
		this.setName(name);
	}





	public String getPhi() {
		return phi;
	}
	public void setPhi(String phi) {
		this.phi = phi;
	}

	public String getD_um() {
		return d_um;
	}
	public void setD_um(String d_um) {
		this.d_um = d_um;
	}
	public String getNr() {
		return nr;
	}
	public void setNr(String n) {
		this.nr = n;
	}
	public String getNi() {
		return ni;
	}
	public void setNi(String n) {
		this.ni = n;
	}
	public String getSigma_d_um() {
		return sigma_d_um;
	}
	public void setSigma_d_um(String sigma_d_um) {
		this.sigma_d_um = sigma_d_um;
	}
	public String getDistrib_shape() {
		return distrib_shape;
	}
	public void setDistrib_shape(String distrib_shape) {
		this.distrib_shape = distrib_shape;
	}
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Henyey-Greenstein");
		el.setAttribute("name", this.getName());
		el.setAttribute("phi", phi);
		el.setAttribute("d_um", d_um);
		el.setAttribute("nr", nr);
		el.setAttribute("ni", ni);
		el.setAttribute("sigma_d_um", sigma_d_um);
		el.setAttribute("distrib_shape", distrib_shape);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		this.setPhi(xml.getStringAttribute("phi",""));
		this.setD_um(xml.getStringAttribute("d_um",""));
		this.setNr(xml.getStringAttribute("nr","1.33"));
		this.setNi(xml.getStringAttribute("ni","0"));
		this.setSigma_d_um(xml.getStringAttribute("sigma_d_um",""));
		this.setDistrib_shape(xml.getStringAttribute("distrib_shape",""));
	}






}
