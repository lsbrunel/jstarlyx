package com.photonlyx.jstarlyx;


import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

public class Dielectric extends Node implements XMLstorable
{

	public Dielectric()
	{
	}
	public Dielectric(Node father)
	{
		super(father);
	}

	public Dielectric(String name)
	{
		this.setName(name);
	}

	
	
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("dielectric");
		el.setAttribute("NAME", this.getName());
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
	}






}
