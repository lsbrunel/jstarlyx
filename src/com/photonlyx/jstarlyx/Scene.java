package com.photonlyx.jstarlyx;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;

import com.photonlyx.jstarlyx.gui.JstarlyxApp;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.thread.CTask2;
import com.photonlyx.toolbox.thread.TaskEndListener;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import com.photonlyx.toolbox.util.Util;

import nanoxml.XMLElement;

public class Scene extends Node  implements CTask2 //, TaskEndListener
{
	//private boolean getPaths=false;
	private int nbPhotons=10000;
	//private int directPath=1;//type of MC algorithm
	private String algorithm;
	public String[] algorithmOptions= {"direct","reverse"};
	private int verbose=1;//verbose option of starlyx
	private int storeTrajectories=0;//starlyx option to store trajectories
	private int storeErrorTrajectories=0;//starlyx option to store error trajectories
	private int nbThreads=1;
	private String path=".";
	private String fileName="scene.xml";
	//private int max_path_length=1000000000;
	//private int max_nb_scattering=-1;
	private int max_nb_events=1000000;

	//symbolic Monte-Carlo:
	private double kHat=-1;//mm-1 scattering coefficient used for null collision in symbolic MC, put -1 for normal MC
	private double pScat=0.33;//fixed probability to sort scattering event, used for symbolic MC
	private double pAbs=0.33;//fixed probability to sort absorption event, used for symbolic MC
	private static Process p;//process of starlyx
	private boolean stopped=true;
	private TaskEndListener taskEndListener;//used by Task2 button

	private TriggerList trig_starlyx_thread=null; //used for starlyx thread 

	public Scene()
	{
		super(null);
	}

	public Scene(Node n)
	{
		super(n);
	}
	public Scene(String path)
	{
		super(null);
		this.path=path;
	}

	public Scene(Node n,String path)
	{
		super(n);
		this.path=path;
	}


	public int getNbPhotons()
	{
		return nbPhotons;
	}
	public void setNbPhotons(int nbPhotons)
	{
		this.nbPhotons = nbPhotons;
	}


	public void setDirectPath(int i) 
	{
		if (i==1) this.algorithm = "direct";
		else this.algorithm = "reverse";	
	}		

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}


	public int getVerbose() {
		return verbose;
	}


	public void setVerbose(int verbose) {
		this.verbose = verbose;
	}


	public int getStoreTrajectories() {
		return storeTrajectories;
	}


	public void setStoreTrajectories(int storeTrajectories) {
		this.storeTrajectories = storeTrajectories;
	}


	public int getStoreErrorTrajectories() {
		return storeErrorTrajectories;
	}

	public void setStoreErrorTrajectories(int storeErrorTrajectories) {
		this.storeErrorTrajectories = storeErrorTrajectories;
	}

	public int getNbThreads() {
		return nbThreads;
	}

	public void setNbThreads(int nbThreads) {
		this.nbThreads = nbThreads;
	}


	//
	//	public int getMax_path_length() {
	//		return max_path_length;
	//	}
	//
	//	public void setMax_path_length(int max_path_length) {
	//		this.max_path_length = max_path_length;
	//	}


//	public int getMax_nb_scattering() {
//		return max_nb_scattering;
//	}
//
//	public void setMax_nb_scattering(int max_nb_scattering) {
//		this.max_nb_scattering = max_nb_scattering;
//	}

	public int getMax_nb_events() {
		return max_nb_events;
	}

	public void setMax_nb_events(int max_nb_events) {
		this.max_nb_events = max_nb_events;
	}

	public double getkHat() {
		return kHat;
	}

	public void setkHat(double kHat) {
		this.kHat = kHat;
	}

	public double getpScat() {
		return pScat;
	}

	public void setpScat(double pScat) {
		this.pScat = pScat;
	}

	public double getpAbs() {
		return pAbs;
	}

	public void setpAbs(double pAbs) {
		this.pAbs = pAbs;
	}

	//	public void whenTaskFinished()
	//	{
	//		if (p!=null) 
	//		{
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	//		System.out.println("3333333333333333333333333333333333333333333333333333333333333333333");
	////		System.out.println("kill "+p.pid());
	////		Util.runCommand("kill "+p.pid()); 
	//		}
	//		if (p!=null) p.destroy();
	//	}

	public Process runStarlyx()
	{
		p=runCommand(JstarlyxApp.starlyxPath+File.separator+"starlyx "+this.getPath()+File.separator+this.getFileName());
		return p;
	}

	public static Process runStarlyx(String path,String file)
	{
		p=Util.runCommand(JstarlyxApp.starlyxPath+"starlyx "+path+File.separator+file);
		return p;
	}
	//	
	//	public  Process saveAndRunStarlyx()
	//	{
	//		String xml=XMLFileStorage.toStringWithIndentation(this.toXML());
	//		TextFiles.saveString(path+File.separator+fileName,xml);
	//		p=runCommand(JstarlyxApp.starlyxPath+"starlyx "+path+File.separator+fileName);
	//		return p;
	//	}

	public void runStarlyx2()
	{
		runCommand2("starlyx",getFileName());
	}

	public void save()
	{
		save(this.getPath(),this.getFileName());
	}

	public void save(String path,String filename)
	{
		String xml=XMLFileStorage.toStringWithIndentation(this.toXML());
		TextFiles.saveString(path+File.separator+filename,xml,false);
	}

	public String getStringXML()
	{
		XMLElement el = this.toXML();
		StringBuffer sb=new StringBuffer();
		sb.append("<?xml version=\"1.0\"?> \n");
		XMLFileStorage.toStringWithIndentation(el,sb,"");
		return sb.toString();
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (Node n:this) n.updateBoundariesVolume(cornerMin, cornerMax);
	}

	public double getSceneSize()
	{
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		this.updateBoundariesVolume(cornerMin,cornerMax);
		double sceneSize=cornerMin.sub(cornerMax).norme();
		return sceneSize;
	}

	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Scene");
//		if (algorithm.contains("reverse")) 
//			el.setAttribute("DIRECT_PATH",0);
//		else el.setAttribute("DIRECT_PATH",1);
		el.setAttribute("ALGORITHM", algorithm);
		el.setIntAttribute("VERBOSE", verbose);
		el.setIntAttribute("NB_PHOTONS", nbPhotons);
		el.setIntAttribute("STORE_TRAJECTORIES", storeTrajectories);
		el.setIntAttribute("STORE_ERROR_TRAJECTORIES", storeErrorTrajectories);
		el.setIntAttribute("NB_THREADS", nbThreads);
		//el.setIntAttribute("MAX_PATH_LENGTH", max_path_length);
		//el.setIntAttribute("MAX_NB_SCATTERING", max_nb_scattering);
		el.setIntAttribute("MAX_NB_EVENTS", max_nb_events);

		if (this.getkHat()>0)
		{
			el.setDoubleAttribute("K_HAT", this.getkHat());
			el.setDoubleAttribute("P_SCAT", this.getpScat());
			el.setDoubleAttribute("P_ABS", this.getpAbs());
		}
		for (Node n:this.getSons()) 
		{
			el.addChild(n.toXML());
		}
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		this.free();
		if (xml==null) 
		{
			return;
		}
		algorithm=xml.getStringAttribute("ALGORITHM","reverse_1");
		verbose=xml.getIntAttribute("VERBOSE", 1);
		nbPhotons=xml.getIntAttribute("NB_PHOTONS", 1000);
		storeTrajectories=xml.getIntAttribute("STORE_TRAJECTORIES", 0);	
		storeErrorTrajectories=xml.getIntAttribute("STORE_ERROR_TRAJECTORIES", 0);	
		nbThreads=xml.getIntAttribute("NB_THREADS", 0);	
		//this.max_path_length=xml.getIntAttribute("MAX_PATH_LENGTH", 10000);	
		//this.max_nb_scattering=xml.getIntAttribute("MAX_NB_SCATTERING",-1);	
		this.max_nb_events=xml.getIntAttribute("MAX_NB_EVENTS",1000000);	
		this.kHat=xml.getDoubleAttribute("K_HAT", -1);	
		this.pScat=xml.getDoubleAttribute("P_SCAT", 0.33);	
		this.pAbs=xml.getDoubleAttribute("P_ABS", 0.33);	
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			Node n=null;
			if (el.getName().contains("spectrum")) n=new Spectrum(this);	
			if (el.getName().contains("volume")) n=new Volume(this);	
			if (el.getName().contains("surface")) n=new Surface(this,path);	
			if (el.getName().contains("openscad")) n=new SurfaceOpenscad(this,path);	
			if (el.getName().contains("camera")) n=new Camera(this);	
			if (el.getName().contains("source")) 
			{
				if (el.getStringAttribute("TYPE", "").compareTo("sun")==0) 		n=new SourceSun(this);	
				if (el.getStringAttribute("TYPE", "").compareTo("point")==0) 	n=new SourcePoint(this);	
				if (el.getStringAttribute("TYPE", "").compareTo("laser")==0) 	n=new SourceLaser(this);	
				if (el.getStringAttribute("TYPE", "").compareTo("spot")==0) 	n=new SourceSpot(this);	
				if (el.getStringAttribute("TYPE", "").compareTo("surface")==0) 	n=new SourceSurface(this,path);	
			}
			if (el.getName().contains("Henyey-Greenstein")) 
			{
				if (el.getStringAttribute("K")!=null) n=new HenyeyGreensteinK(this);	
				else if (el.getStringAttribute("LSTAR")!=null) n=new HenyeyGreensteinLstar(this);	
				else if (el.getStringAttribute("D_UM")!=null) n=new HenyeyGreensteinMie(this);			
			}
			if (el.getName().equals("Mie")) n=new Mie(this);	
			//if (el.getName().contains("material_surface")) n=new MaterialSurface(this);	
			if (el.getName().equals("lambert")) n=new Lambert(this);	
			if (el.getName().equals("dielectric")) n=new Dielectric(this);	
			if (el.getName().equals("mirror")) n=new Mirror(this);	
			if (el.getName().contains("sensor")) n=new Sensor(this,path);	
			if (el.getName().contains("probe")) n=new Sensor(this,path);	
			if (el.getName().contains("grating")) n=new Grating(this);	
			if (n!=null)
			{
				n.updateFromXML(el);
				this.addSon(n);
			}
		}
	}


	public Vector<Volume> getObjectsList() {

		Vector<Volume> list=new Vector<Volume>();
		for (Node n:this.getSons()) 
		{
			if (n instanceof Volume) list.add((Volume)n);
		}

		return list;
	}

	public Node getNodeOfName(String s) 
	{
		for (Node n:this.getSons()) 
		{
			//if (n instanceof StarLyxObject) 
			if (n.getName().compareTo(s)==0) return n;
		}
		return null;
	}


	public String findNewNameWithRoot(String root)
	{
		int c=0;
		while (getNodeOfName(root+c)!=null) c++;
		return root+c;
	}


	/**
	 * set a new minimal scene
	 */
	public void setMinimalScene()
	{
		Scene scene=this;

		scene.free();

		//add the spectra of the camera filters:
		Spectrum sourceSpectrum=new Spectrum("white");
		sourceSpectrum.setData(" 450    1     530   1    650  1 ");
		scene.add(sourceSpectrum);

		SourceSun sun0=new SourceSun();
		sun0.setSpectrum("white");
		scene.add(sun0);

		//os.add(new SourcePoint());

		Camera cam=new Camera();
		cam.setFilters(" filterR filterG filterB  ");
		cam.setField(30);
		cam.setViewX(0.5);
		cam.setViewY(0.5);
		cam.setViewZ(0.5);
		scene.add(cam);

		Volume vol1=new Volume();
		vol1.setName("vol1");
		vol1.setSurfaces("box0");
		vol1.setMaterials("hg0");
		scene.add(vol1);


		//add the spectra of the camera filters:
		Spectrum filterR=new Spectrum("filterR");
		filterR.setData(" 650 1 ");
		scene.add(filterR);
		Spectrum filterG=new Spectrum("filterG");
		filterG.setData(" 530 1 ");
		scene.add(filterG);
		Spectrum filterB=new Spectrum("filterB");
		filterB.setData(" 450 1 ");
		scene.add(filterB);

		//add material_volume:
		HenyeyGreensteinK mv=new HenyeyGreensteinK("hg0");
		mv.setKs("spectrum_k");
		mv.setKa("spectrum_ka");
		mv.setG("spectrum_g");
		scene.add(mv);
		//add spectra of material volume:
		Spectrum k=new Spectrum("spectrum_k");
		k.setData(" 450 10 530 10 650 10 ");
		scene.add(k);
		Spectrum ka=new Spectrum("spectrum_ka");
		ka.setData(" 450 0 530 0 650 0 ");
		scene.add(ka);
		Spectrum g=new Spectrum("spectrum_g");
		g.setData(" 450 0 530 0 650 0");
		scene.add(g);
		Spectrum n=new Spectrum("spectrum_n");
		n.setData(" 450 1.4 530 1.4 650 1.4 ");
		scene.add(n);
	}


	//	public static boolean runCommand(String com) 
	//	{
	//		String s = null;
	//		int index=com.indexOf("starlyx ");
	//		String path=com.substring(index+8, com.length()-1);
	//		if (path.contains(" ")) 
	//		{
	//			Messager.messErr("starlyx command: scene path contains a space character: "+path);
	//			return false;
	//		}
	//		//System.out.println("Run the command: "+com+" \n");
	//
	//		try 
	//		{
	//			Process p = Runtime.getRuntime().exec(com);
	//			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
	//			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
	//			// read the output from the command  
	//			//System.out.println("Standard output:");
	//			while ((s = stdInput.readLine()) != null) 
	//			{
	//				System.out.println(s);
	//				//Global.setMessage(s);
	//			}
	//			// read any errors from the attempted command
	//			//System.out.println("Standard error:");
	//			while ((s = stdError.readLine()) != null) 
	//			{
	//				System.out.println(s);
	//			}
	//			while(p.isAlive()) 
	//			{
	//				//System.out.println("Wait 200ms");
	//				try {Thread.sleep(200);} catch(Exception e){;}
	//			}
	//		}
	//		catch (IOException e) 
	//		{
	//			System.out.println("exception happened - here's what I know: ");
	//			e.printStackTrace();
	//		}
	//		return true;
	//
	//
	//	}


	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setFileName(String s) {
		fileName=s;

	}


	public String getFileName() {
		return fileName;
	}

	//	public Object3DColorSet getObject3D()
	//	{
	//		Object3DColorSet set=new Object3DColorSet();
	//		Frame f=new Frame();
	//		set.add(new Object3DColorInstance(f,Color.BLACK));
	//		return set;
	//	}


	public  Process runCommand(String com) 
	{
		String s = null;
		int index=com.indexOf("starlyx ");
		String path=com.substring(index+8, com.length());
		if (path.contains(" ")) 
		{
			Messager.messErr("starlyx command: scene path contains a space character: "+path);
			return null;
		}
		System.out.println("path: "+path);
		System.out.println("Run the command: "+com);

		File f_render=null;
		Camera cam=(Camera)this.getNodeOfClassContaining("Camera");
		if (cam!=null)
		{
			f_render=new File(this.path+File.separator+cam.getName()+".png");
		}
		//if (f_render.exists())
		{
			if (trig_starlyx_thread!=null) trig_starlyx_thread.trig();
		}
		Process p =null;
		try 
		{
			p = Runtime.getRuntime().exec(com);
			//Global.setMessage("Running starlyx on process of id:"+p.pid());
			Global.setMessage("Running starlyx on process ");
			//System.out.println("Running starlyx on process ");
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
			// read the output from the command  
			//System.out.println("Standard output:");
			while ((s = stdInput.readLine()) != null) 
			{
				System.out.println("Starlyx: "+s);
				Global.setMessage("Starlyx: "+s);
				if (stopped)
				{
					p.destroy();
					//Global.setMessage("Process id:"+p.pid()+" ended by user");
					Global.setMessage("Process ended by user");
					return p;
				}
				if (f_render!=null) if (f_render.exists())
				{
					if ((new Date().getTime()-f_render.lastModified())<1000) 
					{
						if (trig_starlyx_thread!=null) trig_starlyx_thread.trig();
					}
				}
			}
			// read any errors from the attempted command
			System.out.println("Standard error:");
			while ((s = stdError.readLine()) != null) 
			{
				System.out.println(s);
			}
			while(p.isAlive()) 
			{
				//System.out.println("Wait 200ms");
				try {Thread.sleep(200);} catch(Exception e){;}
			}
		}
		catch (IOException e) 
		{
			System.out.println("exception happened - here's what I know: ");
			Messager.messErr(e.toString());
			e.printStackTrace();
			return null;
		}
		if (taskEndListener!=null) taskEndListener.whenTaskFinished();
		//Global.setMessage("Process id:"+p.pid()+" ended");
		Global.setMessage("Process ended");
		return p;
	}


	public  void runCommand2(String cmd1,String cmd2) 
	{
		//in case of rendering , prepare to update the image
		File f_render=null;
		Camera cam=(Camera)this.getNodeOfClassContaining("Camera");
		if (cam!=null)
		{
			f_render=new File(this.path+File.separator+cam.getName()+".png");
		}
		//if (f_render.exists())
		{
			if (trig_starlyx_thread!=null) trig_starlyx_thread.trig();
		}

		
		System.out.println("run command: "+cmd1+" "+cmd2);
		File outFile = new File(path+File.separator+"out.tmp");
		File errFile = new File(path+File.separator+"err.tmp");
		ProcessBuilder pb = new ProcessBuilder(cmd1,cmd2);
		pb.directory(new File(path));
		pb.redirectOutput(outFile);
		pb.redirectError(errFile);
		Process p=null;
		try 
		{
			p = pb.start();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
			return;
		}
		Date d0=new Date();
		for (;;)
		{
			try 
			{
				try {Thread.sleep(500);} catch(Exception e){;}
				//eventually update the rendering  image:
				if (f_render!=null) if (f_render.exists())
				{
					if ((new Date().getTime()-f_render.lastModified())<1000) 
					{
						if (trig_starlyx_thread!=null) trig_starlyx_thread.trig();
					}
				}
				String lastLine=TextFiles.tail2(outFile,1);
//				System.out.println(lastLine);
				Global.setMessage("Starlyx output: "+lastLine);
				//Global.setMessage(cmd1+" "+cmd2+" ruu22unning since: "+(new Date().getTime()-d0.getTime())/1000+" s");
				
				String lastErrorLines=TextFiles.tail2(errFile,10);
				if (lastErrorLines.length()>0)
				{
					Global.setMessage("Error in Starlyx: "+lastErrorLines);
					System.out.println("Error in Starlyx: "+lastErrorLines);
					break;
				}
				
				//p.waitFor();
				try {Thread.sleep(500);} catch(Exception e){;}
				if (!p.isAlive()) 
					{
					System.out.println("starlyx has finished");
					Global.setMessage("starlyx has finished");
					break;
					}
			}
			catch (Exception e) 
			{
				e.printStackTrace();
				return;
			}
		}
	}
	
	


	public void loadFromFile()
	{
		System.out.println(getClass()+" "+"open: "+path+File.separator+fileName);
		StringBuffer sb=TextFiles.readFile(path+File.separator+fileName,false);
		if (sb==null) return;
		String s=sb.toString();
		//System.out.println(s);
		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(s);
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {ex.printStackTrace();}
		if (xml.getName().compareTo("Scene")==0)
		{
			this.updateFromXML(xml);
		}
		else
		{
			Messager.messErr("Main object type is not Scene");
		}
	}

	@Override
	public void run() 
	{
		//p=runStarlyx();
		runStarlyx2();
		taskEndListener.whenTaskFinished();	

	}

	@Override
	public void setStopped(boolean b) 
	{
		stopped=b;	
	}

	@Override
	public boolean isStopped() 
	{
		return stopped;
	}

	@Override
	public void setTaskEndListener(TaskEndListener t) 
	{
		this.taskEndListener=t;	
	}

	public TriggerList getTrig_starlyx_thread() {
		return trig_starlyx_thread;
	}

	public void setTrig_starlyx_thread(TriggerList trig_starlyx_thread) {
		this.trig_starlyx_thread = trig_starlyx_thread;
	}

	@Override
	public void setTriggerListDuring(TriggerList tl) {
		// TODO Auto-generated method stub

	}




}
