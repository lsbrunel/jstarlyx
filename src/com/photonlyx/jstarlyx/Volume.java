package com.photonlyx.jstarlyx;

import nanoxml.XMLElement;

public class Volume extends Node
{
	private String surfaces="";//list of surface names
	private String n="";//name of spectrum of N
	private String materials="";//names of the materials

	public Volume()
	{

	}

	public Volume(Node father)
	{
		super(father);
	}

	public String getN() {
		return n;
	}


	public void setN(String n) {
		this.n = n;
	}


	public String getMaterials() {
		return materials;
	}
	public void setMaterials(String materials) {
		this.materials = materials;
	}


	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("volume");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("N", this.getN());
		el.setAttribute("MATERIALS", materials);
		el.setAttribute("SURFACES",surfaces);

		for (Node n:getSons()) 
		{
			Volume o;
			if (n instanceof Volume) 
			{
				o=(Volume)n;
				el.addChild(o.toXML());
			}
		}

		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
		n=xml.getStringAttribute("N","1");
		materials=xml.getStringAttribute("MATERIALS","");
		surfaces=xml.getStringAttribute("SURFACES");
	}


	public String getSurfaces() {
		return surfaces;
	}

	public void setSurfaces(String s) {
		this.surfaces = s;
	}




}
