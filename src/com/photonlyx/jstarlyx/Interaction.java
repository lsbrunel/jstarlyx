package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.math.geometry.Vecteur;

/**
 * photon interaction in a path
 * @author laurent
 *
 */
public class Interaction 
{
	private float lambda;//wavelength
	private String type;//MAX_DISTANCE_REACHED,MAX_DISTANCE_REACHED,... 
	private Vecteur pos=new Vecteur();//photon position
	private Vecteur dir=new Vecteur();//photon direction
	private float distance;//distance recovered by the photon
	private String surface;
	private String volume;
	private float w;//weight

	/**
	 * construct using a csv line of starlyx export
	 */
	public Interaction(String[] line)
	{
		int index=1;
		lambda=Float.parseFloat(line[index++]);
		type=line[index++];
		pos.setX(Float.parseFloat(line[index++]));
		pos.setY(Float.parseFloat(line[index++]));
		pos.setZ(Float.parseFloat(line[index++]));
		dir.setX(Float.parseFloat(line[index++]));
		dir.setY(Float.parseFloat(line[index++]));
		dir.setZ(Float.parseFloat(line[index++]));
		distance=Float.parseFloat(line[index++]);
		surface=line[index++];
		volume=line[index++];
		w=Float.parseFloat(line[index++]);
	}

	public float getLambda() {
		return lambda;
	}

	public void setLambda(float lambda) {
		this.lambda = lambda;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Vecteur getPos() {
		return pos;
	}

	public void setPos(Vecteur pos) {
		this.pos = pos;
	}

	public Vecteur getDir() {
		return dir;
	}

	public void setDir(Vecteur dir) {
		this.dir = dir;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public String getSurface() {
		return surface;
	}

	public void setSurface(String surface) {
		this.surface = surface;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public float getW() {
		return w;
	}

	public void setW(float w) {
		this.w = w;
	}

	public String toString()
	{
		return lambda+" "+type+" "+pos.x()+" "+pos.y()+" "+pos.z()+" "+
				dir.x()+" "+dir.y()+" "+dir.z()+" "+
				distance+" "+this.surface+" "+this.volume+" "+w;
	}


}
