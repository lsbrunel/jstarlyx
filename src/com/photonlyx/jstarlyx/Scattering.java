package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

/**
 * volume material defined by Ks Ka and a phase function
 * @author laurent
 *
 */
public class Scattering extends Node implements XMLstorable
{
	private String ks="1";//refractive index spectrum of the spherical particle
	private String ka="0";//absorption coef of medium
	private String phase_functions="650 pf1";//name of the sampled data containing the phase function

	public Scattering()
	{
	}
	public Scattering(Node father)
	{
		super(father);
	}

	public Scattering(String name)
	{
		this.setName(name);
	}

	public String getKs() {
		return ks;
	}
	public void setKs(String ks) {
		this.ks = ks;
	}
	public String getKa() {
		return ka;
	}
	public void setKa(String ka) {
		this.ka = ka;
	}
	public String getPhase_functions() {
		return phase_functions;
	}
	public void setPhase_functions(String s) {
		this.phase_functions = s;
	}

	
	
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Scattering");
		el.setAttribute("name", this.getName());
		el.setAttribute("ks", ks);
		el.setAttribute("ka", ka);
		el.setAttribute("phase_functions", phase_functions);
		for (Node n:getSons()) 
		{
			SampledData o;
			if (n instanceof SampledData) 
			{
				o=(SampledData)n;
				el.addChild(o.toXML());
			}
		}
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		this.setKs(xml.getStringAttribute("ks",""));
		this.setKa(xml.getStringAttribute("ka",""));
		this.setKa(xml.getStringAttribute("phase_functions",""));
	}






}
