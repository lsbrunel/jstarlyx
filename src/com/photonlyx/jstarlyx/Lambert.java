package com.photonlyx.jstarlyx;


import java.util.Enumeration;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Vecteur;

import nanoxml.XMLElement;

public class Lambert extends Node implements XMLstorable
{
	private String albedo="1"; //albedo
	private String texture_image=null; //name of the png texture file
	private double texture_width=20; //width of the image in mm
	private Vecteur normal_texture=new Vecteur(1,0,0);
	private Vecteur horizontal_texture=new Vecteur(0,1,0);
	private Vecteur centre_texture=new Vecteur(0,0,0);

	public Lambert()
	{
	}
	public Lambert(Node father)
	{
		super(father);
	}

	public Lambert(String name)
	{
		this.setName(name);
	}

	public String getAlbedo() {
		return albedo;
	}

	public void setAlbedo(String albedo) {
		this.albedo = albedo;
	}

	public String getTexture_image() {
		return texture_image;
	}
	public void setTexture_image(String texture_image) {
		this.texture_image = texture_image;
	}
	public double getTexture_width() {
		return texture_width;
	}
	public void setTexture_width(double texture_width) {
		this.texture_width = texture_width;
	}
	
	public void setNormal_texture(Vecteur normal_texture) {
		this.normal_texture = normal_texture;
	}
	
	public void setHorizontal_texture(Vecteur horizontal_texture) {
		this.horizontal_texture = horizontal_texture;
	}
	public void setCentre_texture(Vecteur centre_texture) {
		this.centre_texture = centre_texture;
	}
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("lambert");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("ALBEDO", albedo);
		if (texture_image!=null)
		{
			el.setAttribute("TEXTURE_IMAGE", this.texture_image);
			el.setAttribute("TEXTURE_WIDTH", this.texture_width);
			//add normal_texture:
			XMLElement el1=normal_texture.toXML();
			el1.setName("normal_texture");
			el.addChild(el1);
			//add horizontal texture:
			XMLElement el3=horizontal_texture.toXML();
			el3.setName("horizontal_texture");
			el.addChild(el3);
			//add center of texture:
			XMLElement el2=centre_texture.toXML();
			el2.setName("centre_texture");
			el.addChild(el2);
		}
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
		this.setAlbedo(xml.getStringAttribute("ALBEDO","1"));
		this.setTexture_image(xml.getStringAttribute("TEXTURE_IMAGE",null));
		this.setTexture_width(xml.getDoubleAttribute("TEXTURE_WIDTH",20));
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("normal_texture"))
			{
				normal_texture.updateFromXML(el);
			}
			if (el.getName().contains("horizontal_texture"))
			{
				horizontal_texture.updateFromXML(el);
			}
			if (el.getName().contains("centre_texture"))
			{
				centre_texture.updateFromXML(el);
			}
		}
	}






}
