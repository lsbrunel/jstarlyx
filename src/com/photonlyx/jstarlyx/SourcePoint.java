package com.photonlyx.jstarlyx;

import java.util.Enumeration;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Point3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;

import nanoxml.XMLElement;

public class SourcePoint extends Node implements XMLstorable
{
	private Vecteur pos=new Vecteur(0,0,2);
	private double power=1;
	private String type="point";
	private String spectrum="";
	private String volume="World";

	public SourcePoint()
	{
		this.setName("sourcePoint0");
	}

	public SourcePoint(Node father)
	{
		super(father);
		this.setName("sourcePoint0");
	}


	@Override
	public Object3DColorSet getObject3D()
	{
		Object3DColorSet set=new Object3DColorSet();
		set.add(new Point3DColor(pos,5));
		return set;
	}


	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (int k=0;k<3;k++) 
		{
			double r=pos.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}			
	}



	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("source");

		el.setAttribute("name", this.getName());
		el.setDoubleAttribute("POWER", power);
		el.setAttribute("SPECTRUM", spectrum);
		el.setAttribute("TYPE", type);
		el.setAttribute("VOLUME", volume);

		//add dir:
		XMLElement el1=pos.toXML();
		el1.setName("pos");
		el.addChild(el1);

		return el;
	}




	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		power=xml.getDoubleAttribute("POWER",1);
		type=xml.getStringAttribute("TYPE","");
		spectrum=xml.getStringAttribute("SPECTRUM","");
		volume=xml.getStringAttribute("VOLUME","World");

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("pos"))
			{
				pos.updateFromXML(el);
			}

		}

	}

	public double getX() {
		return pos.x();
	}

	public void setX(double x) {
		pos.setX( x);
	}

	public double getY() {
		return pos.y();
	}

	public void setY(double y) {
		pos.setY (y);
	}

	public double getZ() {
		return pos.z();
	}

	public void setZ(double z) {
		pos.setZ (z);
	}

	public double getIntensity() {
		return power;
	}

	public void setPower(double d) {
		this.power = d;
	}

	public String getSpectrum() {
		return spectrum;
	}

	public void setSpectrum(String spectrum) {
		this.spectrum = spectrum;
	}




}
