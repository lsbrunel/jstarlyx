package com.photonlyx.jstarlyx;



import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

public class Spectrum  extends Node implements XMLstorable
{
	private String data;

	public Spectrum()
	{
	}

	public Spectrum(Node father)
	{
		super(father);
	}

	public Spectrum(String name)
	{
		this.setName(name);
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}



	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("spectrum");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("DATA", data);
		return el;
	}



	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
		data=xml.getStringAttribute("DATA");
	}



}
