package com.photonlyx.jstarlyx.hdr;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.plx.physics.color.CalcRGBFromSpectrum;
import com.plx.physics.color.Formulae;

public class HyperSpectralImage 
{
	private double xmin=0,xmax=1,ymin=0,ymax=1;
	private int w=0,h=0,nbc=0; //width, height, nb of channels
	private float[] data;
	public double[] waveLengthOptions;
	private boolean auto=true;
	private double factor=1;//to multiply for convertion to 255 scale, 
	private double gamma=1;//non linear correction
	private double waveLength;//selected wavelength


	public HyperSpectralImage()
	{
		data=new float[0];
	}

	public void init()
	{
		data=new float[0];
	}

	public CImageProcessing getCImageProcessing()
	{
		if (data.length==0) return null;

		CImageProcessing cimp=new CImageProcessing(w,h);
		int[] pix=new int[w*h];
		int r,g,b;
		int indexr,indexg,indexb;
		//System.out.println(" factor ="+factor);
		Signal1D1DXY spectrum=new Signal1D1DXY(nbc);
		double[] XYZ=new double[3];
		double[] RGB=new double[3];
		int index_hsp,index_rgb;
		int[] pixels = new int[w * h * 3];//raw data of rgb image
		float[] pixels_not_normalized = new float[w * h * 3];//raw data of rgb image, before normalization
		if (nbc>3)
		{
			float max=0;
			for (int i=0;i<w;i++)
				for (int j=0;j<h;j++)
				{
					//int index_pix=j+i*h;//index of pixel
					int index_pix=i+j*w;//index of pixel
					index_hsp=index_pix*nbc; //index of first data of pixel spectrum in raw hyperspectral data
					for (int k_hsp=0;k_hsp<nbc;k_hsp++)
					{
						spectrum.setValue(k_hsp, waveLengthOptions[k_hsp], data[index_hsp+k_hsp]);
					}
					if ((i==2)&&(j==4)) System.out.println(spectrum);

					//convert spectrum to rgb:
					//if  ((i==2)&&(j==4))CalcRGBFromSpectrum.calcTristimulusFromSpectrum2(XYZ, spectrum);
					CalcRGBFromSpectrum.calcTristimulusFromSpectrum(XYZ, spectrum);
					//if ((i==2)&&(j==4)) System.out.println("XYZ="+XYZ[0]+" "+XYZ[1]+" "+XYZ[2]);
					Formulae.XYZ2RGB(XYZ, RGB);
					if ((i==2)&&(j==4)) System.out.println("RGB="+RGB[0]+" "+RGB[1]+" "+RGB[2]);
					for (int k_rgb=0;k_rgb<3;k_rgb++)
					{
						//				indexppm=k+(i+j*w)*3;
						//				pixels_not_normalized[indexppm]=rgb[k];
						index_rgb=index_pix*3;//index of first data of pixel rgb values in raw hyperspectral data
						pixels_not_normalized[index_rgb+k_rgb]=(float)RGB[k_rgb];
						if (RGB[k_rgb]>max) max=(float)RGB[k_rgb];
					}
				}
			//System.out.println("max="+max);
			//normalize
			if (auto) 
			{
				factor=0;//coef of gamma correction Iin = a Iin^gamma
				if (max==0) max=1;//avoid div by zero
				else factor=(255.0/Math.pow(max, gamma));
			}
			System.out.println("max="+max+" factor="+factor);
			for (int kk=0;kk<w*h*3;kk++) 
				pixels[kk]=(int)(factor*Math.pow(pixels_not_normalized[kk],gamma));
			//fill the java image:
			for (int i=0;i<w;i++)
				for (int j=0;j<h;j++) 
				{
					//int index_pix=j+i*h;//index of pixel
					int index_pix=i+j*w;//index of pixel
					index_rgb=index_pix*3;//index of first data of pixel rgb values in raw hyperspectral data
					//must be in range 0 255 :
					pixels[index_rgb]=Math.max(0,pixels[index_rgb] );
					pixels[index_rgb+1]=Math.max(0,pixels[index_rgb+1]);
					pixels[index_rgb+2]=Math.max(0,pixels[index_rgb+2]);
					pixels[index_rgb]=Math.min(255,pixels[index_rgb]);
					pixels[index_rgb+1]=Math.min(255,pixels[index_rgb+1]);
					pixels[index_rgb+2]=Math.min(255,pixels[index_rgb+2]);
					//set rgb:
					r=pixels[index_rgb];
					g=pixels[index_rgb+1];
					b=pixels[index_rgb+2];
					pix[i+w*j]=new Color(r,g,b).getRGB();
				}


		}
		else if (nbc==3)
		{
			float max=vmax();
			if (auto) 
			{
				if (max!=0) factor=(255.0/Math.pow(max, gamma));
				else factor=1;
			}
			for (int i=0;i<w;i++)
				for (int j=0;j<h;j++) 
				{
					indexr=(i+j*w)*nbc;
					indexg=indexr+1;
					indexb=indexr+2;
					r=(int)(Math.pow(data[indexr],gamma)*factor);
					g=(int)(Math.pow(data[indexg],gamma)*factor);
					b=(int)(Math.pow(data[indexb],gamma)*factor);
					if (r>255) r=255;
					if (g>255) g=255;
					if (b>255) b=255;
					//System.out.println(" r g b ="+r+" "+g+" "+b);
					pix[i+w*j]=new Color(r,g,b).getRGB();
				}
		}

		else if (nbc==1)
		{
			float max=vmax();
			if (auto) 
			{
				if (max!=0) factor=(255.0/Math.pow(max, gamma));
				else factor=1;
			}
			int index;
			for (int i=0;i<w;i++)
				for (int j=0;j<h;j++) 
				{
					index=(i+j*w)*nbc;
					r=(int)(Math.pow(data[index],gamma)*factor);
					g=(int)(Math.pow(data[index],gamma)*factor);
					b=(int)(Math.pow(data[index],gamma)*factor);
					if (r>255) r=255;
					if (g>255) g=255;
					if (b>255) b=255;
					//System.out.println(" r g b ="+r+" "+g+" "+b);
					pix[i+w*j]=new Color(r,g,b).getRGB();
				}
		}

		cimp.setPixelsDirect(pix, w, h);
		return cimp;
	}


	public float vmin()
	{
		float r=(float)1e30;
		float v;
		//for (int i=0;i<w;i++) for (int j=0;j<h;j++) 
		for (int k=0;k<w*h*nbc;k++)  
		{
			v=data[k];
			if (v<r) r=v;
		}
		return  r;
	}


	public float vmax()
	{
		if (data.length==0) return 0;
		float r=(float)-1e30;
		float v;
		for (int k=0;k<w*h*nbc;k++)  
		{
			v=data[k];
			if (v>r) r=v;
		}
		return  r;
	}



	/**
	 * read an image encoded in floats
	 * @param path
	 * @param file
	 * @param littleEndian  bigEndian for java, littleEndian for C
	 * @return
	 */
	public boolean load(String path,String file,boolean littleEndian)
	{

		byte[] b0=new byte[(7)*Float.BYTES];
		BufferedInputStream br;
		try
		{
			br=new BufferedInputStream(new FileInputStream(path+File.separator+file));
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			return false;
		}
		//read header:
		try
		{
			br.read(b0,0,b0.length);
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading raw float image ");
			e.printStackTrace();
			try {br.close();} catch (Exception e1) {};
			return false;
		}
		ByteBuffer buf0=ByteBuffer.wrap(b0);
		if (littleEndian) buf0.order(ByteOrder.LITTLE_ENDIAN); 
		w=(int)buf0.getFloat();
		h=(int)buf0.getFloat();
		nbc=(int)buf0.getFloat();
		xmin=buf0.getFloat();
		xmax=buf0.getFloat();
		ymin=buf0.getFloat();
		ymax=buf0.getFloat();
		//System.out.println("littleEndian ="+littleEndian);
		System.out.println("w="+w+" h="+h+" nbc="+nbc+"  xmin="+xmin+" xmax="+xmax+" ymin="+ymin+" ymax="+ymax);
		if ((w<=0)||(h<=0)) 
		{
			System.err.println("Error w or h <=0");
			try {br.close();} catch (Exception e1) {};
			return false;
		}
		//read data:
		byte[] b=new byte[(w*h*nbc)*Float.BYTES];
		//System.out.println("read "+(b.length)+" bytes in file:"+path+file+" ...");
		try
		{
			br.read(b,0,b.length);
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading data of raw float image ");
			e.printStackTrace();
			return false;
		}
		//read wavelengths:
		byte[] b2=new byte[(nbc)*Float.BYTES];
		//System.out.println("read "+(b.length)+" bytes in file:"+path+file+" ...");
		try
		{
			br.read(b2,0,b2.length);
			br.close();
		}
		catch (Exception e)
		{
			System.err.println(" PROBLEM reading wavelengths of raw float image ");
			e.printStackTrace();
			return false;
		}

		//for (int k=0;k<100;k++) System.out.print(b[k]+" "); System.out.println();
		System.out.println("float image file "+path+File.separator+file+" loaded");
		ByteBuffer buf=ByteBuffer.wrap(b);
		if (littleEndian) buf.order(ByteOrder.LITTLE_ENDIAN); 
		data=new float[w*h*nbc];
		for (int k=0;k<w*h*nbc;k++) 
		{
			data[k]=buf.getFloat();
			//if (data[k]!=0) System.out.println(k+" "+data[k]);
		}
		waveLengthOptions=new double[nbc];
		System.out.println(nbc+" channels");
		ByteBuffer buf2=ByteBuffer.wrap(b2);
		if (littleEndian) buf2.order(ByteOrder.LITTLE_ENDIAN); 
		for (int k=0;k<nbc;k++) 
		{
			waveLengthOptions[k]=(double)buf2.getFloat();
			System.out.println((float)waveLengthOptions[k]);
		}
		waveLength=waveLengthOptions[0];
		return true;
	}



	//
	//	/**
	//	 * read an image encoded in floats
	//	 * @param fullFilename
	//	 * @param littleEndian  bigEndian for java, littleEndian for C
	//	 * @return
	//	 */
	//	public  boolean load2(String path,String file,boolean littleEndian)
	//	{
	//		System.out.println("read  file:"+path+File.separator+file);
	//
	//		byte[] b0=new byte[(700)*Float.BYTES];
	//		BufferedInputStream br;
	//		try
	//		{
	//			br=new BufferedInputStream(new FileInputStream(path+File.separator+file));
	//		}
	//		catch (Exception e)
	//		{
	//			System.err.println(" PROBLEM reading raw float image ");
	//			e.printStackTrace();
	//			return false;
	//		}
	//		//read header:
	//		try
	//		{
	//			int a=28;
	//			for (int k=0;k<a;k++) System.out.print(br.read()+" ");System.out.println();
	//			for (int k=a;k<100;k++) System.out.print(br.read()+" ");System.out.println();
	//		}
	//		catch (Exception e)
	//		{
	//			System.err.println(" PROBLEM reading raw float image ");
	//			e.printStackTrace();
	//			try {br.close();} catch (Exception e1) {};
	//			return false;
	//		}
	//		//		ByteBuffer buf0=ByteBuffer.wrap(b0);
	//		//if (littleEndian) buf0.order(ByteOrder.LITTLE_ENDIAN); 
	//		//		w=(int)buf0.getFloat();
	//		//		h=(int)buf0.getFloat();
	//		//		nbc=(int)buf0.getFloat();
	//		//		xmin=buf0.getFloat();
	//		//		xmax=buf0.getFloat();
	//		//		ymin=buf0.getFloat();
	//		//		ymax=buf0.getFloat();
	//		System.out.println("littleEndian ="+littleEndian);
	//		//System.out.println("w="+w+" h="+h+" nbc="+nbc+"  xmin="+xmin+" xmax="+xmax+" ymin="+ymin+" ymax="+ymax);
	//
	//		w=10;
	//		h=10;
	//		nbc=3;
	//		data=new float[10*10*3];
	//		//
	//		//		byte[] b=new byte[(w*h*nbc)*Float.BYTES];
	//		//		System.out.println("read "+(b.length)+" bytes in file:"+path+file+" ...");
	//		//		byte b1=0;
	//		//		try
	//		//		{
	//		//			for (int k=0;k<3000;k++) 
	//		//			{
	//		//				b1=(byte)br.read(); 
	//		//				data[k]=10;
	//		//
	//		//				System.out.print(b1+" "); 
	//		//			}
	//		//			System.out.println();
	//		//			br.close();
	//		//		}
	//		//		catch (Exception e)
	//		//		{
	//		//			System.err.println(" PROBLEM reading raw float image ");
	//		//			e.printStackTrace();
	//		//			return false;
	//		//		}
	//
	//
	//		return true;
	//	}

	/**
	 * 
	 * @param lambda wavelength in nm
	 * @return
	 */
	public Signal2D1D getChannel(double lambda)
	{
		Signal2D1D signal2D1D=new Signal2D1D();
		if (waveLengthOptions==null) return signal2D1D;
		int channel=-1;
		for (int i=0;i<waveLengthOptions.length;i++) if (waveLengthOptions[i]==lambda) channel=i; 
		if (channel==-1)
		{
			System.out.println(getClass()+"  Can't find channel of wavelength "+lambda+" nm");
			return signal2D1D;
		}
		float[] data=this.getData();
		if (data.length>0)
		{
			int w=this.getW();
			int h=this.getH();
			signal2D1D.init(w,h);
			int index;
			for (int i=0;i<w;i++)
				for (int j=0;j<h;j++)
				{
					index=(i+j*w)*nbc+channel;
					signal2D1D.setValue(i,j,data[index]);
				}
		}
		return signal2D1D;
	}

	public void div_(HyperSpectralImage im)
	{
		for (int kk=0;kk<w*h*nbc;kk++) data[kk]=data[kk]/im.data[kk];
	}
	public void mul_(float d)
	{
		for (int kk=0;kk<w*h*nbc;kk++) data[kk]=data[kk]*d;
	}

	public double getXmin() {
		return xmin;
	}


	public void setXmin(double xmin) {
		this.xmin = xmin;
	}


	public double getXmax() {
		return xmax;
	}


	public void setXmax(double xmax) {
		this.xmax = xmax;
	}


	public double getYmin() {
		return ymin;
	}


	public void setYmin(double ymin) {
		this.ymin = ymin;
	}


	public double getYmax() {
		return ymax;
	}


	public void setYmax(double ymax) {
		this.ymax = ymax;
	}


	public boolean isAuto() {
		return auto;
	}


	public void setAuto(boolean auto) {
		this.auto = auto;
	}


	public double getFactor() {
		return factor;
	}


	public void setFactor(double factor) {
		this.factor = factor;
	}


	public double getGamma() {
		return gamma;
	}



	public void setGamma(double gamma) {
		this.gamma = gamma;
	}



	public int getW() {
		return w;
	}


	public int getH() {
		return h;
	}


	public int getNbc() {
		return nbc;
	}

	public float[] getData() {
		return data;
	}

	//	public double[] waveLengthOptions() {
	//		return wavelengthOptions;
	//	}

	public double getWaveLength() {
		return waveLength;
	}

	public void setWaveLength(double waveLength) {
		this.waveLength = waveLength;
	}






}
