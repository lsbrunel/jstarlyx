package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

/**
 * scatterer defined by Henyey-Greenstein phase function and transport length l* in mm
 * @author laurent
 *
 */
public class HenyeyGreensteinLstar extends Node implements XMLstorable
{
	private String lstar="";//name of spectrum of LSTAR
	private String la="";//name of spectrum of LA
	private String g="";//name of spectrum of G

	public HenyeyGreensteinLstar()
	{
	}
	public HenyeyGreensteinLstar(Node father)
	{
		super(father);
	}

	public HenyeyGreensteinLstar(String name)
	{
		this.setName(name);
	}

	public String getLstar() {
		return lstar;
	}
	public void setLstar(String lstar) {
		this.lstar = lstar;
	}
	public String getLa() {
		return la;
	}
	public void setLa(String la) {
		this.la = la;
	}
	public String getG() {
		return g;
	}

	public void setG(String g) {
		this.g = g;
	}




	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Henyey-Greenstein");
		el.setAttribute("name", this.getName());
		el.setAttribute("lstar", lstar);
		el.setAttribute("la", la);
		el.setAttribute("g", g);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		this.setLstar(xml.getStringAttribute("lstar",""));
		this.setLa(xml.getStringAttribute("la",""));
		this.setG(xml.getStringAttribute("g",""));
	}






}
