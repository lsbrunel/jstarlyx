package com.photonlyx.jstarlyx;


/**
 * generate a Scene
 * @author laurent
 *
 */
public interface SceneGenerator 
{
	public Scene getScene();
	public  String createProject();
	
	public int getNbPhotons() ;
	public void setNbPhotons(int nbPhotons);
}
