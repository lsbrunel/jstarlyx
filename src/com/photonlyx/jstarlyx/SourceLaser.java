package com.photonlyx.jstarlyx;

import java.util.Enumeration;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Point3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;

import nanoxml.XMLElement;

public class SourceLaser extends Node implements XMLstorable
{
	private Vecteur pos=new Vecteur(-3,-3,3);
	private Vecteur dir=new Vecteur(1,0,0);
	private double power=1;
	private double diameter=0;
	private double angle=0;
	private String type="laser";
	private String spectrum="";
	private String volume="World";
	
	
	public SourceLaser()
	{
		this.setName("laser0");
	}

	public SourceLaser(Node n)
	{
		super(n);
		this.setName("laser0");
	}

	@Override
	public Object3DColorSet getObject3D()
	{
		Object3DColorSet set=new Object3DColorSet();
		set.add(new Point3DColor(pos,5));
		set.add(new Segment3DColor(pos,pos.addn(dir)));
		return set;
	}


	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("source");

		el.setAttribute("name", this.getName());
		el.setDoubleAttribute("POWER", power);
		el.setDoubleAttribute("DIAMETER", diameter);
		el.setDoubleAttribute("ANGLE", angle);
		el.setAttribute("SPECTRUM", spectrum);
		el.setAttribute("TYPE", type);
		el.setAttribute("VOLUME", volume);

		//add pos:
		XMLElement elLens=pos.toXML();
		elLens.setName("pos");
		el.addChild(elLens);
		//add dir:
		XMLElement elDir=dir.toXML();
		elDir.setName("dir");
		el.addChild(elDir);

		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		power=xml.getDoubleAttribute("POWER",1);
		diameter=xml.getDoubleAttribute("DIAMETER",0);
		angle=xml.getDoubleAttribute("ANGLE",0);
		type=xml.getStringAttribute("TYPE","");
		spectrum=xml.getStringAttribute("SPECTRUM","");
		volume=xml.getStringAttribute("VOLUME","World");

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("pos"))
			{
				pos.updateFromXML(el);
			}
			if (el.getName().contains("dir"))
			{
				dir.updateFromXML(el);
			}

		}
	}



	public Vecteur getPos()
	{
		return pos;
	}


	public Vecteur getDir() {
		return dir;
	}

	public double getX() {
		return pos.x();
	}

	public void setX(double x) {
		pos.setX( x);
	}

	public double getY() {
		return pos.y();
	}

	public void setY(double y) {
		pos.setY (y);
	}

	public double getZ() {
		return pos.z();
	}

	public void setZ(double z) {
		pos.setZ (z);
	}

	public void setPower(double d) {
		power=d;
		
	}

	
	
	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public void setSpectrum(String s) {
		spectrum=s;
		
	}



	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (int k=0;k<3;k++) 
		{
			double r=pos.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}			
	}








}
