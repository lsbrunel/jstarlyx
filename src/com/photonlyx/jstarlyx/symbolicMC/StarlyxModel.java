package com.photonlyx.jstarlyx.symbolicMC;

import com.photonlyx.jstarlyx.Scene;

public interface StarlyxModel 
{
	public Scene getScene();
	
	public double getKs();

	public double getKa();

	public boolean buildModel();
}
