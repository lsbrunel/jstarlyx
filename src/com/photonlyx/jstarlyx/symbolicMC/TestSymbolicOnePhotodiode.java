package com.photonlyx.jstarlyx.symbolicMC;

import java.io.File;

import com.photonlyx.toolbox.gui.WindowApp;

public class TestSymbolicOnePhotodiode extends WindowApp
{
	public static String path="/home/laurent/temp/testlyxs_symbolic";


	public static void main(String[] args)
	{
		//Create directory 
		File newDir=new File(path);
		if (!newDir.exists()) 
		{
			System.out.println("Create the directory:" +newDir.getAbsolutePath());
			newDir.mkdir();
		}

		//create symbolic MC:
		StarlyxModel modelSymbolic=new StarlyxModelSymbolicSlabOnePhotodiode(path);
		//create normal MC :
		StarlyxModel modelNormal=new StarlyxModelSymbolicSlabOnePhotodiode(path);
		WindowApp app=new TestSymbolicWindow(path,modelNormal,modelSymbolic,false);
	}
	
	

}
