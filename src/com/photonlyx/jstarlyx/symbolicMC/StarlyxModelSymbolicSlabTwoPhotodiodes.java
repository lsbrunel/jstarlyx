package com.photonlyx.jstarlyx.symbolicMC;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.Lambert;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.SourceLaser;
import com.photonlyx.jstarlyx.Spectrum;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.Volume;
import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.CylinderMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;


/**
 * starlyx model using symbolic MC for fast inversion:
 * @author laurent
 *
 */
public class StarlyxModelSymbolicSlabTwoPhotodiodes  implements StarlyxModel
{
	private Scene scene;
	private double wavelength_nm=650;
	private double photodiode_diameter=10;
	private Vecteur photodiode_T_dir=new Vecteur(1,0,0);
	private Vecteur photodiode_T_pos=new Vecteur(5,0,0);
	private Vecteur photodiode_BS_dir=new Vecteur(1,0,0);
	private Vecteur photodiode_BS_pos=new Vecteur(-5,0,0);
	private Vecteur slab_pos=new Vecteur(0,-5,-5);
	private double slab_thickness=1;
	private double slab_size=10;
	//radiative transfer properties of slab:
	private double k=2;
	private double g=0;
	private double ka=0;
	private double n=1;
	//symbolic MC parameters:
	private double k_hat=4;
	private double pScat=0.33;//fixed probability to sort scattering event, used for symbolic MC
	private double pAbs=0.33;//fixed probability to sort absorption event, used for symbolic MC
	private int nbPhotons=10000;

	private static DecimalFormat df = new DecimalFormat("0.00000000000",new DecimalFormatSymbols(Locale.ENGLISH));


	public 	StarlyxModelSymbolicSlabTwoPhotodiodes(String path)
	{
		scene=new Scene(path);	
	}

	public 	StarlyxModelSymbolicSlabTwoPhotodiodes(Scene scene)
	{
		this.scene=scene;
	}

	public boolean buildModel()
	{

		//System.out.println("buildModel model="+model);
		scene.free();
		scene.setAlgorithm("direct");
		scene.setNbThreads(4);//set 4 threads for starlyx MC calculation
		scene.setkHat(k_hat);
		scene.setpScat(pScat);
		scene.setpAbs(pAbs);

		scene.setNbPhotons(nbPhotons);
		scene.setVerbose(1);
		scene.setStoreTrajectories(0);


		Spectrum spred=new Spectrum();
		spred.setName("red");
		spred.setData((int)wavelength_nm+" 1");
		scene.add(spred);

		//add source:
		{
		SourceLaser spot=new SourceLaser();
		spot.getPos().copy(new Vecteur(-2,0,0));
		spot.getDir().copy(new Vecteur(1,0,0));
		spot.setPower(1);
		spot.setDiameter(0);
		//spot.setAngle(0);
		spot.setName("source0");
		spot.setSpectrum("red");
		scene.add(spot);
		}
		
		//add scattering slab:
		{

			//the box:
			Box slab=new Box(slab_pos,new Vecteur(slab_thickness,slab_size,slab_size));
			slab.updateGlobal();
			//save  OBJ file:
			String file="slab.obj";
			TextFiles.saveString(scene.getPath()+File.separator+file,slab.getTriangles().saveToOBJ(),false);				

			Surface surf=new Surface(scene.getPath());
			surf.setName("slab");
			surf.setMaterials("");
			surf.setFile(file);
			scene.add(surf);
		}
		//add slab volume
		
		{
			Volume v=new Volume();
			v.setName("slab");
			v.setSurfaces("slab");
			v.setN(df.format(n));
			v.setMaterials("hg1");
			scene.add(v);
		}
		{		
			//volume material for the slab:
			HenyeyGreensteinK hgSlab=new HenyeyGreensteinK();
			hgSlab.setName("hg1");
			hgSlab.setKs(df.format(k));
			hgSlab.setKa(df.format(ka));
			hgSlab.setG(df.format(g));
			scene.add(hgSlab);
		}

		//add photodiode BS
		{
			//a cylinder:
			double hcyl=photodiode_diameter/10;
			CylinderMesh photodiode=new CylinderMesh(photodiode_diameter,hcyl,20);//represents the probe
			photodiode.getFrame().buildSuchThatZaxisIs(photodiode_BS_dir.scmul(1));
			photodiode.getFrame()._translate(photodiode_BS_pos);
			photodiode.getFrame()._translate(photodiode.getFrame().z().scmul(-hcyl));
			photodiode.updateGlobal();
			//save  OBJ file:
			String file="photodiodeBS.obj";
			TextFiles.saveString(scene.getPath()+File.separator+file,photodiode.getTriangles().saveToOBJ(),false);				

			Surface surf=new Surface(scene.getPath());
			surf.setName("photodiodeBS");
			surf.setMaterials("absorber");
			surf.setFile(file);
			scene.add(surf);
		}

		//add photodiode T
		{
			//a cylinder:
			double hcyl=photodiode_diameter/10;
			CylinderMesh photodiode=new CylinderMesh(photodiode_diameter,hcyl,20);//represents the probe
			photodiode.getFrame().buildSuchThatZaxisIs(photodiode_T_dir.scmul(1));
			photodiode.getFrame()._translate(photodiode_T_pos);
			photodiode.getFrame()._translate(photodiode.getFrame().z().scmul(-hcyl));
			photodiode.updateGlobal();
			//save  OBJ file:
			String file="photodiodeT.obj";
			TextFiles.saveString(scene.getPath()+File.separator+file,photodiode.getTriangles().saveToOBJ(),false);				

			Surface surf=new Surface(scene.getPath());
			surf.setName("photodiodeT");
			surf.setMaterials("absorber");
			surf.setFile(file);
			scene.add(surf);
		}
		
		
		{
			//create surface material for the sensor:
			Lambert l=new Lambert();
			l.setAlbedo("0");
			l.setName("absorber");
			scene.add(l);
		}


		scene.save();

		return true;

	}



	public Scene getScene() 
	{
		return scene;
	}




	public double getWavelength_nm() {
		return wavelength_nm;
	}

	public void setWavelength_nm(double wavelength_nm) {
		this.wavelength_nm = wavelength_nm;
	}

	public double getPhotodiode_diameter() {
		return photodiode_diameter;
	}

	public void setPhotodiode_diameter(double photodiode_diameter) {
		this.photodiode_diameter = photodiode_diameter;
	}

	public double getSlab_thickness() {
		return slab_thickness;
	}

	public void setSlab_thickness(double slab_thickness) {
		this.slab_thickness = slab_thickness;
	}

	public double getSlab_size() {
		return slab_size;
	}

	public void setSlab_size(double slab_size) {
		this.slab_size = slab_size;
	}

	public double getKs() {
		return k;
	}

	public void setKs(double k) {
		this.k = k;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getKa() {
		return ka;
	}

	public void setKa(double ka) {
		this.ka = ka;
	}

	public double getN() {
		return n;
	}

	public void setN(double n) {
		this.n = n;
	}

	public double getK_hat() {
		return k_hat;
	}

	public void setK_hat(double k_hat) {
		this.k_hat = k_hat;
	}

	public double getpScat() {
		return pScat;
	}

	public void setpScat(double pScat) {
		this.pScat = pScat;
	}

	public double getpAbs() {
		return pAbs;
	}

	public void setpAbs(double pAbs) {
		this.pAbs = pAbs;
	}



	public int getNbPhotons() {
		return nbPhotons;
	}

	public void setNbPhotons(int nbPhotons) {
		this.nbPhotons = nbPhotons;
	}

	public static void main(String args[])
	{

		String path="/home/laurent/temp/symbolic";
		
		//Create directory 
		File newDir=new File(path);
		if (!newDir.exists()) 
		{
			System.out.println("Create the directory:" +newDir.getAbsolutePath());
			newDir.mkdir();
		}
		
		StarlyxModelSymbolicSlabTwoPhotodiodes o=new StarlyxModelSymbolicSlabTwoPhotodiodes(path);
		o.buildModel();
		//run starlyx:
		o.getScene().runStarlyx();


	}





}
