package com.photonlyx.jstarlyx.symbolicMC;

import java.io.File;
import java.util.Vector;

import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.StringSource;
import com.photonlyx.toolbox.txt.TextFiles;

public class Polynom 
{
	private String label;//name of the polynom
	private Vector<Element>	list=new Vector<Element>();//elements of the polynomial equation
	//private Scene scene;//the starlyx scene that has calculated this polynomial function
	//temp values
	private int n;
	private double k_hat;
	private double p_scat;
	private double p_abs;
	private double p_null;
	
	
	public Polynom(Scene scene)
	{
		//this.scene=scene;
		n=scene.getNbPhotons();
		k_hat=scene.getkHat();
		p_scat=scene.getpScat();
		p_abs=scene.getpAbs();
		p_null=1-p_scat-p_abs;
		}
	
	public void loadFromFile(String path,String filename)
	{
		list.removeAllElements();
		StringBuffer sb=TextFiles.readFile(path+File.separator+filename,true,false);
		if (sb==null) return ;
		String[] lines=StringSource.readLines(sb.toString());
		int c=0;
		for (String line:lines)
		{
			if (c!=0)
			{
				Definition def=new Definition(line);
				if (def.dim()==4)
				{
					Element el=new Element();
					el.setOcc(Integer.parseInt(def.word(0)));
					el.setPowScat(Integer.parseInt(def.word(1)));
					el.setPowAbs(Integer.parseInt(def.word(2)));
					el.setPowNull(Integer.parseInt(def.word(3)));
					//System.out.println(el);
					list.add(el);
				}
			}
			else
			{
				label=line;
			}
			c++;
		}
	}

//	public double value(int n,double k_hat,double p_scat,double p_abs,double p_null,double k_scat,double k_abs)
	public double value(double k_scat,double k_abs)
	{
		double w=0;
		double r=1;
		double k_null=k_hat-k_scat-k_abs;
		for (Element el:list)
		{
			int occurrence=el.getOcc();
			double power_scat=el.getPowScat();
			double power_abs=el.getPowAbs();
			double power_null=el.getPowNull();
			r=1;
			if (power_scat!=0) r*=Math.pow((double)k_scat/(double)k_hat/(double)p_scat,power_scat);
			if (power_abs!=0)  r*=Math.pow((double)k_abs/(double)k_hat/(double)p_abs,power_abs);
			if (power_null!=0) r*=Math.pow((double)k_null/(double)k_hat/(double)p_null,power_null);
			w+=r*occurrence;
			//n+=occurrence;
		}
		if (n!=0) w/=n;
		return w;
	}
	


	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		sb.append(label+"\n");
		for (Element el:list)
		{
			sb.append(el.toString()+"\n");
		}
		return sb.toString();
	}



	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}





	class Element
	{
		int occ;//occurence
		int powScat;//power for scattering
		int powAbs;//power for absorption
		int powNull;//power for null collision

		public int getOcc() {
			return occ;
		}
		public void setOcc(int occ) {
			this.occ = occ;
		}
		public int getPowScat() {
			return powScat;
		}
		public void setPowScat(int powScat) {
			this.powScat = powScat;
		}
		public int getPowAbs() {
			return powAbs;
		}
		public void setPowAbs(int powAbs) {
			this.powAbs = powAbs;
		}
		public int getPowNull() {
			return powNull;
		}
		public void setPowNull(int powNull) {
			this.powNull = powNull;
		}


		public String toString()
		{
			return occ+" "+powScat+" "+powAbs+" "+powNull;
		}
	}

}
