package com.photonlyx.jstarlyx.symbolicMC;

import java.io.File;

import com.photonlyx.toolbox.gui.WindowApp;

public class TestSymbolicTwoPhotodiodes extends WindowApp
{
	public static String path="/home/laurent/temp/testlyxs_symbolic";

	public static void main(String[] args)
	{
		//Create directory 
		File newDir=new File(path);
		if (!newDir.exists()) 
		{
			System.out.println("Create the directory:" +newDir.getAbsolutePath());
			newDir.mkdir();
		}
		
		double eo=1;
		double e=1;
		double g=0;
		
		double ks=eo/(e*(1-g));
		double khat=ks*2;
		
		
		//create symbolic MC:
		StarlyxModelSymbolicSlabTwoPhotodiodes ms=new StarlyxModelSymbolicSlabTwoPhotodiodes(path);
		ms.setNbPhotons(100000);
		ms.setSlab_thickness(e);
		ms.setKs(ks);
		ms.setG(g);
		ms.setK_hat(khat);
		//create normal MC :
		StarlyxModelSymbolicSlabTwoPhotodiodes mn=new StarlyxModelSymbolicSlabTwoPhotodiodes(path);
		mn.setSlab_thickness(e);
		mn.setKs(ks);
		mn.setG(g);
		mn.setK_hat(-1);
		WindowApp app=new TestSymbolicWindow(path,mn,ms,true);
	}



}
