package com.photonlyx.jstarlyx.symbolicMC;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.util.Vector;

import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.gui.JstarlyxApp;
import com.photonlyx.jstarlyx.gui.SensorData;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.chart.PlotSet;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;

public class TestSymbolicWindow extends WindowApp
{
	//create symbolic MC:
	private StarlyxModel modelSymbolic;
	//create normal MC :
	private StarlyxModel modelNormal;
	private StarlyxModel modelNormal2;

	private ChartPanel cp=new ChartPanel();
	//	private StarlyxModelVariation starlyxModelVariation=new StarlyxModelVariation(path);
	private ParamsBox box,box2;
	private int nbPtsMCnormal=10;//nb of points for MC normal
	private int nbPtsPolynome=30;//nb of points for polynom evaluation

	private PlotSet psPolynomes=new PlotSet();
	private PlotSet psMCnormal=new PlotSet();
	
	private boolean vary_kscat=true;//if true vary kscat if false vary k_scat

	public TestSymbolicWindow(String path,StarlyxModel modelNormal,StarlyxModel modelSymbolic,boolean vary_kscat)
	{
		super("TestSymbolic",false,false,false);
		this.path=path;
		this.modelNormal=modelNormal;
		this.modelNormal2=modelNormal2;
		this.modelSymbolic=modelSymbolic;
		this.vary_kscat=vary_kscat;
		this.add(cp,"test");

		cp.getPanelSide().setPreferredSize(new Dimension(200,0));
		//cp.getChart().setLogx(true);
		if (vary_kscat) cp.getChart().setXlabel("k_scat");else cp.getChart().setXlabel("k_abs");
		cp.getChart().setXunit("mm-1");
		cp.getChart().setYlabel("weight");
		//cw.getCJFrame().setExitAppOnExit(true);
		cp.getChart().add(psMCnormal);
		cp.getChart().add(psPolynomes);
		cp.update();

		//Create eventually directory at path 
		File newDir=new File(path);
		System.out.println("Create the directory:"+newDir.getAbsolutePath());
		if (!newDir.exists()) newDir.mkdir();

		updateGUI();

		calc();
	}


	public void calc()
	{
		if (modelSymbolic!=null) plot_symbolic_polynom();
		if (modelNormal!=null) plot_normal_MC();
	}


	public void updateGUI()
	{
		{
			String[] names={"nbPtsMCnormal"};
			String[] labels={"nbPts"};
			String[] units={""};
			box2=new ParamsBox(this,null,"MC normal",names,labels,units,ParamsBox.VERTICAL,200,25);
			cp.getPanelSide().add(box2.getComponent());
		}
		{
			String[] names={"nbPhotons"};
			String[] units={""};
			box=new ParamsBox(modelNormal,null,"MC normal",names,null,units,ParamsBox.VERTICAL,200,35);
			cp.getPanelSide().add(box.getComponent());
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"plot_normal_MC"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setText("Calc MC normal");
			cp.getPanelSide().add(cButton);
		}
		//button to launch jstarlyx for normal MC:
		{
			TriggerList tl1=new TriggerList();
			tl1.add(new Trigger(this,"launchJstarlyxMC_normal"));
			CButton cButton=new CButton(tl1);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setText("jstarlyx MC normal");
			cp.getPanelSide().add(cButton);
		}		
		{
			String[] names={"nbPtsPolynome"};
			String[] units={""};
			box=new ParamsBox(this,null,"MC symbolic",names,null,units,ParamsBox.VERTICAL,200,35);
			cp.getPanelSide().add(box.getComponent());
		}
		{
			String[] names={"nbPhotons","k_hat","pScat","pAbs","g","ka"};
			String[] units={"","mm","","","","mm"};
			box=new ParamsBox(modelSymbolic,null,"MC symbolic",names,null,units,ParamsBox.VERTICAL,200,35);
			cp.getPanelSide().add(box.getComponent());
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"plot_symbolic_polynom"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setText("Calc symbolic");
			cp.getPanelSide().add(cButton);
		}
		//button to launch jstarlyx for symbolic MC:
		{
			TriggerList tl1=new TriggerList();
			tl1.add(new Trigger(this,"launchJstarlyxMC_symbolic"));
			CButton cButton=new CButton(tl1);
			cButton.setPreferredSize(new Dimension(150,20));
			cButton.setText("jstarlyx MC symbolic");
			cp.getPanelSide().add(cButton);
		}		

		box.update();
		box2.update();
		cp.getPanelSide().validate();
		cp.getPanelSide().repaint();
	}


	public void plot_symbolic_polynom()
	{
		modelSymbolic.getScene().setFileName("scene_MC_symbolic.xml");
		modelSymbolic.buildModel();

		//remove polynomes files:
		for (File f:new File(path).listFiles())
		{
			if (f.getName().contains("polynom")) f.delete();
		}

		modelSymbolic.getScene().runStarlyx();

		//JstarlyxApp.openJstarlyx2(path,m.getScene().getFileName());
		//count the polynomes
		//String[] polynom_names= {"polynom_1_lambda=650nm_surface:photodiode.txt"};
		Vector<String> polynom_names=new Vector<String>();
		for (File f:new File(path).listFiles())
		{
			if (f.getName().contains("polynom")) polynom_names.add(f.getName());
		}

		Polynom[] polynoms=new Polynom[polynom_names.size()];

		for (int j=0;j<polynoms.length;j++) 
		{
			polynoms[j]=new Polynom(modelSymbolic.getScene());
			polynoms[j].loadFromFile(path, polynom_names.get(j));
			//System.out.println(polynoms[j]);
		}

		Signal1D1DXY[] sig=new Signal1D1DXY[polynom_names.size()];
		for (int j=0;j<polynom_names.size();j++) 
		{
			sig[j]=new Signal1D1DXY();
		}


		//check the polynom
		double ksmin=modelSymbolic.getScene().getkHat()/10;
		double ksmax=modelSymbolic.getScene().getkHat()*0.95;
		double sxlog=(Math.log(ksmax)-Math.log(ksmin))/nbPtsPolynome; 
		System.out.print("k_scat\t");
		for (int j=0;j<polynom_names.size();j++) System.out.print(polynom_names.get(j)+"\t");	
		System.out.print("\n");
		Scene s=modelSymbolic.getScene();
		double k_abs=modelSymbolic.getKa();
		double k_scat=modelSymbolic.getKs();
		for (int i=0;i<nbPtsPolynome;i++)
		{
			double k=Math.exp(Math.log(ksmin)+sxlog*i);
			double p_null=1-modelSymbolic.getScene().getpScat()-modelSymbolic.getScene().getpAbs();
			System.out.print(k+"\t");
			for (int j=0;j<polynoms.length;j++) 
			{
				//double w=polynoms[j].value(s.getNbPhotons(),s.getkHat(),s.getpScat(),s.getpAbs(),p_null,k_scat,kabs);
				double w;
				if (vary_kscat) w=polynoms[j].value(k,k_abs);else w=polynoms[j].value(k_scat,k);
				System.out.print(w+"\t");
				sig[j].addPoint(k, w);
			}
			System.out.print("\n");
		}

		//ChartWindow cw=new ChartWindow();
		psPolynomes.removeAllPlots();
		for (int j=0;j<polynoms.length;j++) 
		{
			Plot p=new Plot(sig[j],polynoms[j].getLabel(),Color.RED);
			psPolynomes.add(p);
		}
		cp.update();
	}



	public void plot_normal_MC()
	{

		System.out.println("Normal MC");
		modelNormal.getScene().setFileName("scene_MC_normal.xml");
		modelNormal.buildModel();
		modelNormal.getScene().setkHat(0);//set as normal MC
		modelNormal.getScene().setVerbose(0);
		//	m2.getScene().setNbPhotons(1000);
		modelNormal.getScene().save();

		//clean the MC normal plots:
		psMCnormal.removeAllPlots();
		//count the sensors:
		Vector<Node> list=modelNormal.getScene().getNodesOfClass("Surface");
		int nbSurfaces=list.size();
		Signal1D1DXY[] ws=new Signal1D1DXY[nbSurfaces];//weight signals
		Signal1D1DXY[] es=new Signal1D1DXY[nbSurfaces];//error signals
		for (int i=0;i<nbSurfaces;i++)
		{
			Surface surf=(Surface)(list.get(i));
			ws[i]=new Signal1D1DXY();
			es[i]=new Signal1D1DXY();
			Plot p=new Plot(ws[i],"MC normal "+surf.getName(),Color.BLUE);
			p.setError(es[i]);
			psMCnormal.add(p);
		}


		double ksmin=modelSymbolic.getScene().getkHat()/10;
		double ksmax=modelSymbolic.getScene().getkHat()*0.95;
		//double sxlog=(Math.log(ksmax)-Math.log(ksmin))/nbPtsMCnormal; 
		double sx=(ksmax-ksmin)/nbPtsMCnormal; 
		double k_abs=modelSymbolic.getKa();
		double k_scat=modelSymbolic.getKs();
		//vary the k
		for (int i=0;i<nbPtsMCnormal;i++)
		{
			//double k_scat=Math.exp(Math.log(ksmin)+sxlog*i);
			double k=ksmin+sx*i;
			HenyeyGreensteinK hg1=(HenyeyGreensteinK)modelNormal.getScene().getNodeOfName("hg1");
			if (vary_kscat) hg1.setKs(k+"");else hg1.setKa(k+"");
			modelNormal.getScene().save();
			modelNormal.getScene().runStarlyx();
			Vector<SensorData> data=SensorData.readSensorsCSV( path, "sensors.csv");
			//			for (SensorData sd:data) 
			//				{
			//				System.out.println(sd.getName());
			//				 Vector<Double> ls=sd.getLambdas();
			//				 Vector<Double> ws=sd.getWeights();
			//				 Vector<Double> ss=sd.getSigmas();
			//				for (int j=0;j<ls.size();j++) System.out.println(ls.get(j)+" "+ws.get(j)+" "+ss.get(j));
			//				}
			for (int j=0;j<nbSurfaces;j++)
			{
				double weight=data.get(j).getWeights().get(0);
				double sigma=data.get(j).getSigmas().get(0);
				System.out.println("weight="+weight);
				ws[j].addPoint(k,weight );
				es[j].addPoint(k,sigma*2 );				
			}
			cp.getChart().update();
		}
	}






	public  void launchJstarlyxMC_symbolic()
	{

		JstarlyxApp.openJstarlyx2(path,modelSymbolic.getScene().getFileName());
	}

	public  void launchJstarlyxMC_normal()
	{

		JstarlyxApp.openJstarlyx2(path,modelNormal.getScene().getFileName());
	}




	public int getNbPtsMCnormal() {
		return nbPtsMCnormal;
	}


	public void setNbPtsMCnormal(int nbPtsMCnormal) {
		this.nbPtsMCnormal = nbPtsMCnormal;
	}


	public int getNbPtsPolynome() {
		return nbPtsPolynome;
	}


	public void setNbPtsPolynome(int nbPtsPolynome) {
		this.nbPtsPolynome = nbPtsPolynome;
	}





	
	
}
