package com.photonlyx.jstarlyx.abaque;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.Dielectric;
import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.HenyeyGreensteinLstar;
import com.photonlyx.jstarlyx.Lambert;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.SourceSun;
import com.photonlyx.jstarlyx.Spectrum;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.Volume;
import com.photonlyx.jstarlyx.geometry.OBJ_tools;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;

public class SimulationLayerOnPlatePattern extends SimulationStarlyxLayer implements XMLstorable
{
	private double platedy=100;//mm
	private double platedz=100;//mm
	private double plateThickness=0.001;//mm
	private double glassRefactiveIndex=1;

	
	public SimulationLayerOnPlatePattern(Abaque abaque)
	{
		super(abaque);
	}

	
	public  String  getFullPath()
	{
		DecimalFormat df=new DecimalFormat("0.00000",new DecimalFormatSymbols(Locale.ENGLISH));
		String dir=
					"plate_"+df.format(plateThickness)
					+"_e_"+df.format(getLayerThickness())
					+"_g_"+getgSpectrum().replaceAll(" ","_")
					+"_lstar_"+getLstarSpectrum().replaceAll(" ","_")
					+"_la_"+getLaSpectrum().replaceAll(" ","_")
					+"_pixels_"+this.getDim()+"x"+this.getDim()
					+"_nbPhotons_"+this.getNbPhotons();
		String path=getAbaque().getBase_path()+File.separator+dir;
		return path;
	}

	
	public  String getSceneXML()
	{
		Scene scene=new Scene(this.getFullPath());

		scene.setNbPhotons(getNbPhotons());
		scene.setAlgorithm("reverse_1");
		scene.setStoreTrajectories(0);
		scene.setStoreErrorTrajectories(0);
		scene.setNbThreads(getNbThreads());
		scene.setVerbose(0);

		SourceSun source=new SourceSun();
		source.setName("sun");
		source.setX(1);
		source.setY(0);
		source.setZ(0);
		source.setPower(1);
		source.setSpectrum("source_spectrum");
		scene.add(source);
		
		{
		Spectrum sp=new Spectrum();
		sp.setData(this.getSourceSpectrum());
		sp.setName("source_spectrum");
		scene.add(sp);
		}

		Camera cam=new Camera();
		cam.setName(getCameraName());
		cam.setDimx(getDim());
		cam.setDimy(getDim());
		cam.setField(getCameraField());
		cam.setFilters(getCameraFilters());
		cam.setGamma(1);
		cam.setX(-300);
		cam.setY(0);
		cam.setZ(0);
		cam.setViewX(0);
		cam.setViewY(0);
		cam.setViewZ(0);
		scene.add(cam);

		HenyeyGreensteinLstar hg=new HenyeyGreensteinLstar();
		hg.setName("hg1");
		hg.setLstar("lstar_spectrum");
		hg.setG("g_spectrum");
		hg.setLa("la_spectrum");
		scene.add(hg);

		//use USAF resolution test
		{
			//create xml object:
			Surface surf=new Surface(scene.getPath());
			surf.setName(getPatternFilename());
			surf.setMaterials("lambert_white");
			surf.setFile(getPatternFilename());
			scene.add(surf);

			double interstice=0.010;
			//copy obj file to 
			TriangleMeshFrame mesh=new TriangleMeshFrame();		

			StringBuffer sb=TextFiles.readFile(getAbaque().getBase_path()+File.separator+this.getPatternFilename());
			if (sb==null)
			{
				System.out.println("Can't find pattern: "+getAbaque().getBase_path()+File.separator+this.getPatternFilename());
				return null;
			}
			String s=sb.toString();
			mesh.readOBJ(s);
			mesh.getFrame()._translate(new Vecteur(plateThickness+interstice,0,0));
			mesh.updateGlobal();
			TextFiles.saveString(scene.getPath()+File.separator+getPatternFilename(),mesh.saveToOBJ(),false);

			//surface material white
			Lambert matsurf=new Lambert();
			matsurf.setName("lambert_white");
			matsurf.setAlbedo("1");
			scene.add(matsurf);			
		}



		DecimalFormat df = new DecimalFormat("0.00000000000",new DecimalFormatSymbols(Locale.ENGLISH));
		{
			Volume v=new Volume();
			v.setName("product");
			v.setSurfaces("air_product product_glass");
			v.setN(df.format(this.getLayerRefractiveIndex()));
			v.setMaterials("hg1");
			scene.add(v);
		}
		{
			Volume v=new Volume();
			v.setName("glass");
			v.setSurfaces("product_glass glass_air");
			v.setN(df.format(glassRefactiveIndex));
			v.setMaterials("noscat");
			scene.add(v);
		}

		{
			Surface surf=new Surface(scene.getPath());
			surf.setName("air_product");
			surf.setMaterials("dielectric1");
			surf.setFile("air_product.obj");
			scene.add(surf);
		}
		{
			Surface surf=new Surface(scene.getPath());
			surf.setName("product_glass");
			surf.setMaterials("dielectric1");
			surf.setFile("product_glass.obj");
			scene.add(surf);
		}
		{
			Surface surf=new Surface(scene.getPath());
			surf.setName("glass_air");
			surf.setMaterials("dielectric1");
			surf.setFile("glass_air.obj");
			scene.add(surf);
		}
		OBJ_tools.createLayerOnPlateObjFiles2(scene.getPath(),0,getLayerThickness(),plateThickness,platedy,platedz,getLayerdy(),getLayerdz());


		//volume material for glass
		HenyeyGreensteinK hgVerre=new HenyeyGreensteinK();
		hgVerre.setName("noscat");
		hgVerre.setKs("0");
		hgVerre.setKa("0");
		hgVerre.setG("0");
		scene.add(hgVerre);

		//surface material for the product and the glass
		Dielectric dielectric=new Dielectric();
		dielectric.setName("dielectric1");
		scene.add(dielectric);

		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getLstarSpectrum());
			sp.setName("lstar_spectrum");
			scene.add(sp);
		}
		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getgSpectrum());
			sp.setName("g_spectrum");
			scene.add(sp);
		}
		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getLaSpectrum());
			sp.setName("la_spectrum");
			scene.add(sp);
		}

		XMLElement el=scene.toXML();
		String s=XMLFileStorage.toStringWithIndentation(el);
		return s;


	}


//
//	public  File createDir()
//	{
//		//Create directory 
//		File newDir=new File(this.getFullPath());
//		System.out.println("Create the directory:" +newDir.getAbsolutePath());
//		if (!newDir.exists()) newDir.mkdir();
//
//		Signal1D1DXY spectrumSource=new Signal1D1DXY(1);
//		spectrumSource.setValue(0, 650,1.0);
//		spectrumSource.setValue(0, 530,1.0);
//		spectrumSource.setValue(0, 450,1.0);
//		Signal1D1DXY spectrumLstar=new Signal1D1DXY(1);
//		spectrumLstar.setValue(0, 650,lstar);
//		Signal1D1DXY spectrumG=new Signal1D1DXY(1);
//		spectrumG.setValue(0, 650,g);
//		Signal1D1DXY spectrumLa=new Signal1D1DXY(1);
//		spectrumLa.setValue(0, 650,la);
//		String s=getSceneXML();
//		TextFiles.saveString(this.getFullPath()+File.separator+sceneFilename,s );
//		return newDir;
//	}
//
//
//	public  String  getFullPath()
//	{
//		DecimalFormat df=new DecimalFormat("0.00000",new DecimalFormatSymbols(Locale.ENGLISH));
//		String dir="plate_"+df.format(plateThickness)+"_g_"+df.format(g)+"_lstar_"+df.format(lstar)+"_e_"+df.format(layerThickness);
//		String path=abaque.getBase_path()+File.separator+dir;
//		return path;
//	}
//


	public XMLElement toXML() 
	{
		XMLElement el = super.toXML();
		el.setAttribute("plateThickness",plateThickness);
		el.setAttribute("platedy",platedy);
		el.setAttribute("platedz",platedz);
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		super.updateFromXML(xml);		
		plateThickness=xml.getDoubleAttribute("plateThickness",0);
		platedy=xml.getDoubleAttribute("platedy",150);
		platedz=xml.getDoubleAttribute("platedz",100);
	}


}
