package com.photonlyx.jstarlyx.abaque;

import java.io.File;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;

public abstract class SimulationStarlyxLayer extends SimulationStarlyx
{
	private Abaque abaque;

	private  String sceneFilename="scene.xml";
	private  String cameraName="rendering";
	private  int dim=100; //camera resolution
	private  int nbThreads=30;
	private  int nbPhotons=1000;
	private String sourceSpectrum="450 1 530 1 650 1";
	private String lstarSpectrum="450 1 530 1 650 1";
	private String gSpectrum="450 0 530 0 650 0";
	private String laSpectrum="450 1000 530 1000 650 1000";
	private String patternFilename="usaf1951.obj";
	private String cameraFilters="650 530 450";
	private double cameraField=10;//deg
	private double layerThickness=0.1;//mm;
	private double layerRefractiveIndex=1.45;
	private double layerdy=100;
	private double layerdz=40;

	
	//	public File createDir();
//	public String getCameraName();
//	public void setCameraName(String cameraName);
//	public String getSceneFilename();
//	public void setSceneFilename(String sceneFilename);	
//	public  String  getFullPath();
//	public XMLElement toXML();
//	public void updateFromXML(XMLElement xml);
	
	public SimulationStarlyxLayer(Abaque abaque)
	{
		super(abaque);
		this.abaque=abaque;	
	}
	
	public abstract String getSceneXML();
	public abstract String  getFullPath();
	
	public  File createDir()
	{
		//Create directory 
		File newDir=new File(this.getFullPath());
		System.out.println("Create the directory:" +newDir.getAbsolutePath());
		if (!newDir.exists()) newDir.mkdir();
		String s=getSceneXML();
		TextFiles.saveString(this.getFullPath()+File.separator+sceneFilename,s );
		return newDir;
	}



	public String getSceneFilename() {
		return sceneFilename;
	}


	public void setSceneFilename(String sceneFilename) {
		this.sceneFilename = sceneFilename;
	}

	public String getCameraName() {
		return cameraName;
	}

	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}

	public int getDim() {
		return dim;
	}

	public void setDim(int dim) {
		this.dim = dim;
	}

	public int getNbThreads() {
		return nbThreads;
	}

	public void setNbThreads(int nbThreads) {
		this.nbThreads = nbThreads;
	}

	public int getNbPhotons() {
		return nbPhotons;
	}

	public void setNbPhotons(int nbPhotons) {
		this.nbPhotons = nbPhotons;
	}

	public double getLayerThickness() {
		return layerThickness;
	}

	public void setLayerThickness(double layerThickness) {
		this.layerThickness = layerThickness;
	}


	public double getLayerRefractiveIndex() {
		return layerRefractiveIndex;
	}


	public void setLayerRefractiveIndex(double layerRefractiveIndex) {
		this.layerRefractiveIndex = layerRefractiveIndex;
	}




	public String getPatternFilename() {
		return patternFilename;
	}

	public void setPatternFilename(String patternFilename) {
		this.patternFilename = patternFilename;
	}

	public String getCameraFilters() {
		return cameraFilters;
	}

	public void setCameraFilters(String cameraFilters) {
		this.cameraFilters = cameraFilters;
	}
	
	
	
	
	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("sceneFilename",sceneFilename);
		el.setAttribute("nbThreads",nbThreads);
		el.setAttribute("nbPhotons",nbPhotons);
		el.setAttribute("cameraName",cameraName);
		el.setAttribute("cameraField",cameraField);
		el.setAttribute("dim",dim);//camera resolution
		
		el.setAttribute("SOURCE_SPECTRUM",sourceSpectrum);
	
		el.setAttribute("layerThickness",layerThickness);
		el.setAttribute("layerdy",layerdy);
		el.setAttribute("layerdz",layerdz);

		el.setAttribute("LSTAR_SPECTRUM",lstarSpectrum);
		el.setAttribute("G_SPECTRUM",gSpectrum);
		el.setAttribute("LA_SPECTRUM",laSpectrum);

		//		el.setAttribute("lstar",lstar);
		//		el.setAttribute("g",g);
		//		el.setAttribute("la",la);
		el.setAttribute("patternFilename",patternFilename);
		el.setAttribute("cameraFilters",cameraFilters);
		el.setAttribute("cameraField",cameraField);
		
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		sceneFilename=xml.getStringAttribute("sceneFilename","scene.xml");
		nbThreads=xml.getIntAttribute("nbThreads",6);
		nbPhotons=xml.getIntAttribute("nbPhotons",10000);
		cameraName=xml.getStringAttribute("cameraName","rendering");
		cameraField=xml.getDoubleAttribute("cameraField",10);
		dim=xml.getIntAttribute("dim",500);
		sourceSpectrum=xml.getStringAttribute("SOURCE_SPECTRUM","650 1");
		layerThickness=xml.getDoubleAttribute("layerThickness",0.1);
		layerdy=xml.getDoubleAttribute("layerdy",100);
		layerdz=xml.getDoubleAttribute("layerdz",40);
		//		lstar=xml.getDoubleAttribute("lstar",0);
		//		g=xml.getDoubleAttribute("g",0);
		//		la=xml.getDoubleAttribute("la",0);
		lstarSpectrum=xml.getStringAttribute("LSTAR_SPECTRUM","650 1");
		gSpectrum=xml.getStringAttribute("G_SPECTRUM","650 0");
		laSpectrum=xml.getStringAttribute("LA_SPECTRUM","650 1000");
		patternFilename=xml.getStringAttribute("PATTERNFILENAME","usaf1951.obj");
		cameraFilters=xml.getStringAttribute("cameraFilters","650");
		cameraField=xml.getDoubleAttribute("cameraField",10);
	}

	
	
	
	
	
	public String getSourceSpectrum() {
		return sourceSpectrum;
	}

	public void setSourceSpectrum(String sourceSpectrum) {
		this.sourceSpectrum = sourceSpectrum;
	}

	public String getLstarSpectrum() {
		return lstarSpectrum;
	}

	public void setLstarSpectrum(String lstarSpectrum) {
		this.lstarSpectrum = lstarSpectrum;
	}

	public String getgSpectrum() {
		return gSpectrum;
	}

	public void setgSpectrum(String gSpectrum) {
		this.gSpectrum = gSpectrum;
	}

	public String getLaSpectrum() {
		return laSpectrum;
	}

	public void setLaSpectrum(String laSpectrum) {
		this.laSpectrum = laSpectrum;
	}

	public double getCameraField() {
		return cameraField;
	}

	public void setCameraField(double cameraField) {
		this.cameraField = cameraField;
	}

	public double getLayerdy() {
		return layerdy;
	}

	public void setLayerdy(double layerdy) {
		this.layerdy = layerdy;
	}

	public double getLayerdz() {
		return layerdz;
	}

	public void setLayerdz(double layerdz) {
		this.layerdz = layerdz;
	}

	public Abaque getAbaque() {
		return abaque;
	}
	
	
	
	
	
	
	

}
