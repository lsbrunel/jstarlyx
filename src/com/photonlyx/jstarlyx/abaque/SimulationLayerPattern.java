package com.photonlyx.jstarlyx.abaque;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.Dielectric;
import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.HenyeyGreensteinLstar;
import com.photonlyx.jstarlyx.Lambert;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.SourceSun;
import com.photonlyx.jstarlyx.Spectrum;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.Volume;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3DFrame;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;

public class SimulationLayerPattern extends SimulationStarlyxLayer implements XMLstorable
{

	public SimulationLayerPattern(Abaque abaque)
	{
		super(abaque);
	}

	public  String getSceneXML()
	{
		Scene scene=new Scene(this.getFullPath());

		scene.setNbPhotons(getNbPhotons());
		scene.setAlgorithm("reverse_1");
		scene.setStoreTrajectories(0);
		scene.setStoreErrorTrajectories(0);
		scene.setNbThreads(getNbThreads());
		scene.setVerbose(0);

		SourceSun source=new SourceSun();
		source.setName("sun");
		source.setX(1);
		source.setY(0);
		source.setZ(0);
		source.setPower(1);
		source.setSpectrum("source_spectrum");
		scene.add(source);

		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getLstarSpectrum());
			sp.setName("source_spectrum");
			scene.add(sp);
		}

		Camera cam=new Camera();
		cam.setName(getCameraName());
		cam.setDimx(getDim());
		cam.setDimy(getDim());
		cam.setField(getCameraField());
		cam.setFilters(getCameraFilters());
		cam.setGamma(1);
		cam.setX(-300);
		cam.setY(0);
		cam.setZ(0);
		cam.setViewX(0);
		cam.setViewY(0);
		cam.setViewZ(0);
		scene.add(cam);

		HenyeyGreensteinLstar hg=new HenyeyGreensteinLstar();
		hg.setName("hg1");
		hg.setLstar("lstar_spectrum");
		hg.setG("g_spectrum");
		hg.setLa("la_spectrum");
		scene.add(hg);

		//pattern:
		{
			//create xml object:
			Surface surf=new Surface(scene.getPath());
			surf.setName(getPatternFilename());
			surf.setMaterials("lambert_pattern");
			surf.setFile(getPatternFilename());
			scene.add(surf);

			double interstice=0.010;
			//copy obj file to 
			TriangleMeshFrame mesh=new TriangleMeshFrame();		

			String s=TextFiles.readFile(getAbaque().getBase_path()+File.separator+this.getPatternFilename()).toString();
			mesh.readOBJ(s);
			mesh.getFrame()._translate(new Vecteur(interstice,0,0));
			mesh.updateGlobal();
			TextFiles.saveString(scene.getPath()+File.separator+getPatternFilename(),mesh.saveToOBJ(),false);
			{
			//surface material of pattern
			Lambert matsurf=new Lambert();
			matsurf.setName("lambert_pattern");
			matsurf.setAlbedo("0.8");
			scene.add(matsurf);
			}
			{
			//surface material of frame
			Lambert matsurf=new Lambert();
			matsurf.setName("lambert_frame");
			matsurf.setAlbedo("1");
			scene.add(matsurf);
			}
		}

		//frame lower:
		{
			Vecteur p1=new Vecteur (-getLayerThickness()-0.05,-getLayerdy()*0.6,-getLayerdz()*0.6-getLayerdz()*0.2);
			Vecteur p2=new Vecteur (-getLayerThickness()-0.05,-getLayerdy()*0.6,-getLayerdz()*0.6);
			Vecteur p3=new Vecteur (-getLayerThickness()-0.05,getLayerdy()*0.6,-getLayerdz()*0.6);
			Vecteur p4=new Vecteur (-getLayerThickness()-0.05,getLayerdy()*0.6,-getLayerdz()*0.6-getLayerdz()*0.2);
			Quadrilatere3D qq=new Quadrilatere3D(p1,p2,p3,p4);
			Quadrilatere3DFrame q=new Quadrilatere3DFrame(qq);
			TextFiles.saveString(scene.getPath()+File.separator+"frameLow.obj",q.getTriangles().saveToOBJ(),false);
			Surface surf=new Surface(scene.getPath());
			surf.setName("frameLow");
			surf.setMaterials("lambert_frame");
			surf.setFile("frameLow.obj");
			scene.add(surf);
		}

		//frame upper:
		{
			Vecteur p1=new Vecteur (-getLayerThickness()-0.05,-getLayerdy()*0.6,getLayerdz()*0.6+getLayerdz()*0.2);
			Vecteur p2=new Vecteur (-getLayerThickness()-0.05,-getLayerdy()*0.6,getLayerdz()*0.6);
			Vecteur p3=new Vecteur (-getLayerThickness()-0.05,getLayerdy()*0.6,getLayerdz()*0.6);
			Vecteur p4=new Vecteur (-getLayerThickness()-0.05,getLayerdy()*0.6,getLayerdz()*0.6+getLayerdz()*0.2);
			Quadrilatere3D qq=new Quadrilatere3D(p1,p2,p3,p4);
			Quadrilatere3DFrame q=new Quadrilatere3DFrame(qq);
			TextFiles.saveString(scene.getPath()+File.separator+"frameUp.obj",q.getTriangles().saveToOBJ(),false);
			Surface surf=new Surface(scene.getPath());
			surf.setName("frameUp");
			surf.setMaterials("lambert_frame");
			surf.setFile("frameUp.obj");
			scene.add(surf);
		}
		{
			Surface surf=new Surface(scene.getPath());
			surf.setName("product");
			surf.setMaterials("dielectric1");
			surf.setFile("product.obj");
			scene.add(surf);
		}

		DecimalFormat df = new DecimalFormat("0.00000000000",new DecimalFormatSymbols(Locale.ENGLISH));
		{
			Volume v=new Volume();
			v.setName("product");
			v.setSurfaces("product");
			v.setN(df.format(this.getLayerRefractiveIndex()));
			v.setMaterials("hg1");
			scene.add(v);
		}
		Box b=new Box(new Vecteur(-getLayerThickness(),-getLayerdy()/2,-getLayerdz()/2),getLayerThickness(),getLayerdy(),getLayerdz());
		TextFiles.saveString(scene.getPath()+File.separator+"product.obj",b.saveToOBJ(),false);


		//volume material for glass
		HenyeyGreensteinK hgVerre=new HenyeyGreensteinK();
		hgVerre.setName("noscat");
		hgVerre.setKs("0");
		hgVerre.setKa("0");
		hgVerre.setG("0");
		scene.add(hgVerre);

		//surface material for the product and the glass
		Dielectric dielectric=new Dielectric();
		dielectric.setName("dielectric1");
		scene.add(dielectric);

		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getLstarSpectrum());
			sp.setName("lstar_spectrum");
			scene.add(sp);
		}
		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getgSpectrum());
			sp.setName("g_spectrum");
			scene.add(sp);
		}
		{
			Spectrum sp=new Spectrum();
			sp.setData(this.getLaSpectrum());
			sp.setName("la_spectrum");
			scene.add(sp);
		}

		XMLElement el=scene.toXML();
		String s=XMLFileStorage.toStringWithIndentation(el);
		return s;


	}


	public  String  getFullPath()
	{
		DecimalFormat df=new DecimalFormat("0.00000",new DecimalFormatSymbols(Locale.ENGLISH));
		String dir="_g_"+getgSpectrum().replaceAll(" ","_")
					+"_lstar_"+getLstarSpectrum().replaceAll(" ","_")
					+"_la_"+getLaSpectrum().replaceAll(" ","_");
		String path=getAbaque().getBase_path()+File.separator+dir;
		return path;
	}

	
	
	


	public XMLElement toXML() 
	{
		return super.toXML();
	}

	public void updateFromXML(XMLElement xml)
	{
		super.updateFromXML(xml);
	}


}
