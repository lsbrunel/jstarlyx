package com.photonlyx.jstarlyx.abaque;

public class AbaqueApp 
{


	public static void main(String[] args) 
	{
		String base_path=".";

		//		//Create eventually base directory 
		//		File newDir=new File(base_path);
		//		System.out.println("Create the directory:"+newDir.getAbsolutePath());
		//		if (!newDir.exists()) newDir.mkdir();
		//
		//		
		//		Abaque abaque=new Abaque(base_path);
		//		Simulation s1=new Simulation();
		//		abaque.add(s1);
		//		
		//String s=XMLFileStorage.toStringWithIndentation(abaque.toXML());
		//		TextFiles.saveString(base_path+File.separator+"abaque.xml", s, true);

		if (args.length==0)
		{
			//			base_path="/home/laurent/temp/floutant";
			//			String xmlfilename="abaque.xml";
			//			Abaque abaque=new Abaque(base_path,xmlfilename);
			//			//launch the renderings:
			//			abaque.launchSimulations();
			//			//abaque.createLatex();
			printHelp();
		}
		else if ((args.length==2)&&(args[0].compareTo("-l")==0))
		{	
			System.out.println("Create latex project");
			base_path=".";
			String xmlfilename=args[1];
			Abaque abaque=new Abaque(base_path,xmlfilename);
			abaque.createLatex();
		}
		else if ((args.length==1)&&(args[0].compareTo("-h")==0))
		{	
			printHelp();
		}
		else if ((args.length==1)&&(args[0].compareTo("-e")==0))
		{	
			printExample();
		}
		else if ((args.length==2)&&(args[0].compareTo("-c")==0))
		{	
			System.out.println("Create StarLyx projects (only)");
			base_path=".";
			String xmlfilename=args[1];
			Abaque abaque=new Abaque(base_path,xmlfilename);
			abaque.createStarLyXprojects();
		}
		else if (args.length==1) 
		{	
			System.out.println("Create StarLyx projects and launch simulations");
			base_path=".";
			String xmlfilename=args[0];
			Abaque abaque=new Abaque(base_path,xmlfilename);
			//launch the renderings:
			abaque.launchSimulations();
		}
		else
		{
			printHelp();
		}



		//create pdf:
		//		Util.runCommand("cd "+base_path+" \n");
		//		Util.runCommand("pdflatex "+base_path+File.separator+latexDirName+File.separator+latexFileName);

		//		//open jstarlyx session:
		//		JstarlyxApp jstarlyxApp =new JstarlyxApp();
		//		jstarlyxApp.path=path;
		//		jstarlyxApp.getCJFrame().setExitAppOnExit(false);
		//		jstarlyxApp.getFileManager().open(jstarlyxApp.path,sceneFilename);
		//		jstarlyxApp.getTriggerListFile().trig();


	}


	private static void printHelp()
	{
		System.out.println("Usage:");
		System.out.println(" -h : help");
		System.out.println(" -e : show an example of xml file");
		System.out.println(" abaque.xml : create and launch StarLyx simulations");
		System.out.println(" -c abaque.xml : create directories and starlyx scenes only");
		System.out.println(" -l abaque.xml : only create latex project");

	}

	private static void printExample()
	{
		String s="";
		s+="<?xml version=\"1.0\"?> ";s+="\n";
		s+="<com.photonlyx.jstarlyx.abaque.Abaque>";s+="\n";
		s+="<com.photonlyx.jstarlyx.abaque.SimulationLayerPattern"			
				+ " SCENEFILENAME=\"scene.xml\" NBTHREADS=\"6\" "+ " NBPHOTONS=\"100\" "
				+ " CAMERANAME=\"rendering\"" + " CAMERAFILTERS=\"650\" "+ " CAMERAFIELD=\"10\" "+ " DIM=\"200\" "
				+ " SOURCE_SPECTRUM=\"450 1 530 1 650 1\"" 
				+ " LAYERTHICKNESS=\"0.100\"  " + " LAYERDY=\"100\"  LAYERDZ=\"40\" LAYERREFRACTIVEINDEX=\"1\" "
				+ " LA_SPECTRUM=\"650 100\" LSTAR_SPECTRUM=\"650 0.005\" "+ "G_SPECTRUM=\"650 0\"   "
				+ " PATTERNFILENAME=\"usaf1951.obj\" "			
				+ " > "			
				+ "  </com.photonlyx.jstarlyx.abaque.Simulation>";
		s+="<com.photonlyx.jstarlyx.abaque.SimulationLayerOnPlatePattern"			
				+ " SCENEFILENAME=\"scene.xml\" NBTHREADS=\"6\" "+ " NBPHOTONS=\"100\" "
				+ " CAMERANAME=\"rendering\"" + " CAMERAFILTERS=\"650\" "+ " CAMERAFIELD=\"10\" "+ " DIM=\"200\" "
				+ " SOURCE_SPECTRUM=\"450 1 530 1 650 1\"" 
				+ " PLATETHICKNESS=\"0.001\" PLATEDY=\"150\"  PLATEDZ=\"100\" "
				+ " LAYERTHICKNESS=\"0.100\"  " + " LAYERDY=\"100\"  LAYERDZ=\"40\" "
				+ " LA_SPECTRUM=\"650 100\" LSTAR_SPECTRUM=\"650 0.005\" "+ "G_SPECTRUM=\"650 0\"   "
				+ " PATTERNFILENAME=\"usaf1951.obj\" "			
				+ " > "			
				+ "  </com.photonlyx.jstarlyx.abaque.Simulation>";
		s+="\n";
		s+="\n";
		s+=" </com.photonlyx.jstarlyx.abaque.Abaque>";
		s+="\n";
		System.out.println(s);
	}



}
