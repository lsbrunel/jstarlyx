package com.photonlyx.jstarlyx.abaque;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.EmissionNada;
import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.Sensor;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.Spectrum;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.Volume;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;
import nanoxml.XMLElement;

public class SimulationNadaChandrasekhar extends SimulationStarlyx implements XMLstorable
{
	private double x,y,z; //probe pos
	private double wx,wy,wz;//probe dir
	private double g,ks;
	private double eta0,j;
	private double objectSize=10;
	private String objFilename="lapin2.obj";


	public SimulationNadaChandrasekhar(Abaque abaque)
	{
		super(abaque);
	}

	public  String getSceneXML()
	{
		Scene scene=new Scene(this.getFullPath());

		scene.setNbPhotons(this.getNbPhotons());
		scene.setAlgorithm("reverse_1");
		scene.setStoreTrajectories(0);
		scene.setStoreErrorTrajectories(0);
		scene.setNbThreads(6);
		scene.setVerbose(1);
		//scene.setMax_path_length(100000000);
		scene.setMax_nb_events(1000000);

//		Camera cam=new Camera();
//		cam.setName("cam0");
//		cam.setVolume("volume1");
//		cam.setDimx(1);
//		cam.setDimy(1);
//		cam.setField(0);
//		cam.setFilters("filterR");
//		cam.setGamma(1);
//		cam.setX(x);
//		cam.setY(y);
//		cam.setZ(z);
//		cam.setViewX(x-wx);//the probe points towards the opposite light direction 
//		cam.setViewY(y-wy);//the probe points towards the opposite light direction 
//		cam.setViewZ(z-wz);//the probe points towards the opposite light direction 
//		scene.add(cam);
		
		Sensor probe=new Sensor("");
		probe.setName("cam0");
		probe.setVolume("volume1");
		probe.setFilters("filterR");
		probe.setX(x);
		probe.setY(y);
		probe.setZ(z);
		probe.setViewPoint(new Vecteur(x-wx,y-wy,z-wz));//the probe points towards the opposite light direction 
		scene.add(probe);

		Spectrum spred=new Spectrum();
		spred.setName("filterR");
		spred.setData("650 1");
		scene.add(spred);

		HenyeyGreensteinK hg=new HenyeyGreensteinK();
		hg.setName("hg1");
		hg.setKs(ks+"");
		hg.setG(g+"");
		hg.setKa("0");
		scene.add(hg);

		Volume v=new Volume();
		v.setName("volume1");
		v.setSurfaces("surface1");
		v.setN("1");
		v.setMaterials("hg1");
		scene.add(v);

		Surface surface1=new Surface(this.getFullPath());
		surface1.setName("surface1");
		surface1.setMaterials("nada1");
		surface1.setObjfile("../"+objFilename);
		scene.add(surface1);

		//surface material for emission "Nada"
		EmissionNada nada1=new EmissionNada();
		nada1.setName("nada1");
		nada1.setEta0(""+eta0);
		nada1.setJ(""+j);
		scene.add(nada1);


		XMLElement el=scene.toXML();
		String s=XMLFileStorage.toStringWithIndentation(el);
		return s;

	}


	public  String  getFullPath()
	{
		DecimalFormat df=new DecimalFormat("0.00000",new DecimalFormatSymbols(Locale.ENGLISH));
		
//		String pos="c";
//		if (x==-3) pos="o";
//		String w="x";
//		if (wy==1) w="y";
//		if (wx==1.414) w="ob";
		
		String dir=this.getObjFilename()+"_nbPh_"+this.getNbPhotons()+"_ks_"+ks+"_g_"+g+"_eta0_"+eta0+"_j_"+j+"_"+this.getPos()+"_"+ this.getProbeDir();
		String path=getAbaque().getBase_path()+File.separator+dir;
		return path;
	}



	public XMLElement toXML() 
	{
		XMLElement el = new XMLElement();
		el.setName(getClass().toString().replace("class ", ""));
		el.setAttribute("nbPhotons",this.getNbPhotons());
		el.setAttribute("x",x);
		el.setAttribute("y",y);
		el.setAttribute("wx",wx);
		el.setAttribute("wy",wy);
		el.setAttribute("wz",wz);
		el.setAttribute("g",g);
		el.setAttribute("ks",ks);
		el.setAttribute("eta0",eta0);
		el.setAttribute("j",j);
		el.setAttribute("objFilename",objFilename);

		
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setNbPhotons(xml.getIntAttribute("nbPhotons",0));
		x=xml.getDoubleAttribute("x",0);
		y=xml.getDoubleAttribute("y",0);
		z=xml.getDoubleAttribute("z",0);
		wx=xml.getDoubleAttribute("wx",1);
		wy=xml.getDoubleAttribute("wy",0);
		wz=xml.getDoubleAttribute("wz",0);
		g=xml.getDoubleAttribute("g",0);
		ks=xml.getDoubleAttribute("ks",10);
		eta0=xml.getDoubleAttribute("eta0",0);
		j=xml.getDoubleAttribute("j",1);
		objFilename=xml.getStringAttribute("objFilename","lapin2.obj");
	}




	public String getObjFilename() {
		return objFilename;
	}

	public void setObjFilename(String objFilename) {
		this.objFilename = objFilename;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public double getWx() {
		return wx;
	}

	public void setWx(double wx) {
		this.wx = wx;
	}

	public double getWy() {
		return wy;
	}

	public void setWy(double wy) {
		this.wy = wy;
	}

	public double getWz() {
		return wz;
	}

	public void setWz(double wz) {
		this.wz = wz;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getKs() {
		return ks;
	}

	public void setKs(double ks) {
		this.ks = ks;
	}

	public double getEta0() {
		return eta0;
	}

	public void setEta0(double eta0) {
		this.eta0 = eta0;
	}

	public double getJ() {
		return j;
	}

	public void setJ(double j) {
		this.j = j;
	}



	private String getProbeDir() {
		return "(+"+wx+","+wy+","+wz+")";
	}

	private double getEO() {
		return objectSize*ks*(1-g);
	}

	private String getPos() {
		return "(+"+x+","+y+","+z+")";
	}

	private double solution()
	{
		double wx_=wx;//the direction of the radian intensity is the opposite of the probe direction
		return eta0/(4*Math.PI)+3/(4*Math.PI)*j*((g-1)*ks*x+wx_);
	}

	public static void main(String args[])
	{
		String base_path=".";
		//String base_path="/home/laurent/fabserver/cloud1/photonlyx/projets/2015_MC_rendering/starlyx/validation/nada_chandrasekhar/2022_10_12_lapin2";
		//String base_path="/home/laurent/fabserver/cloud1/photonlyx/projets/2015_MC_rendering/starlyx/validation/nada_chandrasekhar/2022_10_28_sphere";
		//String base_path="/home/laurent/temp/2022_10_12_lapin2";
		String xmlfilename=null;
//		String objFilename=null;

		if (args.length==0)
		{
			xmlfilename="abaque.xml";
//			objFilename="lapin2.obj";
		}	
		else if (args.length==1)
		{
			xmlfilename=args[0];
//			objFilename="lapin2.obj";
		}	
//		else if (args.length==2)
//		{
//			xmlfilename=args[0];
//			objFilename=args[1];
//		}	
		else
		{
			System.out.println("Usage: lyx_jstarlyx_abaque abaque.xml");
		}
		System.out.println("Abaque: "+xmlfilename );
		//System.out.println("OBJ: "+ objFilename);

		if (xmlfilename!=null)
		{
			StringBuffer sb=new StringBuffer();
			System.out.println("SimulationNadaChandrasekhar");
			Abaque abaque=new Abaque(base_path,xmlfilename);
			abaque.createStarLyXprojects();
			String sep="\t";
			String ss="objFile"+sep+"nbPhotons"+sep+"ks"+sep+"g"+sep+"EO"+sep+"eta0"+sep+"j"+sep+
					"pos"+sep+"dir"+sep+"starlyx"+sep+"2_sigma"+sep+"solution"+sep+"diff(sigmas)"+"\n";
			System.out.print(ss);
			sb.append(ss);
			DecimalFormat df=new DecimalFormat("0.000000");
			for (SimulationStarlyx s1:abaque)
			{
				SimulationNadaChandrasekhar s=(SimulationNadaChandrasekhar)s1;
				//s.setObjFilename(objFilename);
				//System.out.println("Run starlyx for scene: "+s.getFullPath());
				Scene.runStarlyx(s.getFullPath(),s.getSceneFilename());
				String[][] array=TextFiles.readCSV(s.getFullPath()+File.separator+"probes.csv", true, "\t",false);
				if (array==null) 
				{
					return ;
				}
				int c=0;
				double w=0,sig=0;
				for (String[] l:array)
				{
					//								for (String ss:l)
					//									System.out.print(ss+"\t");
					//								System.out.print("\n");
					if (c==2) 
					{
						w=Double.parseDouble(l[1]);
					}
					//								System.out.print("\n");
					if (c==3) 
					{
						sig=Double.parseDouble(l[1]);
					}
				c++;
				}
				ss=s.getObjFilename()+sep+s.getNbPhotons()
				+sep+df.format(s.getKs())
				+sep+df.format(s.getG())
				+sep+df.format(s.getEO())
				+sep+df.format(s.getEta0())
				+sep+df.format(s.getJ())
				+sep+s.getPos()
				+sep+s.getProbeDir()
				+sep+df.format(w)
				+sep+df.format(sig*2)
				+sep+df.format(s.solution())
				//+sep+df.format(w-s.solution())
				+sep+df.format((w-s.solution())/sig)
				+"\n";
				System.out.print(ss);
				sb.append(ss);


			}
			TextFiles.saveString(base_path+File.separator+"resultat.csv", sb.toString(), true);
		}

	}





}
