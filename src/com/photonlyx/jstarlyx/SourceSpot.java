package com.photonlyx.jstarlyx;

import java.awt.Color;
import java.util.Enumeration;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.ConeMesh;
import com.photonlyx.toolbox.math.geometry.DiskMesh;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.threeD.gui.Text3DColor;

import nanoxml.XMLElement;

public class SourceSpot extends Node implements XMLstorable
{
	private Vecteur pos=new Vecteur(-3,-3,3);
	private Vecteur dir=new Vecteur(1,0,0);
	private double power=1;
	private double radiance=1;//radiance W sr-1 m-2 used when angle=0
	private double diameter=3;
	private double angle=10;
	private String type="spot";
	private String spectrum="";
	private String volume="World";
	private static Color c=Color.red;


	public SourceSpot()
	{
		this.setName("laser0");
	}

	public SourceSpot(Node n)
	{
		super(n);
		this.setName("laser0");
	}

	@Override
	public Object3DColorSet getObject3D()
	{
		Scene scene=(Scene)this.getAncestorOfClass("Scene");
		double sceneSize=scene.getSceneSize();
		//System.out.println("sceneSize: "+sceneSize);
		Object3DColorSet set=new Object3DColorSet();
		//the cone:
		if (angle<180)
		{
			double hcone,dcone;
			if (angle<90)
			{
				hcone=sceneSize/10;
				dcone=2*hcone*Math.tan(angle/2*Math.PI/180);
			}
			else
			{
				dcone=sceneSize/10;
				hcone=dcone/(2*Math.tan(angle/2*Math.PI/180));				
			}
			ConeMesh cone=new ConeMesh(dcone,hcone,20);//represents the probe
			cone.getFrame().buildSuchThatZaxisIs(dir.scmul(-1));
			cone.getFrame()._translate(pos);
			cone.getFrame()._translate(cone.getFrame().z().scmul(-hcone));
			cone.updateGlobal();	
			set.add(new Object3DColorInstance(cone,c));
		}
		TriangleMeshFrame cyl=getBody();		
		set.add(new Object3DColorInstance(cyl,c));	
		//the axis:
		set.add(new Point3DColor(pos,c,5));
		set.add(new Segment3DColor(pos,pos.addn(cyl.getFrame().getAxis(2).scmul(diameter)),c));
		//name
		Vecteur textPos=pos.addn(cyl.getFrame().getAxis(2).scmul(-diameter*2));
		textPos._add(cyl.getFrame().getAxis(2).scmul(diameter*1.2));
		Text3DColor t=new Text3DColor(this.getName(),textPos,c);
		set.add(t);

		return set;
	}

	private TriangleMeshFrame getBody()
	{
		//	DiskMesh cyl=new DiskMesh(diameter,30);//represents the source disk
		//	cyl.getFrame().buildSuchThatZaxisIs(dir.scmul(1));
		//	cyl.getFrame()._translate(pos);
		//	cyl.updateGlobal();
		//	return cyl;
		DiskMesh d=new DiskMesh(diameter,20);//represents the probe
		d.getFrame().buildSuchThatZaxisIs(dir.scmul(1));
		d.getFrame()._translate(pos);
		d.updateGlobal();
		return d;
	}


	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("source");

		el.setAttribute("NAME", this.getName());
		el.setDoubleAttribute("POWER", power);
		el.setDoubleAttribute("RADIANCE", power);
		el.setDoubleAttribute("DIAMETER", diameter);
		el.setDoubleAttribute("ANGLE", angle);
		el.setAttribute("SPECTRUM", spectrum);
		el.setAttribute("TYPE", type);
		el.setAttribute("VOLUME", volume);

		//add pos:
		XMLElement elLens=pos.toXML();
		elLens.setName("pos");
		el.addChild(elLens);
		//add dir:
		XMLElement elDir=dir.toXML();
		elDir.setName("dir");
		el.addChild(elDir);

		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		power=xml.getDoubleAttribute("POWER",1);
		power=xml.getDoubleAttribute("RADIANCE",1);
		diameter=xml.getDoubleAttribute("DIAMETER",0);
		angle=xml.getDoubleAttribute("ANGLE",0);
		type=xml.getStringAttribute("TYPE","");
		spectrum=xml.getStringAttribute("SPECTRUM","");
		volume=xml.getStringAttribute("VOLUME","World");

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("pos"))
			{
				pos.updateFromXML(el);
			}
			if (el.getName().contains("dir"))
			{
				dir.updateFromXML(el);
			}

		}
	}



	public Vecteur getPos()
	{
		return pos;
	}


	public Vecteur getDir() {
		return dir;
	}

	public double getX() {
		return pos.x();
	}

	public void setX(double x) {
		pos.setX( x);
	}

	public double getY() {
		return pos.y();
	}

	public void setY(double y) {
		pos.setY (y);
	}

	public double getZ() {
		return pos.z();
	}

	public void setZ(double z) {
		pos.setZ (z);
	}

	public void setPower(double d) {
		power=d;

	}
	public void setDir(Vecteur dir) {
		this.dir = dir;
	}
	public void setPos(Vecteur pos) {
		this.pos = pos;
	}



	public double getDiameter() {
		return diameter;
	}

	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public double getPower() {
		return power;
	}

	public String getSpectrum() {
		return spectrum;
	}

	public void setSpectrum(String s) {
		spectrum=s;

	}


	public double getRadiance() {
		return radiance;
	}

	public void setRadiance(double radiance) {
		this.radiance = radiance;
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (int k=0;k<3;k++) 
		{
			double r=pos.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}			
		getBody().checkOccupyingCube(cornerMin, cornerMax);


	}







}
