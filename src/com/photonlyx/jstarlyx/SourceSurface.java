package com.photonlyx.jstarlyx;

import java.awt.Color;
import java.io.File;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.TextFiles;

import nanoxml.XMLElement;

public class SourceSurface extends Node implements XMLstorable
{
	private double power=1;
	private double diameter=3;
	private double angle=10;
	private String type="surface";
	private String spectrum="";
	private String volume="World";
	private String file;
	
	private String path;//path to the obj file
	private TriangleMeshFrame mesh=new TriangleMeshFrame();
	private static Color c=Color.red;
	
	
	public SourceSurface(String path)
	{
		this.setName("laser0");
		this.path=path;
	}

	public SourceSurface(Node father,String path)
	{
		super(father);
		this.setName("laser0");
		this.path=path;
	}
	
	private void readMeshFile()
	{
		if (file.endsWith(".obj"))
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+file);
			mesh.readOBJ(sb.toString());
		}
		else
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+file);
			mesh.readSTL(sb.toString());
			//mesh.readSTLinBinaryFile(Global.path, file);
		}
	}
	
	
	@Override
	public Object3DColorSet getObject3D()
	{
		this.readMeshFile();
		Object3DColorSet setc=new Object3DColorSet();		
		for (Triangle3D t:mesh.getTriangles()) 
		{
			Triangle3DColor tc=new Triangle3DColor(t,c,true);
			setc.add(tc);
		}
		return setc;
	}
	
	
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("source");

		el.setAttribute("name", this.getName());
		el.setDoubleAttribute("POWER", power);
		el.setDoubleAttribute("DIAMETER", diameter);
		el.setDoubleAttribute("angle", angle);
		el.setAttribute("SPECTRUM", spectrum);
		el.setAttribute("TYPE", type);
		el.setAttribute("VOLUME", volume);
		el.setAttribute("FILE", file);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		power=xml.getDoubleAttribute("POWER",1);
		diameter=xml.getDoubleAttribute("DIAMETER",0);
		angle=xml.getDoubleAttribute("angle",0);
		type=xml.getStringAttribute("TYPE","");
		spectrum=xml.getStringAttribute("SPECTRUM","");
		volume=xml.getStringAttribute("VOLUME","World");
		file=xml.getStringAttribute("FILE");
	}



	public void setPower(double d) {
		power=d;
		
	}
	
	

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public double getPower() {
		return power;
	}

	public String getSpectrum() {
		return spectrum;
	}

	public void setSpectrum(String s) {
		spectrum=s;
		
	}

	
	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		mesh.checkOccupyingCube(cornerMin, cornerMax);
	}




}
