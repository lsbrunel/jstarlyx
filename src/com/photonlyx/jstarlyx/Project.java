package com.photonlyx.jstarlyx;

import java.util.Enumeration;

import com.photonlyx.jstarlyx.gui.JstarlyxApp;
import com.photonlyx.toolbox.util.Global;

import nanoxml.XMLElement;

public class Project extends Node
{
	//private String path="./";

	public Project()
	{
		super(null);
	}
	
//	public Project(String path)
//	{
//		super(null);
//		this.path=path;
//	}
//	
//	public Project(Node n,String path)
//	{
//		super(n);
//		this.path=path;
//	}
	


	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Project");
		for (Node n:this.getSons()) 
		{
			el.addChild(n.toXML());
		}
		return el;
	}

	public void updateFromXML(XMLElement xml)
	{
		this.free();
		if (xml==null) 
		{
			return;
		}
		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			Node n=null;
			if (el.getName().compareTo("Scene")==0) n=new Scene(this);	
			if (el.getName().compareTo("Variation")==0) n=new Variation(this);	
			if (n!=null)
			{
				n.updateFromXML(el);
				this.addSon(n);
			}
		}
	}
//	public String getPath() {
//		return path;
//	}
//
//	public void setPath(String path) {
//		this.path = path;
//	}
//

}
