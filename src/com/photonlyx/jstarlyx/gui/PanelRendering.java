package com.photonlyx.jstarlyx.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.File;
import java.util.Vector;

import javax.swing.JPanel;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.hdr.HyperSpectralImage;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.imageGui.PanelImageWithPanelSide;
import com.photonlyx.toolbox.math.image.CImageProcessing;
import com.photonlyx.toolbox.math.signal.Signal2D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

public class PanelRendering extends JPanel
{
	private HyperSpectralImage  hyperSpectralImage=new HyperSpectralImage();
	private ParamsBox paramsBoxFactor;
	private Scene scene;
	private WindowApp app;
	private JPanel panelNorth=new JPanel();
	private PanelImageWithPanelSide panelRGB=new PanelImageWithPanelSide();
	private ChartPanel cp=new ChartPanel();
	private static int VIEW_RGB=0;
	private static int VIEW_CHANNEL=1;
	public String[] viewOptions= {"rgb","channel"};
	private String view=viewOptions[0];
	private static int SIGMA_WEIGHT=0;
	private static int SIGMA_SIGMA=1;
	private static int SIGMA_SIGMA_PC=2;
	public String[] sigmaOptions= {"weight","sigma","sigma%"};
	private String sigma=sigmaOptions[0];
	private Signal2D1D signal2D1D=new Signal2D1D();//view of a channel
	private String cameraName;

	public PanelRendering(WindowApp app,Scene os)
	{
		this.app=app;
		this.scene=os;

		this.setLayout(new BorderLayout());
		panelNorth.setBackground(Global.background);
		panelNorth.setPreferredSize(new Dimension(0,50));
		panelRGB.setBackground(Global.background);
		panelRGB.getPanelSide().setPreferredSize(new Dimension(150,0));

		cp.setSignal2D1D(signal2D1D);
		//cp.hideLegend();
		cp.setPaletteVisible(true);
		cp.getChart().setKeepXYratio(true);
		cp.getChart().setGridLinex(false);
		cp.getChart().setGridLiney(false);

		//		loadHDRandUpdate();
		updateGUI();

	}

	public String[] cameraNameOptions()
	{
		Vector<Node> cams=scene.getNodesOfClass("Camera");
		String[] s=new String[cams.size()];
		int i=0;
		for (Node n:cams) s[i++]=((Camera)n).getName();
		return s;
	}

	public void loadHDRImage()
	{
		//Camera cam=(Camera)scene.getNodeOfClass("Camera");
		//if (cam==null) return;
		if (cameraName==null) return;
		String file=cameraName+".raw";
		String file_sigma=cameraName+"_sigma.raw";
		System.out.println("Load HDR image "+app.path+File.separator+file);
		File f=new File(app.path+File.separator+file);
		File f_sigma=new File(app.path+File.separator+file_sigma);
		System.out.println("sigma option= "+sigma);

		if (!f.exists())
		{
			hyperSpectralImage.init();
			return;
		}

		if  (sigma==sigmaOptions[SIGMA_WEIGHT])//see weights
		{
			if (f.exists()) hyperSpectralImage.load(app.path, file,true);
		}
		else if  (sigma==sigmaOptions[SIGMA_SIGMA])//see sigmas
		{
			if (f_sigma.exists()) hyperSpectralImage.load(app.path,file_sigma,true);
		}
		else if  (sigma==sigmaOptions[SIGMA_SIGMA_PC])//see sigma/weight
		{
			HyperSpectralImage  w=new HyperSpectralImage();
			if (f_sigma.exists()) w.load(app.path,file,true);
			//HyperSpectralImage  s=new HyperSpectralImage();
			if (f_sigma.exists()) hyperSpectralImage.load(app.path,file_sigma,true);
			hyperSpectralImage.div_(w);
			//s.mul_(100);
		}
	}

	public void updateChannel()
	{
		signal2D1D.copy(hyperSpectralImage.getChannel(hyperSpectralImage.getWaveLength()));	
		signal2D1D._flipAroundX();

	}

	public void clean()
	{
		hyperSpectralImage.init();
		updateGUI();			
	}

	public void loadHDRandUpdate()
	{
		//check if there is a rendering:
		if (cameraNameOptions().length==0) 
		{
			//no rendering
			hyperSpectralImage.init();
			updateGUI();	
			//return;
		}
		else
		{	
			//there is a rendering
			loadHDRImage();
			updateChannel();
			updateGUI();
		}
		cp.update();
	}

	public void generatePNGs()
	{
		String save=cameraName;
		String[] ss=cameraNameOptions();
		for (String name:ss)
		{
			cameraName=name;
			loadHDRImage();
			if (hyperSpectralImage.getW()*hyperSpectralImage.getH()==0) continue;
			CImageProcessing cim=hyperSpectralImage.getCImageProcessing();
			cim.saveImage(app.path,cameraName+".png",0);
		}
		cameraName=save;
	}

	public void updateGUI()
	{
		this.removeAll();

		panelNorth.removeAll();
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"loadHDRandUpdate"));
			tl.add(new Trigger(paramsBoxFactor,"update"));
			String[] names={"cameraName"};
			ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.HORIZONTAL,240,30);
			panelNorth.add(pb.getComponent());
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateGUI"));
			String[] names={"view"};
			ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.HORIZONTAL,140,30);
			panelNorth.add(pb.getComponent());
		}


		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"loadHDRandUpdate"));
			tl.add(new Trigger(paramsBoxFactor,"update"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(100,15));
			cButton.setText("Update");
			panelNorth.add(cButton);
		}

		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"generatePNGs"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(120,15));
			cButton.setText("Generate png(s)");
			panelNorth.add(cButton);
		}

		this.add(panelNorth,BorderLayout.NORTH);



		if (view==viewOptions[VIEW_RGB])
		{
			this.add(panelRGB,BorderLayout.CENTER);
			panelRGB.getPanelSide().removeAll();

			{
				String[] names={"gamma"};
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"loadHDRandUpdate"));
				tl.add(new Trigger(panelRGB,"update"));
				paramsBoxFactor=new ParamsBox(hyperSpectralImage,tl,null,names,null,null,ParamsBox.VERTICAL,140,30);
				panelRGB.getPanelSide().add(paramsBoxFactor.getComponent());
			}
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"loadHDRImage"));
				tl.add(new Trigger(paramsBoxFactor,"update"));
				String[] names={"auto"};
				String[] labels={"Factor auto"};
				ParamsBox pb=new ParamsBox(hyperSpectralImage,tl,null,names,labels,null,ParamsBox.VERTICAL,140,30);
				panelRGB.getPanelSide().add(pb.getComponent());
			}
			if (!hyperSpectralImage.isAuto())
			{
				String[] names={"factor"};
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"loadHDRandUpdate"));
				tl.add(new Trigger(panelRGB,"update"));
				paramsBoxFactor=new ParamsBox(hyperSpectralImage,tl,null,names,null,null,ParamsBox.VERTICAL,140,30);
				panelRGB.getPanelSide().add(paramsBoxFactor.getComponent());
			}

			this.add(panelRGB,BorderLayout.CENTER);
			if (paramsBoxFactor!=null) paramsBoxFactor.update();
			if ((hyperSpectralImage.getW()==0)||(hyperSpectralImage.getH()==0)) return;
			this.panelRGB.getPanelImage().setImage(hyperSpectralImage.getCImageProcessing());
			this.panelRGB.getPanelImage().update();
			panelRGB.repaint();
		}
		else if (view==viewOptions[VIEW_CHANNEL])
		{
			this.add(cp,BorderLayout.CENTER);
			cp.getPanelSide().removeAll();			
			if (hyperSpectralImage.waveLengthOptions!=null)
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"updateChannel"));
				tl.add(new Trigger(cp.getChart(),"update"));
				String[] names={"waveLength"};
				paramsBoxFactor=new ParamsBox(hyperSpectralImage,tl,null,names,null,null,ParamsBox.VERTICAL,140,30);
				cp.getPanelSide().add(paramsBoxFactor.getComponent());
			}	
			{
				TriggerList tl=new TriggerList();
				tl.add(new Trigger(this,"loadHDRImage"));
				tl.add(new Trigger(this,"updateChannel"));
				tl.add(new Trigger(cp.getChart(),"update"));
				String[] names={"sigma"};
				ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.HORIZONTAL,140,30);
				//panelNorth.add(pb.getComponent());
				cp.getPanelSide().add(pb.getComponent());
			}
		}
		this.validate();
		this.repaint();
	}

	public HyperSpectralImage getMultiSpectralImage() 
	{
		return hyperSpectralImage;
	}



	public String getView() {
		return view;
	}



	public void setView(String s) 
	{
		view=s;
		//		for (int i=0;i<viewOptions.length;i++)
		//		{
		//			if (viewOptions[i].compareTo(view)==0) view=viewOptions[i];
		//		}
	}



	public String getSigma() {
		return sigma;
	}

	public void setSigma(String sigma) {
		this.sigma = sigma;
	}

	public String getCameraName() {
		return cameraName;
	}

	public void setCameraName(String cameraName) {
		this.cameraName = cameraName;
	}


}
