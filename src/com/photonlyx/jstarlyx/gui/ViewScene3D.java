package com.photonlyx.jstarlyx.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;

import javax.swing.JTabbedPane;

import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Paths;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.ColorChooserFrame;
import com.photonlyx.toolbox.gui.ParStringBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;

public class ViewScene3D extends Graph3DPanel
{
	private boolean has=true;
	private String filter="all";
	private boolean segments=true;
	private int dotSize=0;
	private int opacity=128;

	private Scene scene;
	private Paths paths;
	private ParamsBox pbFilter,pbPaths;
	private String backgroundColor="255 255 255";
	private String pathsColor="255 100 100";

	public ViewScene3D(Scene scene)
	{
		this.scene=scene;
		this.getPanelSide().setPreferredSize(new Dimension(250,0));
		//set default view option in function of the algorithm:
		adaptFilter2Scene();
		//button to reload:
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(scene,"loadFromFile"));
			tl.add(new Trigger(this,"load"));
			tl.add(new Trigger(this,"update"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(140,20));
			cButton.setText("Reload scene");
			this.getPanelSide().add(cButton);
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update"));
			String[] names={"has","filter"};
			String[] labels={"has","filter"};
			pbFilter=new ParamsBox(this,tl,null,names,labels,null,ParamsBox.VERTICAL,240,30);
			this.getPanelSide().add(pbFilter);
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update"));
			String[] names={"segments","dotSize"};
			String[] labels={"segments","dot"};
			pbPaths=new ParamsBox(this,tl,null,names,labels,null,ParamsBox.VERTICAL,240,30);
			this.getPanelSide().add(pbPaths);
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update"));
			String[] names={"opacity","backgroundColor","pathsColor"};
			String[] labels={"opacity","backg.","paths"};
			pbPaths=new ParamsBox(this,tl,null,names,labels,null,ParamsBox.VERTICAL,240,30);
			this.getPanelSide().add(pbPaths);
			ParStringBox pb=(ParStringBox)pbPaths.getParamBoxOfParam("backgroundColor");
			pb.setFieldLength(8);
			ParStringBox pb1=(ParStringBox)pbPaths.getParamBoxOfParam("pathsColor");
			pb1.setFieldLength(8);
		}
		this.getPanelSide().validate();
		load();

		update();
	}
	
	
	/**
	 * set default view option in function of the algorithm:
	 */
	public void adaptFilter2Scene()
	{
		String algo=scene.getAlgorithm();
		if (algo!=null)
		{
			if (algo.compareTo("direct")==0)
			{
				has=false;
				filter="PATH_GOES_TO_INFINITE";
			}
			if (algo.compareTo("reverse")==0)
			{
				has=true;
				filter="w!=0";
			}
		}
		if (pbFilter!=null) pbFilter.update();
	}

	public void load() 
	{
		//load the paths:
		paths=new Paths(scene);
		paths.load();
	}

	public String[] filterOptions()
	{
		String[] ss={"all","w!=0","PATH_GOES_TO_INFINITE","RAY_ENTERED_A_SENSOR"};
		return ss;
	}

	
	private Color decodeColor(String s)
	{
		Definition def=new Definition(s);
		Color c=new Color(Integer.parseInt(def.word(0)),Integer.parseInt(def.word(1)),Integer.parseInt(def.word(2)));
		return c;
	}
	
	public void update()
	{
		this.setBackground(decodeColor(backgroundColor));
		this.setAxesLength(scene.getSceneSize()/2);
		this.removeAll3DObjects();

		for (Node n:scene.getSons()) 
		{
			Object3DColorSet os=n.getObject3D();
			if (os!=null)
			{
				this.add(os);
			}
		}

		if (paths!=null)
		{
			//Paths paths3=paths.filter(hasW,onlyPathsWithNonZeroWeight);
			Paths paths3=paths.filter(has,filter);
			Color c1=decodeColor(pathsColor);
			Color c=new Color(c1.getRed(),c1.getGreen(),c1.getBlue(),opacity);
			this.add(paths3.getView(dotSize,segments,c));
		}
		super.update();

	}		




	public boolean isHas() {
		return has;
	}

	public void setHas(boolean has) {
		this.has = has;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public boolean isSegments() {
		return segments;
	}


	public void setSegments(boolean segments) {
		this.segments = segments;
	}


	public int getDotSize() {
		return dotSize;
	}


	public void setDotSize(int dotSize) {
		this.dotSize = dotSize;
	}


	public int getOpacity() {
		return opacity;
	}


	public void setOpacity(int opacity) {
		this.opacity = opacity;
	}


	public String getBackgroundColor() {
		return backgroundColor;
	}


	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}


	public String getPathsColor() {
		return pathsColor;
	}


	public void setPathsColor(String pathsColor) {
		this.pathsColor = pathsColor;
	}


	public static void main(String[] args)
	{
		Scene scene=new Scene();
		String filename="scene.xml";
		String path="./";

		if (args.length==0)
		{
			//String filename="scene.xml";
			//String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2012_Dior/2024/2024_03_26_test_rendus_surfaces_rugueuses/2024_04_22_gonio_surface_rugueuse";
			filename="scene_direct.xml";
			path="/home/laurent/fabserver/cloud1/photonlyx/projets/2015_MC_rendering/starlyx/projets_de_test/2024_04_30_compare_reverse_direct_slab";
		}

		else if (args.length==1)
		{
			filename=args[0];
			path="./";
			scene.setPath(path);
			scene.setFileName(filename);
		}

		CJFrame cjframe	=new CJFrame();
		cjframe.setTitle(scene.getPath()+File.separator+scene.getFileName());
		cjframe.setLocation(400,50);
		cjframe.setSize(new Dimension(800,800));
		cjframe.setExitAppOnExit(true);
		JTabbedPane topTabPane=new JTabbedPane();
		cjframe.add(topTabPane);
		//load the scene:
		scene.setPath(path);
		scene.setFileName(filename);
		scene.loadFromFile();
		ViewScene3D v=new ViewScene3D(scene);
		topTabPane.add(v.getComponent());
		v.initialiseProjectorAuto();			
		
		Paths paths3=v.paths.filter(false,"PATH_GOES_TO_INFINITE");
		System.out.println(paths3);

	}


	public Graph3DPanel getGraph3DPanel() 
	{
		return this;
	}



}
