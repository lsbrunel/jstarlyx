package com.photonlyx.jstarlyx.gui;

import java.awt.Color;
import java.io.File;
import java.util.Vector;

import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.ColorList;
import com.photonlyx.toolbox.math.signal.Signal1D1DXY;
import com.photonlyx.toolbox.txt.TextFiles;
import com.plx.physics.color.CalcRGBFromSpectrum;

/**
 * parsing of starlyx output
 * @author laurent
 *
 */
public class SensorData
{
	private String name="";
	private Vector<Double> lambdas=new Vector<Double>();//wavelengths	
	private Vector<Double> weights=new Vector<Double>();//sensor signals
	private Vector<Double> sigmas=new Vector<Double>();	//rms of sensor signal

	public SensorData(String name)
	{
		this.name=name;
	}

	public String getName() {
		return name;
	}

	public Vector<Double> getLambdas() {
		return lambdas;
	}

	public Vector<Double> getWeights() {
		return weights;
	}

	public Vector<Double> getSigmas() {
		return sigmas;
	}


	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		sb.append("Name: "+name+"\n");
		sb.append("Lambdas: ");
		for (double lambda:lambdas) sb.append(lambda+" ");sb.append("\n");
		sb.append("Weights: ");
		for (double w:weights) sb.append(w+" ");sb.append("\n");
		sb.append("Sigmas: ");
		for (double s:sigmas) sb.append(s+" ");sb.append("\n");
		return sb.toString();
	}
	
	
	
	

	public  static Vector<SensorData> readSensorsCSV(String path,String filename)
	{
		String[][] array=TextFiles.readCSV(path+File.separator+filename,true,"\t",true);
		if (array==null) return null;
		Vector<SensorData> data;
		if (array[0][0].compareTo("reverse_path")==0) data=read4Reverse(array);
		else data=read4Direct(array);
		return data;
	}

	private  static Vector<SensorData> read4Direct(String[][] array)
	{
		if (array==null) return null;
		String[] labels=array[0];
		int nbLambdas=labels.length-1;//nb wavelengths
		int n=((array.length)-1)/2;//nb surfaces
		Vector<SensorData> data=new Vector<SensorData>();
		//System.out.println(n+" surfaces. Spectra of dim "+nbLambdas);
		//get the wavelengths:
		double[] lambdas=new double[nbLambdas];
		System.out.println();
		for (int j=0;j<nbLambdas;j++) 
			{
			//System.out.println(array[0][1+j]);
			lambdas[j]=Double.parseDouble(array[0][1+j]);
			}
		//for (int j=0;j<nbLambdas;j++) System.out.print( lambdas[j]+" ");
		//System.out.println();
		for (int i=0;i<n;i++) //scan rows (surfaces)
		{
			String sensor_name=array[2*i+1][0].replaceAll("_weight","");
			SensorData sd=new SensorData(sensor_name);
			data.add(sd);
			for (int j=0;j<nbLambdas;j++)//scan lambda
			{
				sd.getLambdas().add(lambdas[j]);
				//System.out.println(i+" "+j+" lambda="+lambdas[j]+"  w:"+array[i+1][1+j*2]+" s:"+array[i+1][1+j*2+1]);
				double w=Double.parseDouble(array[2*i+1][1+j]);
				double s=Double.parseDouble(array[2*i+2][1+j]);
				sd.getWeights().add(w);
				sd.getSigmas().add(s);
			}
		}
		return data;
	}

	private  static Vector<SensorData> read4Reverse(String[][] array)
	{
		if (array==null) return null;
		int nbLambdas=array[1].length-1;//nb wavelengths
		int nbSurfaces=((array.length-1))/3;//nb surfaces
		Vector<SensorData> data=new Vector<SensorData>();
		//System.out.println(nbSurfaces+" surfaces. Spectra of dim "+nbLambdas);
		for (int i=0;i<nbSurfaces;i++) //scan rows (surfaces)
		{
			String sensor_name=array[3*i+1][0].replaceAll("_weight","");
			SensorData sd=new SensorData(sensor_name);
			data.add(sd);
			for (int j=0;j<nbLambdas;j++)//scan lambda
			{
				double l=Double.parseDouble(array[1+3*i+0][1+j]);
				sd.getLambdas().add(l);
				//System.out.println(i+" "+j+" lambda="+lambdas[j]+"  w:"+array[i+1][1+j*2]+" s:"+array[i+1][1+j*2+1]);
				double w=Double.parseDouble(array[1+3*i+1][1+j]);
				double s=Double.parseDouble(array[1+3*i+2][1+j]);
				sd.getWeights().add(w);
				sd.getSigmas().add(s);
			}
		}
		//for (SensorData sensor:data) System.out.println(sensor);
		return data;
	}


	public static Plot[] showSpectra(String path,String filename)
	{
		Vector<SensorData> data=SensorData.readSensorsCSV( path, filename);
		if (data==null) return null;

		Plot[] plots=new Plot[data.size()];
		ColorList cl=new ColorList();
		int i=0;
		for (SensorData sensor:data)
		{
			Signal1D1DXY sig=new Signal1D1DXY();
			Signal1D1DXY sigma=new Signal1D1DXY();
			plots[i]=new Plot(sig);
			plots[i].setError(sigma);
			//String sensor_name=array[2*i+1][0].replaceAll("_weight","");
			String sensor_name=sensor.getName();
			plots[i].setLabel(sensor_name);
			plots[i].setColor(cl.colour(i));
			int j=0;
			for (double lambda:sensor.getLambdas())//scan lambda
			{
				sig.addPoint(lambda,sensor.getWeights().get(j));
				sigma.addPoint(lambda,sensor.getSigmas().get(j));
				j++;
			}
			i++;
		}
		return plots;
	}

	/**
	 * the plot are the weights versus surface number. One plot for each wavelength
	 * @param path
	 * @param filename
	 * @return
	 */
	public static Plot[] showSurfacesOnXaxis(String path,String filename)
	{
		Vector<SensorData> data=SensorData.readSensorsCSV( path, filename);
		int nbLambdas=data.get(0).getLambdas().size();
		int nbSurfaces=data.size();
		System.out.println(nbSurfaces+" surfaces. Spectra of dim "+nbLambdas);
		System.out.println();
		Plot[] plots=new Plot[nbLambdas];
		int i=0;
		for (double lambda:data.get(0).getLambdas())
		{
			System.out.println("lamda: "+lambda);
			Signal1D1DXY weight=new Signal1D1DXY();
			Signal1D1DXY sigma=new Signal1D1DXY();
			plots[i]=new Plot(weight);
			plots[i].setError(sigma);
			plots[i].setLabel(lambda+"");
			plots[i].setColor(getPureColor(lambda));
			//System.out.println("surface: "+array[i][0]);
			for (int j=0;j<nbSurfaces;j++)//scan surfaces
			{
				weight.addPoint(j,data.get(j).getWeights().get(i));
				sigma.addPoint(j,data.get(j).getSigmas().get(i));
			}
			i++;
		}
		return plots;
	}



	public static  Color getPureColor(double lambda_nm)
	{
		double[] xyz=CalcRGBFromSpectrum.calcXYZ(lambda_nm);
		double[] rgb=new double[3];
		convert(rgb,xyz);
		//Formulae.XYZ2RGBAdobe(xyz, rgb);
		Color c=new Color((int)rgb[0],(int)rgb[1],(int)rgb[2]);
		return c;
	}


	/**color code convertion*/
	private static void convert(double RGB[],double[] XYZ)
	{
		double X=XYZ[0];
		double Y=XYZ[1];
		double Z=XYZ[2];
		double R =  3.2404542*X - 1.5371385*Y - 0.4985314*Z;
		double G = -0.9692660*X + 1.8760108*Y + 0.0415560*Z;
		double B =  0.0556434*X - 0.2040259*Y + 1.0572252*Z;
		R*=255;
		G*=255;
		B*=255;
		R=Math.max(0,R );
		G=Math.max(0,G );
		B=Math.max(0,B );
		R=Math.min(255,R );
		G=Math.min(255,G );
		B=Math.min(255,B );
		RGB[0]=R;
		RGB[1]=G;
		RGB[2]=B;
	}

}