package com.photonlyx.jstarlyx.gui;

import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.StringReader;
import java.net.URI;
import java.util.Vector;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.SourcePoint;
import com.photonlyx.jstarlyx.SourceSun;
import com.photonlyx.jstarlyx.Spectrum;
import com.photonlyx.jstarlyx.StarLyxBox;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.Volume;
import com.photonlyx.toolbox.chart.*;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.InputDialog;
import com.photonlyx.toolbox.gui.ParStringBox;
import com.photonlyx.toolbox.gui.ParStringComboBox;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.TaskButton2;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.gui.menu.CJMenuFile;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMesh;
import com.photonlyx.toolbox.math.geometry.TriangleMeshSource;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.Definition;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;
import nanoxml.XMLElement;

/**
 * User interface to use starlyx engine
 * @author laurent
 */
public class JstarlyxApp extends WindowApp implements ActionListener
{
	private Vector<String> examples=new Vector<String>();
	private Scene scene=new Scene(this.path);
	//3D viewer:
	//private PanelScene3D panel3D=new PanelScene3D(this,this.scene);
	private ViewScene3D viewScene3D=new ViewScene3D(scene);
	//rendering view:
	private ChartPanel cp=new ChartPanel();
	private PanelRendering panelRendering=new PanelRendering(this,scene);

	public static String  starlyxPath=getStarlyxPath();

	private ParStringComboBox comboBoxObjects;
	private String selectedObjectName="";
	private JPanel paramPanel=new JPanel();
	private TriggerList tlFile=new TriggerList();
	private PanelXML panelXML=new PanelXML();//XML text editor:
	private FileManager fm;
	private PanelResult panelResult=new PanelResult(this);
	//private CTask2 calculateBlurredImageTask=new RunStarlyxTask();

	public static String SENSORS_OUTPUT_FILE="sensors.csv";

	public JstarlyxApp()
	{
		super("com_photonlyx_jstarlyx",false,true,true);
		//tlFile.add(new Trigger(panelXML,"updateFromFile"));
		tlFile.add(new Trigger(this,"updateOpticalSystemFromEditor"));
		//tlFile.add(new Trigger(panel3D,"erasePathsFiles"));
		//tlFile.add(new Trigger(panel3D,"update3D"));
		tlFile.add(new Trigger(viewScene3D,"adaptFilter2Scene"));
		tlFile.add(new Trigger(viewScene3D,"update"));
		//tlFile.add(new Trigger(panel3D.getGraph3DPanel(),"initialiseProjectorAuto"));
		
		tlFile.add(new Trigger(viewScene3D,"initialiseProjectorAuto"));
		tlFile.add(new Trigger(this,"updateParamPanel"));
		tlFile.add(new Trigger(this,"updateObjectSelector"));
		tlFile.add(new Trigger(panelRendering,"clean"));
		tlFile.add(new Trigger(panelResult,"updateView"));

		fm=new FileManager(this,tlFile,scene);

		String[] ext= {"xml"};
		CFileFilter cff=new CFileFilter(ext,"starlyx file");
		CJMenuFile cJMenuFile=new CJMenuFile(fm,cff,tlFile,this.getCJFrame());
		cJMenuFile.getJMenu().setText("File");

		JMenuBar bar=new JMenuBar();
		bar.setBackground(Global.background);
		this.getCJFrame().setJMenuBar(bar);	
		bar.add(cJMenuFile.getJMenu());


		//menu for examples:
		{
			File fpath=new File(starlyxPath+File.separator+"examples");	
			File[] fs=fpath.listFiles();
			for (File f:fs) if (f.isDirectory()) examples.add(f.getName());
			JMenu menu_examples=new JMenu(Messager.getString("Examples"));
			menu_examples.setFont(Global.font);
			JMenuItem[] items =new JMenuItem[examples.size()];
			int i=0;
			for (String s:examples)
			{
				items[i]=new JMenuItem(s);
				items[i].addActionListener(this);
				menu_examples.add(items[i]);
				items[i].setFont(Global.font);
				i++;
			}
			bar.add(menu_examples);
		}

		//menu about
		{
			JMenu menu_about=new JMenu(Messager.getString("About"));
			menu_about.setFont(Global.font);
			JMenuItem[] items =new JMenuItem[2];
			int i=0;
			{
				items[i]=new JMenuItem("Web page");
				items[i].addActionListener(this);
				menu_about.add(items[i]);
				items[i].setFont(Global.font);
				i++;
			}
			{
				items[i]=new JMenuItem("User guide");
				items[i].addActionListener(this);
				menu_about.add(items[i]);
				items[i].setFont(Global.font);
				i++;
			}
			bar.add(menu_about);
		}

		//scene.setPath(this.path);

		this.getToolBar().add(fm.getPanelIcons());

		//chart panel for image:
		cp.hidePanelSide();
		cp.hideLegend();


		//toolbar:
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(fm,"save"));//fm.save();
		//			tl.add(new Trigger(scene,"runStarlyx"));
		//			//tl.add(new Trigger(this,"showClouds"));//showClouds();
		//			tl.add(new Trigger(this.getPanel3D(),"update3D"));
		//			tl.add(new Trigger(panelRendering,"loadHDRandUpdate"));
		//			tl.add(new Trigger(panelResult,"updateView"));
		//
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(100,20));
		//			cButton.setText("Run Starlyx");
		//			this.getToolBar().add(cButton);
		//		}
		{
			TriggerList tl1=new TriggerList();
			tl1.add(new Trigger(fm,"save"));//fm.save();
			TriggerList tl2=new TriggerList();
			tl2.add(new Trigger(viewScene3D,"update"));
			tl2.add(new Trigger(panelRendering,"loadHDRandUpdate"));
			tl2.add(new Trigger(panelResult,"updateView"));
			scene.setTrig_starlyx_thread(tl2);//set trigger for camera rendering update

			TaskButton2 taskButton=new TaskButton2(scene,tl1,tl2);
			taskButton.setStoppedLabel("Run starlyx");
			taskButton.setRunningLabel("Calculating...");
			taskButton.getJButton().setPreferredSize(new Dimension(100,20));
			this.getToolBar().add(taskButton.getJButton());
		}

		//		{
		//			String[] names={"nbPhotons","getPaths"};
		//			ParamsBox paramsBoxTimeSerie=new ParamsBox(scene,null,null,names,null,null,ParamsBox.HORIZONTAL,140,30);
		//			this.getToolBar().add(paramsBoxTimeSerie.getComponent());
		//		}

		this.getToolBar().repaint();
		this.getToolBar().validate();


		//panel side:
		//int w=145;
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"updateOpticalSystemFromEditor"));
		//			tl.add(new Trigger(this.getPanel3D(),"update3D"));
		//			tl.add(new Trigger(this,"updateObjectSelector"));
		//			tl.add(new Trigger(this,"updateParamPanel"));
		//			tl.add(new Trigger(panel3D.getGraph3DPanel(),"initialiseProjectorAuto"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Update 3D from editor");
		//			this.getToolBar().add(cButton);
		//		}

		//
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addSelectedSurface"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Save selected to disk");
		//			this.getPanelSide().add(cButton);
		//		}



		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addSourceSun"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add source sun");
		//			this.getPanelSide().add(cButton);
		//		}
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addSourcePoint"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add source point");
		//			this.getPanelSide().add(cButton);
		//		}
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addCamera"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add camera");
		//			this.getPanelSide().add(cButton);
		//		}
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addBox"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add box");
		//			this.getPanelSide().add(cButton);
		//		}
		//				{
		//					TriggerList tl=new TriggerList();
		//					tl.add(new Trigger(this,"addSphericalCap"));
		//					tl.add(new Trigger(this,"update"));
		//					CButton cButton=new CButton(tl);
		//					cButton.setPreferredSize(new Dimension(w,15));
		//					cButton.setText("Add spherical cap");
		//					this.getPanelSide().add(cButton);
		//				}

		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addObjectOBJ"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add OBJ");
		//			this.getPanelSide().add(cButton);
		//		}
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addMaterialVolume"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add material_volume");
		//			this.getPanelSide().add(cButton);
		//		}
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"addSpectrum"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("Add spectrum");
		//			this.getPanelSide().add(cButton);
		//		}
		//		{
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"deleteSelectedObject"));
		//			tl.add(new Trigger(this,"update"));
		//			CButton cButton=new CButton(tl);
		//			cButton.setPreferredSize(new Dimension(w,15));
		//			cButton.setText("delete");
		//			this.getPanelSide().add(cButton);
		//		}

		//		{
		//			//combo box for project selection:
		//			ParString parSelectedProject=new ParString(this,"selectedObject");
		//			comboBoxObjects=new ParStringComboBox(null,parSelectedProject);
		//			comboBoxObjects.setLabel("Object:");
		//			comboBoxObjects.setSize(300,30);
		//			comboBoxObjects.setTriggerList(null);
		//			////get the objects list:
		//			//for (StarLyxObject sp:os.getObjectsList()) comboBoxObjects.addItem(sp.getName());
		//
		//			TriggerList tl=new TriggerList();
		//			tl.add(new Trigger(this,"updateParamPanel"));
		//			//tl.add(new Trigger(this,"update"));
		//			comboBoxObjects.setTriggerList(tl);
		//
		//			this.getPanelSide().add(comboBoxObjects.getComponent());
		//		}

		paramPanel.setPreferredSize(new Dimension(200,600));

		//		this.getPanelSide().add(paramPanel);
		//		this.getPanelSide().repaint();
		//		this.getPanelSide().validate();

		//put an example of scene:
		//scene.setMinimalScene();
		//panelXML.setText(scene.getStringXML());

		this.add(panelXML,"Scene");
		//this.add(panel3D,"3D view");
		this.add(viewScene3D.getMainPanel(),"3D view");
		this.add(panelRendering,"Rendering");
		this.add(panelResult,"Result");
		//this.add(new PanelCloudHistogram(scene),"cloud histo");

		updateParamPanel();
		update();
		viewScene3D.update();
//		panel3D.update3D();
//		panel3D.getGraph3DPanel().setRender(false);
//		panel3D.getGraph3DPanel().initialiseProjectorAuto();
//		panel3D.getGraph3DPanel().update();
	}


	public void updateOpticalSystemFromEditor()
	{
		if (panelXML.getText().length()==0) return;
		XMLElement xml = new XMLElement();
		StringReader reader = new StringReader(panelXML.getText());
		try {	xml.parseFromReader(reader);}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		if (xml.getName().compareTo("Scene")==0)
		{
			scene.updateFromXML(xml);
		}
		else
		{
			Messager.messErr("Main object type is not Scene");
		}

	}



	public void updateObjectSelector()
	{
		//update the combo box of objects:
		//remove all items:
		if (comboBoxObjects!=null) 
		{
			comboBoxObjects.removeAllItems();	
			//add the objects list:
			for (Node sp:scene.getSons()) comboBoxObjects.addItem(sp.getName());
		}
	}


	public void updateParamPanel()
	{
		paramPanel.removeAll();

		Node o=scene.getNodeOfName(selectedObjectName);
		if (o==null) 
		{
			paramPanel.validate();
			paramPanel.repaint();
			return;
		}
		if (o instanceof Volume)
		{
			String[] names= {"name","matvol","surfaces"};
			ParamsBox pb=new ParamsBox(o,null,"object",names,null,null,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("name");psb.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof Surface)
		{
			String[] names= {"name","matsurf","objfile"};
			ParamsBox pb=new ParamsBox(o,null,"object",names,null,null,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("name");psb.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof StarLyxBox)
		{
			String[] names= {"name","x","y","z","dx","dy","dz"};
			ParamsBox pb=new ParamsBox(o,null,"box",names,null,null,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("name");psb.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof Camera)
		{
			String[] names= {"name","x","y","z","viewX","viewY","viewZ","field","dimx","dimy"};
			String[] units= {"","","","","","","","deg","pix","pix"};
			ParamsBox pb=new ParamsBox(o,null,"camera",names,null,units,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("name");psb.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof SourceSun)
		{
			String[] names= {"name","x","y","z"};
			ParamsBox pb=new ParamsBox(o,null,"sun source",names,null,null,ParamsBox.VERTICAL,200,22,false);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof SourcePoint )
		{
			String[] names= {"name","x","y","z"};
			ParamsBox pb=new ParamsBox(o,null,"point source",names,null,null,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("name");psb.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof Spectrum )
		{
			String[] names= {"name","data"};
			ParamsBox pb=new ParamsBox(o,null,"spectrum",names,null,null,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("data");psb.setFieldLength(10);
			ParStringBox psb1=(ParStringBox)pb.getParamBoxOfParam("name");psb1.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		if (o instanceof HenyeyGreensteinK )
		{
			String[] names= {"name","k","ka","g","n"};
			ParamsBox pb=new ParamsBox(o,null,"mat. vol.",names,null,null,ParamsBox.VERTICAL,200,22,false);
			ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("name");psb.setFieldLength(10);
			paramPanel.add(pb.getComponent());
		}
		paramPanel.validate();
		paramPanel.repaint();

	}




	public void addSourceSun()
	{
		SourceSun s=new SourceSun();
		scene.add(s);
		s.setName(scene.findNewNameWithRoot("sourceSun"));
		selectedObjectName=s.getName();
	}

	public void addSourcePoint()
	{
		SourcePoint s=new SourcePoint();
		scene.add(s);
		s.setName(scene.findNewNameWithRoot("sourcePoint"));
		selectedObjectName=s.getName();
	}

	public void addCamera()
	{
		Camera c=new Camera();
		scene.add(c);
		c.setName(scene.findNewNameWithRoot("camera"));
		selectedObjectName=c.getName();
	}



	public void addSelectedSurface()
	{
		Vector<Object3D> objs=viewScene3D.getGraph3DPanel().getSelectedObjects();
		if (objs.size()==0) return;
		Object3D ob=(Object3D)objs.get(0);
		if (ob instanceof TriangleMeshSource) 
		{
			TriangleMeshSource of=(TriangleMeshSource)ob;
			TriangleMesh tris=of.getTriangles();
			String s=tris.saveToOBJ();
			Surface surf=new Surface(this.path);
			scene.add(surf);
			surf.setName(scene.findNewNameWithRoot(ob.getClass().getSimpleName()));
			TextFiles.saveString(this.path+File.separator+surf.getName()+".obj", s);
			surf.setObjfile(surf.getName()+".obj");
			selectedObjectName=surf.getName();
			viewScene3D.getGraph3DPanel().delete(ob);
			//os.save(Global.path, fm.getFilename());
			panelXML.setText(scene.getStringXML());
			viewScene3D.update();
			updateObjectSelector();
			updateParamPanel();
		}
	}




	public void addBox()
	{
		StarLyxBox box=new StarLyxBox(this.path);
		scene.add(box);
		box.setName(scene.findNewNameWithRoot("box"));
		box.setObjfile(box.getName()+".obj");
		selectedObjectName=box.getName();
	}

	//	public void addSphericalCap()
	//	{
	//		StarLyxSphericalCap box=new StarLyxSphericalCap();
	//		os.add(box);
	//		box.setName(os.findNewNameWithRoot("sph_cap"));
	//		box.setObjfile(box.getName()+".obj");
	//		selectedObjectName=box.getName();
	//	}

	public void addObjectOBJ()
	{
		InputDialog inputDialog=new InputDialog();
		inputDialog.setFieldLabel("OBJ file name");
		inputDialog.setInitialFieldValue(".obj");
		inputDialog.show();
		String file=inputDialog.getStringAnswer();
		if (file==null) return;
		if (!new File(this.path+File.separator+file).exists())
		{
			Messager.messErr("No file of name "+file+" found");
			return;
		}
		Surface o=new Surface(this.path);
		scene.add(o);
		o.setName(file);
		o.setObjfile(file);
		selectedObjectName=o.getName();
	}

	public void addSpectrum()
	{
		Spectrum box=new Spectrum();
		scene.add(box);
		box.setName(scene.findNewNameWithRoot("spectrum"));
		box.setData("");
		selectedObjectName=box.getName();
	}

	public void addMaterialVolume()
	{
		HenyeyGreensteinK mv=new HenyeyGreensteinK();
		scene.add(mv);
		mv.setName(scene.findNewNameWithRoot("mat_vol"));
		selectedObjectName=mv.getName();
	}

	public void deleteSelectedObject()
	{
		Node o=scene.getNodeOfName(selectedObjectName);
		scene.removeSon(o);
		selectedObjectName=scene.getObjectsList().elementAt(0).getName();
	}





	public String getSelectedObject() {
		return selectedObjectName;
	}


	public void setSelectedObject(String selectedObject) {
		this.selectedObjectName = selectedObject;
	}


	public TriggerList getTriggerListFile() {
		return tlFile;
	}


	public  Scene getScene() {
		return scene;
	}

	public FileManager getFileManager() {
		return fm;
	}

	public void setFileManager(FileManager fm) {
		this.fm=fm;

	}


	public PanelXML getPanelXML() {
		return panelXML;
	}

public static void openViewApp(String path,String fileName)
{
	if (fileName.endsWith(".xml"))
	{
		Scene scene=new Scene();
		scene.setPath(path);
		scene.setFileName(fileName);
		CJFrame cjframe	=new CJFrame();
		cjframe.setTitle(scene.getPath()+File.separator+scene.getFileName());
		cjframe.setLocation(400,50);
		cjframe.setSize(new Dimension(800,800));
		cjframe.setExitAppOnExit(true);
		JTabbedPane topTabPane=new JTabbedPane();
		cjframe.add(topTabPane);
		//load the scene:
		scene.loadFromFile();
		ViewScene3D vs=new ViewScene3D(scene);
		topTabPane.add(vs.getComponent());
		vs.initialiseProjectorAuto();
	}
}

	public static void main(String[] args)
	{
		Triangle3D.drawCenter=false;
		Triangle3D.drawNormal=false;
		if (args.length==0)
		{
		JstarlyxApp app =new JstarlyxApp();
		Global.path=app.path;
		}
		
		if (args.length==1)
		{
			JstarlyxApp app =new JstarlyxApp();
			//			Global.path=new File("./").getParent()+File.separator;
			//			System.out.println("Global.path======"+Global.path);
			String fileName=args[0];
			app.path="./";
			if (fileName.endsWith(".xml"))
			{
				app.getFileManager().open(app.path, fileName);
				app.getTriggerListFile().trig();

			}
			Global.path=app.path;
		}
		
		if (args.length==2)
		{
			if (args[0].compareTo("-v")==0) 
			{
				String fileName=args[1];
				openViewApp("./",fileName);
//				if (fileName.endsWith(".xml"))
//				{
//					Scene scene=new Scene();
//					scene.setPath("./");
//					scene.setFileName(fileName);
//					CJFrame cjframe	=new CJFrame();
//					cjframe.setTitle(scene.getPath()+File.separator+scene.getFileName());
//					cjframe.setLocation(400,50);
//					cjframe.setSize(new Dimension(800,800));
//					cjframe.setExitAppOnExit(true);
//					JTabbedPane topTabPane=new JTabbedPane();
//					cjframe.add(topTabPane);
//					//load the scene:
//					scene.loadFromFile();
//					ViewScene3D vs=new ViewScene3D(scene);
//					topTabPane.add(vs.getComponent());
//					vs.initialiseProjectorAuto();
//				}
			}
		}
		//	Global.path="./";
	}

//
//	public PanelScene3D getPanel3D() 
//	{
//		return panel3D;
//	}


	/**
	 * open jstarlyx app for this model
	 * @param dir create directory dir
	 * @param xmlModel the scene
	 */
	public  static void openJstarlyx(String path,String sceneFilename)
	{
		//open jstarlyx session:
		JstarlyxApp jstarlyxApp =new JstarlyxApp();
		jstarlyxApp.path=path;
		System.out.println(" Jstarlyx path:"+jstarlyxApp.path);
		jstarlyxApp.getCJFrame().setExitAppOnExit(false);
		jstarlyxApp.getFileManager().open(jstarlyxApp.path,sceneFilename);
		jstarlyxApp.getTriggerListFile().trig();
	}

	public  static void openJstarlyx(Scene scene)
	{
		 openJstarlyx(scene.getPath(),scene.getFileName());
	}



	/**
	 * open jstarlyx app for this path
	 * @param dir create directory dir
	 * @param xmlModel the scene
	 */
	public  static void openJstarlyx2(String path,String sceneFileName)
	{
		//open jstarlyx session:
		JstarlyxApp jstarlyxApp =new JstarlyxApp();
		jstarlyxApp.path=path+File.separator;
		System.out.println(" Jstarlyx path:"+jstarlyxApp.path);
		jstarlyxApp.getCJFrame().setExitAppOnExit(false);
		jstarlyxApp.getFileManager().open(jstarlyxApp.path,sceneFileName);
		jstarlyxApp.getTriggerListFile().trig();
	}

	private static String getStarlyxPath()
	{
		String systemPath=System.getenv("PATH");
		//System.out.println("systemPath="+systemPath);
		Definition def=new Definition(systemPath,':');
		Vector<String> v=new Vector<String>();
		for (String s:def)
		{
			if (s.contains("starlyx"))
				if (!s.contains("jstarlyx"))
				{
					System.out.println(s);
					v.add(s);
				}
		}
		if (v.size()==0)
		{
			Messager.messErr("Can't find starlyx path in env. variable PATH");
			return null;
		}
		if (v.size()>1)
		{
			String s="Multiple starlyx paths:";
			for (String w:v) s+=w+":";
			Messager.messErr(s);
			return null;
		}
		String starlyxPath=v.elementAt(0);
		return starlyxPath;
	}

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		Object source = e.getSource();
		if (source instanceof JMenuItem)
		{
			JMenuItem mi=(JMenuItem)source;
			if (mi.getText().compareTo("Web page")==0)
			{
				if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) 
				{
					try
					{
						Desktop.getDesktop().browse(new URI("http://www.starlyx.org"));
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
			else if (mi.getText().compareTo("User guide")==0)
			{
				if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) 
				{
					try
					{
						Desktop.getDesktop().browse(new URI("http://www.starlyx.org/starlyx_doc.pdf"));
					}
					catch(Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}
			else
			{
				//examples
				String exampleName=mi.getText();
				System.out.println("exampleName="+ exampleName);
				if (starlyxPath==null) return;
				String path=starlyxPath+File.separator+"examples"+File.separator+exampleName;
				String fileName="scene.xml";
				//fm.open(path,fileName);
				//this.getCJFrame().setTitle(path+File.separator+fileName);
				fm.getTrig().trig();
				openJstarlyx2(path,fileName);
			}
		}

	}










	//
	//	public class RunStarlyxTask  implements CTask2
	//	{
	//		private TaskEndListener taskEndListener;
	//		private boolean stopped=true;
	//		private Process p;
	//		
	//		@Override
	//		public void run() {
	//			p=scene.runStarlyx();
	//			if (taskEndListener!=null) taskEndListener.whenTaskFinished();
	//		}
	//
	//		@Override
	//		public void setStopped(boolean b)
	//		{
	//			stopped=b;
	//		}
	//
	//		@Override
	//		public boolean isStopped()
	//		{
	//			return stopped;
	//		}
	//
	//		@Override
	//		public void setTaskEndListener(TaskEndListener taskEndListener) 
	//		{
	//			this.taskEndListener=taskEndListener;	
	//		}
	//	}
	//	
	//	










}
