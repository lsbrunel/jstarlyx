package com.photonlyx.jstarlyx.gui;

import java.awt.Dimension;
import java.io.File;

import javax.swing.JTabbedPane;

import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.toolbox.gui.CJFrame;

public class ViewScene3DApp 
{
	public ViewScene3DApp(String path,String filename)
	{
		//load the scene:
		Scene scene=new Scene();
		scene.setPath(path);
		scene.setFileName(filename);
		scene.loadFromFile();
		//create the window:
		CJFrame cjframe	=new CJFrame();
		cjframe.setTitle(scene.getPath()+File.separator+scene.getFileName());
		cjframe.setLocation(400,50);
		cjframe.setSize(new Dimension(800,800));
		cjframe.setExitAppOnExit(true);
		JTabbedPane topTabPane=new JTabbedPane();
		cjframe.add(topTabPane);
		ViewScene3D v=new ViewScene3D(scene);
		topTabPane.add(v.getComponent());
		v.initialiseProjectorAuto();		

	}

	public static void main(String[] args)
	{
		String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2012_Dior/2024/2024_03_26_test_rendus_surfaces_rugueuses/2024_04_22_gonio_surface_rugueuse";
		String file="scene.xml";
		ViewScene3DApp app=new ViewScene3DApp(path,file);
	}


}
