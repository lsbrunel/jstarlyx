package com.photonlyx.jstarlyx.gui;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class PanelXML extends JPanel //implements DocumentListener 
{
	//private FileManager fm;
	//private JTextArea txtArea= new JTextArea();//XML editor
	
    RSyntaxTextArea textArea = new RSyntaxTextArea(20, 60);


public PanelXML()
{
	//this.fm=fm;
	this.setLayout(new BorderLayout());
	
	
    textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_XML);
    textArea.setCodeFoldingEnabled(true);
    textArea.setFont(Global.font.deriveFont((float)14.0));
    RTextScrollPane sp = new RTextScrollPane(textArea);
	this.add(sp,BorderLayout.CENTER);

	
//	
//	txtArea.setFont(Global.font.deriveFont((float)14.0));
//	//txtArea.setEditable(false);
//	JScrollPane jsp=new JScrollPane(txtArea);
//	this.add(jsp,BorderLayout.CENTER);
	//txtArea.getDocument().addDocumentListener(this);
}

//public void updateFromFile()
//{
//	txtArea.setText(TextFiles.readFile(Global.path+fm.getFilename()).toString());
//}
public void setText(String s)
{
textArea.setText(s);
}


public String getText()
{
	return textArea.getText();
}
//
//@Override
//public void insertUpdate(DocumentEvent e)
//{
//TextFiles.saveStringUTF8(Global.path+fm.getFilename(), txtArea.getText());
//System.out.println(txtArea.getText());
//}
//
//
//@Override
//public void removeUpdate(DocumentEvent e)
//{
//TextFiles.saveStringUTF8(Global.path+fm.getFilename(), txtArea.getText());
//System.out.println(txtArea.getText());
//
//}
//
//
//
//@Override
//public void changedUpdate(DocumentEvent e)
//{
//TextFiles.saveStringUTF8(Global.path+fm.getFilename(), txtArea.getText());
//System.out.println(txtArea.getText());
//
//}


}
