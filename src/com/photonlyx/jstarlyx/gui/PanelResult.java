package com.photonlyx.jstarlyx.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.io.File;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;

public class PanelResult extends JPanel
{
	private JTextArea jta=new JTextArea();
	private ChartPanel cp=new ChartPanel();
	private JstarlyxApp app;
	private boolean seeAsSpectrum=true;

	public PanelResult(JstarlyxApp app)
	{
		this.app=app;
		this.setLayout(new BorderLayout());

		jta.setBackground(Global.background);
		jta.setFont(Global.font);
		//	JPanel jptxt=new JPanel();
		//	jptxt.setLayout(new BorderLayout());
		//	jptxt.setPreferredSize(new Dimension(1000,300));
		JScrollPane jscrollpane = new JScrollPane(jta);
		jscrollpane.setPreferredSize(new Dimension(1000,100));
		//this.add(jscrollpane,BorderLayout.NORTH);

		cp.setPreferredSize(new Dimension(800,500));
		cp.getChart().setLogx(false);
		cp.getChart().setYlabel("");
		cp.getChart().setXlabel("Wavelength");
		cp.getChart().setXunit("nm");
		cp.getChart().setFormatTickx("0");
		cp.getSplitPane().setDividerLocation(0.9);
		cp.hidePanelSide();

		//split pane for graph and legend:
		JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, jscrollpane ,cp);
		splitPane.setDividerLocation(100);
		splitPane.setOneTouchExpandable(true); 
		splitPane.setContinuousLayout(true); 
		splitPane.setResizeWeight(1);
		this.setLayout(new BorderLayout());


		//	this.add(cp);
		//this.add(cp,BorderLayout.CENTER);
		JPanel jp=new JPanel();//center panel
		jp.setBackground(Global.background);
		jp.setLayout(new BorderLayout());
		jp.add(splitPane,BorderLayout.CENTER);

		JPanel top=new JPanel();
		top.setBackground(Global.background);
		top.setLayout(new FlowLayout());
		top.setPreferredSize(new Dimension(0,40));
		jp.add(top,BorderLayout.NORTH);

		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateView"));
			String[] names={"seeAsSpectrum"};
			ParamsBox pb=new ParamsBox(this,tl,null,names,null,null,ParamsBox.VERTICAL,150,30);
			top.add(pb);
		}
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"updateView"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(100,15));
			cButton.setText("Update");
			top.add(cButton);
		}


		this.add(jp,BorderLayout.CENTER);

	}



	public void updateView()
	{
		StringBuffer sb=null;
		sb=TextFiles.readFile(app.path+File.separator+JstarlyxApp.SENSORS_OUTPUT_FILE,false,false);

		if (sb!=null)
		{
			String s=sb.toString();
			jta.setText(s);

			Plot[] plots=null;
			if (seeAsSpectrum)
			{
				plots=SensorData.showSpectra(app.path,JstarlyxApp.SENSORS_OUTPUT_FILE);	
				cp.getChart().setXlabel("Wavelength");
				cp.getChart().setXunit("nm");
				cp.getChart().setFormatTickx("0");
			}
			else 
			{
				plots=SensorData.showSurfacesOnXaxis(app.path,JstarlyxApp.SENSORS_OUTPUT_FILE);	
				cp.getChart().setXlabel("# surface");
				cp.getChart().setXunit("");
				cp.getChart().setFormatTickx("0");

			}
			if (plots!=null)
			{
				cp.getChart().removeAllPlots();
				for (Plot p:plots) cp.getChart().add(p);
				cp.update();
			}
		}
	}





	public boolean isSeeAsSpectrum() {
		return seeAsSpectrum;
	}



	public void setSeeAsSpectrum(boolean seeAsSpectrum) {
		this.seeAsSpectrum = seeAsSpectrum;
	}







}
