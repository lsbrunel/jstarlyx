package com.photonlyx.jstarlyx.gui;

import java.awt.Dimension;
import java.util.Vector;

import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.math.geometry.cloud.CloudPoint;
import com.photonlyx.toolbox.math.histo.Histogram2D1D;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.util.Global;

public class PanelCloudHistogram extends ChartPanel
{
	private Histogram2D1D h=new Histogram2D1D();
	private String surfaceName="";
	private Scene os;
	private WindowApp app;

	public PanelCloudHistogram(WindowApp app,Scene os)
	{
		this.app=app;
		this.os=os;
		
		this.getChart().setSignal2D1D(h);
		this.getChart().setLogz(true);


		{
			String[] names= {"surfaceName"};
			ParamsBox pb=new ParamsBox(this,null,null,names,null,null,ParamsBox.VERTICAL,200,22,false);
			//ParStringBox psb=(ParStringBox)pb.getParamBoxOfParam("surfaceName");psb.setFieldLength(10);
			this.getPanelSide().add(pb.getComponent());
		}
		
		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"fillHisto"));
			tl.add(new Trigger(this,"update"));
			CButton cButton=new CButton(tl);
			cButton.setPreferredSize(new Dimension(100,15));
			cButton.setText("Update");
			this.getPanelSide().add(cButton);
		}
	}




	public String getSurfaceName() {
		return surfaceName;
	}

	public void setSurfaceName(String surfaceName) {
		this.surfaceName = surfaceName;
	}

	public String[] surfaceNameOptions()
	{		
		Vector<String> sn=new Vector<String>();
		for (Node n:os)
		{
			if (n instanceof Surface)
			{
				Surface s=(Surface)n;
				sn.add(s.getName());
			}
		}
		String[] array=new String[sn.size()];
		return sn.toArray(array);
	}



	public void fillHisto()
	{
		Surface s=(Surface)os.getNodeOfName(surfaceName);
		Cloud c=new Cloud();
		String filename=s.getName()+".pointscloud";
		if (c.readBinaryFile(app.path, filename, true)) 
		{
			//System.out.println(c.size()+" points in "+filename);
			Vecteur p1=c.calcPmin();
			Vecteur p2=c.calcPmax();
			System.out.println(p1);
			System.out.println(p2);
			double xmin=p1.y();
			double xmax=p2.y();
			double ymin=p1.z();
			double ymax=p2.z();
			h.init(xmin, xmax, ymin, ymax, 100, 100);
			for (CloudPoint cp:c)
			{
				h.addOccurence(cp.getP().y(), cp.getP().z(), 1);
				//System.out.println(cp.getP().y()+" "+cp.getP().z());
			}
		}
	}






}
