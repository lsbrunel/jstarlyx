package com.photonlyx.jstarlyx.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.swing.JPanel;

import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.toolbox.gui.ParamsBox;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.Object3D;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.math.geometry.cloud.Cloud;
import com.photonlyx.toolbox.threeD.gui.CloudColor;
import com.photonlyx.toolbox.threeD.gui.Graph3DPanel;
import com.photonlyx.toolbox.threeD.gui.Object3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;

/**
 * 
 * @author laurent
 *
 * @deprecated
 */
public class PanelScene3D extends JPanel
{
	private Scene scene;
	private Graph3DPanel g3dp; 
	private boolean seePaths=true;
	private boolean seePathsError=true;
	private boolean onlyPathInBoundaryVolume=false;
	private boolean onlyPathsWithNonZeroWeight=false;
	private Object3DColorSet paths=new Object3DColorSet();
	private Object3DColorSet paths_error=new Object3DColorSet();
	private Object3DColorSet clouds=new Object3DColorSet();
	private Object3DColorSet surfaces=new Object3DColorSet();


	public PanelScene3D(WindowApp app,Scene scene)
	{
		super();
		this.scene=scene;

		this.setLayout(new BorderLayout());
		g3dp=new Graph3DPanel(app);
		this.add(g3dp.getMainPanel(),BorderLayout.CENTER);
		g3dp.getPanelSide().setPreferredSize(new Dimension(150,0));
		g3dp.setWireFrame(true);
		g3dp.setRender(false);
		g3dp.getJPanel().setBackground(new Color(240,240,240));

		g3dp.addColorSet(paths);
		g3dp.addColorSet(paths_error);
		g3dp.addColorSet(clouds);
		g3dp.addColorSet(surfaces);

		{
			TriggerList tl=new TriggerList();
			tl.add(new Trigger(this,"update3D"));
			String[] names={"seePathsError","seePaths","onlyPathInBoundaryVolume","onlyPathsWithNonZeroWeight"};
			String[] labels={"seePathsError","seePaths","inVolume","no null weights"};
			ParamsBox pb=new ParamsBox(this,tl,null,names,labels,null,ParamsBox.VERTICAL,150,30);
			g3dp.getPanelSide().add(pb);
		}

	}

	public Graph3DPanel getGraph3DPanel()
	{
		return g3dp;
	}

	public void update3D()
	{
		Graph3DPanel g3d=g3dp;

		g3d.removeAll3DObjects();

		surfaces.removeAllElements();

		g3d.setAxesLength(scene.getSceneSize()/2);

		for (Node n:scene.getSons()) 
		{
			Object3DColorSet os=n.getObject3D();
			if (os!=null)
			{
				surfaces.add(os);
			}
		}
		if (seePaths) 
		{
			loadPaths();
			g3d.addColorSet(paths);
		}
		if (seePathsError) 
		{
			loadPathsError();
			g3d.addColorSet(paths_error);
		}
		g3d.addColorSet(clouds);
		g3d.addColorSet(surfaces);

		g3d.update();
	}

	public void loadPaths()
	{
		if (seePaths) 
			loadPaths(scene,paths,"paths.obj","weights.txt",Color.BLUE,onlyPathInBoundaryVolume,onlyPathsWithNonZeroWeight);
	}
	public void loadPathsError()
	{
		if (seePathsError) 
			loadPaths(scene,paths_error,"paths_error.obj","weights_error.txt",Color.RED,onlyPathInBoundaryVolume,onlyPathsWithNonZeroWeight);
	}
	
	
	public void erasePathsFiles()
	{
		File f1=new File(scene.getPath()+File.separator+"paths.obj");
		f1.delete();
		File f2=new File(scene.getPath()+File.separator+"paths_error.obj");
		f2.delete();
	}
	
	public static void loadPaths(Scene scene,Object3DColorSet paths,String file_paths,String file_weights,
			Color c ,boolean onlyPathInBoundaryVolume,boolean onlyPathsWithNonZeroWeight)
	{	
		boolean coloredPaths=(c==null);	
		paths.removeAllElements();
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		scene.updateBoundariesVolume(cornerMin, cornerMax);
		//enlarge the boundary volume:
		Vecteur diag=cornerMax.sub(cornerMin);
		cornerMin._sub(new Vecteur(1,1,1));
		diag._add(new Vecteur(2,2,2));
		paths.add(new Object3DColorInstance(new Box(cornerMin,diag),new Color(200,200,255)));
		//double sceneSize=app.getScene().getSceneSize();
		if (scene.getStoreTrajectories()==1) 
		{	
			StringBuffer sb=TextFiles.readFile(scene.getPath()+File.separator+file_paths,false);
			if (sb==null) return;
			String[] lines_w=TextFiles.readLines(scene.getPath()+File.separator+file_weights,true,false);
			if (lines_w==null) return;
			System.out.println(" read paths in file "+scene.getPath()+File.separator+file_paths);
			Color[] colors= {Color.RED,Color.BLUE, Color.GREEN,Color.CYAN,Color.GRAY,Color.MAGENTA,Color.PINK};
			int ci=0;
			Object3DSet obs=Object3DSet.readOBJ(sb.toString());

			int index=0;
			for (Object3D o:obs) 
				if (o instanceof PolyLine3D) //just show the polylines
				{				
					double weight=0;
					if (index<lines_w.length) weight=Double.parseDouble(lines_w[index++]);
					if (onlyPathsWithNonZeroWeight) if (weight==0) continue;
					if (coloredPaths) c=colors[ci];
					//cut the polyline such that is stays in the scene boundary volume:
					PolyLine3D polyline=(PolyLine3D)o;
					for (int i=0;i<polyline.nbPoints()-1;i++)
					{
						Vecteur p1=polyline.get(i);
						Vecteur p2=polyline.get(i+1);
						if (onlyPathInBoundaryVolume)
						{
							boolean inBoundaryVolume=true;
							for (int k=0;k<3;k++) 
							{
								inBoundaryVolume=inBoundaryVolume&&(p1.u[k]>=cornerMin.u[k]); 
								inBoundaryVolume=inBoundaryVolume&&(p1.u[k]<=cornerMax.u[k]); 
								inBoundaryVolume=inBoundaryVolume&&(p2.u[k]>=cornerMin.u[k]); 
								inBoundaryVolume=inBoundaryVolume&&(p2.u[k]<=cornerMax.u[k]); 
							}
							if (inBoundaryVolume)
							{
								Segment3D seg=new Segment3D(p1,p2);
								Object3DColorInstance oc=new Object3DColorInstance(seg,c);
								paths.add(oc);
							}
						}
						else
						{
							Segment3D seg=new Segment3D(p1,p2);
							Object3DColorInstance oc=new Object3DColorInstance(seg,c);
							paths.add(oc);							
						}

					}
					//					Object3DColorInstance oc=new Object3DColorInstance(o,c);
					//					paths.add(oc);
					ci++;
					if (ci==colors.length) ci=0;
				}
			//paths.addObjObjects(sb.toString());
			//g3d.initialiseProjectorAuto();
			//g3d.repaint();
		}

	}



	public void showClouds()
	{
		Graph3DPanel g3d=g3dp;
		clouds.removeAllElements();	
		for (Node n:scene)
		{
			if (n instanceof Surface)
			{
				Surface s=(Surface)n;
				Cloud c=new Cloud();
				String filename=s.getName()+".pointscloud";
				//System.out.println("Read points cloud "+Global.path+filename);
				if (new File(scene.getPath()+filename).exists())
					if (c.readBinaryFile(scene.getPath(), filename, true)) 
					{
						clouds.add(new CloudColor(c,Color.BLUE));
						System.out.println(c.size()+" points");
					}
			}
		}
		g3d.initialiseProjectorAuto();
		g3d.repaint();

		for (Object3DColor ocs:clouds)
		{
			if (ocs instanceof CloudColor)
			{
				CloudColor cc=(CloudColor)ocs;	
				System.out.print(cc.getCloud().size()+"\t");
			}
		}
		System.out.println();

	}



	public boolean isSeePaths() {
		return seePaths;
	}


	public void setSeePaths(boolean seePaths) {
		this.seePaths = seePaths;
	}


	public boolean isSeePathsError() {
		return seePathsError;
	}


	public void setSeePathsError(boolean seePathsError) {
		this.seePathsError = seePathsError;
	}

	public boolean isOnlyPathInBoundaryVolume() {
		return onlyPathInBoundaryVolume;
	}

	public void setOnlyPathInBoundaryVolume(boolean onlyPathInBoundaryVolume) {
		this.onlyPathInBoundaryVolume = onlyPathInBoundaryVolume;
	}

	public boolean isOnlyPathsWithNonZeroWeight() {
		return onlyPathsWithNonZeroWeight;
	}

	public void setOnlyPathsWithNonZeroWeight(boolean onlyPathsWithNonZeroWeight) {
		this.onlyPathsWithNonZeroWeight = onlyPathsWithNonZeroWeight;
	}


}
