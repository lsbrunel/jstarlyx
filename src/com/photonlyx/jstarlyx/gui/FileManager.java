package com.photonlyx.jstarlyx.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.DecimalFormat;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.toolbox.gui.CButton;
import com.photonlyx.toolbox.gui.CJFrame;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.io.CFileFilter;
import com.photonlyx.toolbox.io.CFileManager;
import com.photonlyx.toolbox.io.NewFileAction;
import com.photonlyx.toolbox.triggering.Trigger;
import com.photonlyx.toolbox.triggering.TriggerList;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Messager;

public class FileManager  implements CFileManager //implements ActionListener
{
	private boolean isNewFile=true;
	private TriggerList trig;
	private JPanel jp=new JPanel();
	private JstarlyxApp app;
	private Scene scene;
	
	private int nbmenus=6;
	private JMenuItem[] items;
	private JMenu menu;


	public FileManager (JstarlyxApp app,TriggerList trig,Scene scene)
	{
		this.app=app;
		this.trig=trig;
		this.scene=scene;

		Trigger t_new=new Trigger(this,"newFile");
		Trigger t_open=new Trigger(this,"open");
		Trigger t_saveas=new Trigger(this,"saveAs");
		Trigger t_save=new Trigger(this,"save");

		jp.setPreferredSize(new Dimension(40*4, 40));
		//jp.setSize(new Dimension(250, 30));
		jp.setBackground(Global.background);
		jp.setLayout(new FlowLayout(FlowLayout.LEFT));//create the legend of this chart:
		addIcon("com/photonlyx/toolbox/gui/NewFileAction.png",t_new);
		addIcon("com/photonlyx/toolbox/gui/OpenFileAction.png",t_open);
		addIcon("com/photonlyx/toolbox/gui/SaveAsFileAction.png",t_saveas);
		addIcon("com/photonlyx/toolbox/gui/SaveFileAction.png",t_save);
	}



	private void addIcon(String f,Trigger tAction)
	{
		TriggerList tl1=new TriggerList();
		tl1.add(trig);
		tl1.add(tAction);
		CButton cb=new CButton(tl1);
		cb.setPreferredSize(new Dimension(30, 30));
		cb.setIconFileName(f);
		cb.setContentAreaFilled(false);
		cb.setBorderPainted(false);
		jp.add(cb);	
	}

	public JPanel getPanelIcons() {return jp;}

	/**
open a new file
@param path the path of the file directory
@param fileName the file name
	 */
	public boolean open(String path,String fileName)
	{
		if (fileName==null) return false;
		System.out.println(getClass()+" "+"open: "+path+File.separator+fileName);
		String s=TextFiles.readFile(path+File.separator+fileName,false).toString();
		app.path=path;
		if (s==null) return false;

		app.getPanelXML().setText(s);
		
		//setSaved();
		//isNewFile=false;
		scene.setFileName(fileName);
		scene.setPath(path);
		return true;
	}
	
	public void readFile()
	{
		open(app.path,scene.getFileName());
	}

	public boolean save(String path,String fileName)
	{
//		XMLElement el = os.toXML();
//		StringBuffer sb=new StringBuffer();
//		sb.append("<?xml version=\"1.0\"?> \n");
//		XMLFileStorage.toStringWithIndentation(el,sb,"");
//		//sb.append(el.toString());
//		TextFiles.saveString(path+fileName,sb.toString());
		
		TextFiles.saveString(path+File.separator+fileName,app.getPanelXML().getText());
		scene.setFileName(fileName);;
		System.out.println(getClass()+" "+"saved to "+path+File.separator+fileName);
		//setSaved();
		//isNewFile=false;
		return true;
	}


	public boolean save()
	{
		if (scene.getFileName()==null) 
			{
			saveAs();
			return true;
			}
		else return save(app.path,scene.getFileName());
	}

	public void open()
	{
		String ch=app.path;
		if (ch==null) ch=".";
		JFileChooser df=new JFileChooser(ch);
		String[] ext= {"xml"};
		CFileFilter cff=new CFileFilter(ext,"starlyx file");
		df.addChoosableFileFilter(cff);
		df.setFileFilter(cff);
		int returnVal = df.showOpenDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			app.path=df.getSelectedFile().getParent()+File.separator;
			String path=df.getSelectedFile().getParent()+File.separator;
			String fileName=df.getSelectedFile().getName();
			this.open(path,fileName);
			app.getCJFrame().setTitle(path+File.separator+fileName);
		}
		else 
		{
			return;
		}
	}




	public void saveAs()
	{
		String ch=app.path;
		if (ch==null) ch=".";
		JFileChooser df=new JFileChooser(ch);
		String[] ext= {"xml"};
		CFileFilter jff=new CFileFilter(ext,"starlyx file");
		df.addChoosableFileFilter(jff);
		df.setFileFilter(jff);
		int returnVal = df.showSaveDialog(null);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			app.path=df.getSelectedFile().getParent()+File.separator;
			//System.out.println("Global.chemin:"+Global.chemin);
			String fileName=df.getSelectedFile().getName();
			String extension=jff.getExtension();
			if (!fileName.endsWith(extension)) fileName+="."+extension;
			String path=df.getSelectedFile().getParent()+File.separator;
			if(df.getSelectedFile().exists())
			{
				if (JOptionPane.showConfirmDialog(null,Messager.getString("overwrite_file")+" "+fileName+"?"
						,Messager.getString("Confirm"), JOptionPane.YES_NO_OPTION)== JOptionPane.NO_OPTION)
				{
					return;
				}
				else
				{
					this.save(path,fileName);
					app.getCJFrame().setTitle(/*path+*/fileName);
				}
			}
			else 
			{
				this.save(path,fileName);
				app.getCJFrame().setTitle(/*path+*/fileName);		
			}
		}
		else 
		{
			return;
		}
	}



	public void saveFile()
	{
		if (!this.isNewFile()) this.save(); else saveAs();
	}

	/**
tells if the file is new or has been already saved
	 */
	public boolean isNewFile()
	{
		return isNewFile;
	}


	public void newFile()
	{
		String s=getNewFileName(app.path,"xml");
		newFile(s);
	}


	public void newFile(String newFileName)
	{
		//setSaved();
		scene.free();
		scene.setFileName(newFileName);
		//os.updateFromXML(null);
		app.getPanelXML().setText("");
		isNewFile=true;
	}


	public static  String getNewFileName(String path,String extension)
	{
		//add a number to the default file name such that it is a new file
		String newfilename,newglobalfilename;
		DecimalFormat f=new DecimalFormat("000");
		File file;
		int count=1;
		do
		{
			newfilename="new"+f.format(count)+"."+extension;
			newglobalfilename=path+File.separator+newfilename;
			// 	System.out.println("NewFileAction"+" "+newglobalfilename+" exists ? ->"+new File(newglobalfilename).exists());
			count++;
		}
		while (new File(newglobalfilename).exists());
		// System.out.println("NewFileAction"+" "+"choosen:"+newglobalfilename);
		return newfilename;
	}



	public Scene getScene() {
		return scene;
	}
	
	
	
//
//public void createMenu()
//{
//	String[] labels=new String[nbmenus];
//	labels[0]=Messager.getString("New");
//	labels[1]=Messager.getString("Open_a_file");
//	labels[2]=Messager.getString("Save");
//	labels[3]=Messager.getString("Save_as");
//	labels[4]=Messager.getString("Print");
//	labels[5]=Messager.getString("Exit");
//	menu=new JMenu(Messager.getString("File"));
//
//	items =new JMenuItem[nbmenus];
//	for (int i=0;i<nbmenus;i++) items[i]=new JMenuItem(labels[i]);
//	for (int i=0;i<nbmenus;i++) items[i].addActionListener(this);
//	for (int i=0;i<nbmenus;i++) menu.add(items[i]);
//	items[0].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
//	items[1].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
//	items[2].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
//	items[5].setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
//
//	menu.setFont(Global.font);
//	for (int i=0;i<nbmenus;i++) items[i].setFont(Global.font);
//	
//	JMenuBar bar=new JMenuBar();
//	bar.setBackground(Global.background);
//	app.getCJFrame().setJMenuBar(bar);	
//	bar.add(menu);
//}
//
//
//
////lauch the work actions of all the networks (work buttons here)
//private void launchTriggerList()
//{
//if (trig!=null) trig.trig();
//}
//
//
//public void actionPerformed(ActionEvent e)
//	{
//
//	Object source = e.getSource();
//
//	if (source==items[0]) //create a new file
//		{
//		String s;
//		newFile(s=NewFileAction.getNewFileName(app.getPath(),cff.getExtension()));
//		app.getCJFrame().setTitle(s);
//
//		launchTriggerList();
//		}
//	if (source==items[1]) //open another file
//		{
//		System.out.println("Open file in path:"+Global.path);
//		String ch=Global.path;
//		if (ch==null) ch=".";
//		JFileChooser df=new JFileChooser(ch);
//		df.addChoosableFileFilter(cff/*.getFileFilter()*/);
//		df.setFileFilter(cff);
//		int returnVal = df.showOpenDialog(app.getCJFrame());
//		if(returnVal == JFileChooser.APPROVE_OPTION)
//			{
//			Global.path=df.getSelectedFile().getParent()+File.separator;
//			String path=df.getSelectedFile().getParent()+File.separator;
//			String fileName=df.getSelectedFile().getName();
//			open(path,fileName);
//			app.getCJFrame().setTitle(/*path+*/fileName);
//			}
//		launchTriggerList();
//		}
//
//	if (source==items[2])//save
//		{
//		boolean ok=this.save();
//		if (!ok) saveAs();
//		}
//	if (source==items[3])//save as
//		{
//		saveAs();
//		//launchTriggerList();
//		}
//	if (source==items[4]) //print the Visible son
//		{
//		//Component comp=fatherWindow;
//		Messager.messErr("Not available yet !");
//		//flexo.image.Utils.print(comp);
//		}
//	if (source==items[5])//quit
//		{
//		String answer=Messager.yesNoQuestion(Messager.getString("really_close_the_application"),Messager.getString("closing") );
//		if (answer.compareTo("yes")==0) 
//			{
//			   app.getCJFrame().dispose();
//			}
//		}
//
//
//	}



public JMenu getMenu() 
{
	return menu;
}



@Override
public String getPath() {
	
	return scene.getPath();
}



@Override
public boolean load() {
	open();
	return true;
}



@Override
public boolean isSaved() {
	// TODO Auto-generated method stub
	return false;
}



@Override
public void setSaved() {
	// TODO Auto-generated method stub
	
}



@Override
public String getFileName() {
	// TODO Auto-generated method stub
	return scene.getFileName();
}



public TriggerList getTrig() {
	return trig;
}





}
