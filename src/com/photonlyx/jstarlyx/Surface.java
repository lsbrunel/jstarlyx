package com.photonlyx.jstarlyx;


import java.awt.Color;
import java.io.File;

import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Text3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.TextFiles;
import nanoxml.XMLElement;

public class Surface extends Node
{
	private String file="";
	private String materials="";
	private TriangleMeshFrame mesh=new TriangleMeshFrame();
	private String path;//path to the obj file
	private int store_impacts=0;
	private static Color c=Color.GRAY;

	public Surface(String path)
	{
		this.path=path;
	}

	public Surface(Node father,String path)
	{
		super(father);
		this.path=path;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getMaterials() {
		return materials;
	}

	public void setMaterials(String s) {
		this.materials = s;
	}

	public int getStore_impacts() {
		return store_impacts;
	}

	public void setStore_impacts(int store_impacts) {
		this.store_impacts = store_impacts;
	}

	private void readMeshFile()
	{
		if (file.endsWith(".obj"))
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+file,true,false);
			mesh.readOBJ(sb.toString());
		}
		else
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+file,true,false);
			mesh.readSTL(sb.toString());
			//mesh.readSTLinBinaryFile(Global.path, file);
		}
	}


	@Override
	public Object3DColorSet getObject3D()
	{
		this.readMeshFile();
		Object3DColorSet set=new Object3DColorSet();
		set.add(new Object3DColorInstance(mesh,c));	
		//name
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		mesh.checkOccupyingCube(cornerMin,cornerMax);
		Vecteur textPos=cornerMax;
		Text3DColor t=new Text3DColor(this.getName(),textPos,c);
		set.add(t);
		return set;
	}


	public String getObjfile()
	{
		return file;
	}


	public void setObjfile(String objfile)
	{
		this.file = objfile;
	}


	public TriangleMeshFrame getSet() {
		return mesh;
	}

	public void setSet(TriangleMeshFrame set) {
		this.mesh = set;
	}
	
	private TriangleMeshFrame getBody()
	{
	return mesh;
	}
	
	
	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		//mesh.checkOccupyingCube(cornerMin, cornerMax);
		getBody().checkOccupyingCube(cornerMin, cornerMax);
	}



	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("surface");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("MATERIALS", materials);
		el.setAttribute("FILE",file);
		if (store_impacts==1)  el.setAttribute("STORE_IMPACTS",store_impacts);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;

		this.setName(xml.getStringAttribute("NAME",""));
		materials=xml.getStringAttribute("MATERIALS","");
		file=xml.getStringAttribute("FILE");
		store_impacts=xml.getIntAttribute("STORE_IMPACTS");
	}





}
