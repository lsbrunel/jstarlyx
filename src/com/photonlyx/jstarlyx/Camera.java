package com.photonlyx.jstarlyx;

import java.awt.Color;
import java.util.Enumeration;
import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.DiskMesh;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Circle3DColor;
import com.photonlyx.toolbox.threeD.gui.Object3DColorInstance;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Point3DColor;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;

import nanoxml.XMLElement;

public class Camera extends Node implements XMLstorable
{
	private Vecteur viewPoint=new Vecteur(0,0,0);
	private Vecteur lens=new Vecteur(-3,-3,3);
	private int dimx=100,dimy=100;//nb pixels in H and V
	private double field_h=50;//full horizontal field in degrees
	private String filters="filterR filterG filterB";//name of the output file of starlyx
	//local frame:
	private Vecteur camX,camY,camZ;
	private double focal=1;
	private double camSize_h,camSize_v;
	private double x,xp,d,objectPlaneSize_h,objectPlaneSize_v;
	private double gamma;
	private String volume="World";//volume name where is placed the camera
	private double na=0;

	public Camera()
	{
		this.setName("camera0");
	}

	public Camera(Node n)
	{
		super(n);
		this.setName("camera0");
	}


	private void configureGeometry()
	{
		camSize_h=2*focal*Math.tan(Math.PI/180.0*field_h/2);
		camSize_v=camSize_h*dimy/(double)dimx;
		//on calcule le repère local de la caméra
		//l'axe optique de la camera est le vecteur X (local)
		camX=viewPoint.sub(lens);
		camX._normalise();
		Vecteur z=new Vecteur(0,0,1);
		//camY is the horizontal axis:
		camY=z.vectmul(camX);
		camY._normalise();
		//camY is the vertical axis:
		camZ=camX.vectmul(camY);
		
		//distance from lens to view point:
		x=lens.sub(viewPoint).norme();
		//use conjugate formula to calculate the image distance:
		xp=Math.pow(focal,2)/(x-focal)+focal;
		//calc the diameter of the aperture:
		d=Math.tan(Math.asin(na))*2.0*focal;
		objectPlaneSize_h=camSize_h*x/xp;
		objectPlaneSize_v=camSize_v*x/xp;
		
//		System.out.println("camX= "+camX);
//		System.out.println("camY= "+camY);
//		System.out.println("camZ= "+camZ);
//		System.out.println("focal "+focal);
//		System.out.println("x "+x);
//		System.out.println("xp "+xp);
//		System.out.println("camSize "+camSize);
//		System.out.println("aperture diameter "+d);
//		System.out.println("objectPlaneSize "+objectPlaneSize);
	}



	@Override
	public Object3DColorSet getObject3D()
	{
		configureGeometry();

		Object3DColorSet set=new Object3DColorSet();
		set.add(new Point3DColor(lens,Color.BLACK,5));

		//corners of the sensor (the sensor is transparent and in front of lens !)
		{
			Vecteur sensorCentre=lens.addn(camX.scmul(-xp));
			Vecteur p1=sensorCentre.addn(camY.scmul(camSize_h/2.0)).addn(camZ.scmul(camSize_v/2.0));
			Vecteur p2=sensorCentre.addn(camY.scmul(camSize_h/2.0)).addn(camZ.scmul(-camSize_v/2.0));
			Vecteur p3=sensorCentre.addn(camY.scmul(-camSize_h/2.0)).addn(camZ.scmul(-camSize_v/2.0));
			Vecteur p4=sensorCentre.addn(camY.scmul(-camSize_h/2.0)).addn(camZ.scmul(camSize_v/2.0));

			//draw sensor borders:
			set.add(new Segment3DColor(p1,p2,Color.BLACK));
			set.add(new Segment3DColor(p2,p3,Color.BLACK));
			set.add(new Segment3DColor(p3,p4,Color.BLACK));
			set.add(new Segment3DColor(p4,p1,Color.BLACK));
			//draw sensor axis
			Vecteur p5=sensorCentre.addn(camY.scmul(camSize_h/2.0));
			set.add(new Segment3DColor(sensorCentre,p5,Color.BLACK));
			Vecteur p6=sensorCentre.addn(camZ.scmul(camSize_v/2.0));
			set.add(new Segment3DColor(sensorCentre,p6,Color.BLACK));
			//draw sensor hat:
			Vecteur p7=sensorCentre.addn(camZ.scmul(camSize_v/2.0*1.5));
			set.add(new Segment3DColor(p7,p1,Color.BLACK));
			set.add(new Segment3DColor(p7,p4,Color.BLACK));

			//draw segments from lens to corners
			set.add(new Segment3DColor(lens,p1,Color.BLACK));
			set.add(new Segment3DColor(lens,p2,Color.BLACK));
			set.add(new Segment3DColor(lens,p3,Color.BLACK));
			set.add(new Segment3DColor(lens,p4,Color.BLACK));
		}
		
		{
		//draw the aperture:
		//set.add(new Circle3DColor(d/2, lens, camX,20,Color.RED,false));
		DiskMesh disk1=new DiskMesh(d,30);
		disk1.getFrame()._translate(lens);	
		disk1.getFrame().getAxis(0).affect(camZ);	
		disk1.getFrame().getAxis(1).affect(camY);	
		disk1.getFrame().getAxis(2).affect(camX);	
		//disk1.getFrame()._rotate(new Vecteur(0,-Math.PI/2,0));	
		disk1.updateGlobal();
		set.add(new Object3DColorInstance(disk1,Color.BLACK));
		}
		
		//draw the object plane:
		{
			Vecteur objectPlaneCentre=lens.addn(camX.scmul(x));
			Vecteur p1=objectPlaneCentre.addn(camY.scmul(objectPlaneSize_h/2.0)).addn(camZ.scmul(objectPlaneSize_v/2.0));
			Vecteur p2=objectPlaneCentre.addn(camY.scmul(objectPlaneSize_h/2.0)).addn(camZ.scmul(-objectPlaneSize_v/2.0));
			Vecteur p3=objectPlaneCentre.addn(camY.scmul(-objectPlaneSize_h/2.0)).addn(camZ.scmul(-objectPlaneSize_v/2.0));
			Vecteur p4=objectPlaneCentre.addn(camY.scmul(-objectPlaneSize_h/2.0)).addn(camZ.scmul(objectPlaneSize_v/2.0));
			//draw object plane borders:
			set.add(new Segment3DColor(p1,p2,Color.DARK_GRAY));
			set.add(new Segment3DColor(p2,p3,Color.DARK_GRAY));
			set.add(new Segment3DColor(p3,p4,Color.DARK_GRAY));
			set.add(new Segment3DColor(p4,p1,Color.DARK_GRAY));
			//draw segments from lens to corners
			set.add(new Segment3DColor(lens,p1,Color.DARK_GRAY));
			set.add(new Segment3DColor(lens,p2,Color.DARK_GRAY));
			set.add(new Segment3DColor(lens,p3,Color.DARK_GRAY));
			set.add(new Segment3DColor(lens,p4,Color.DARK_GRAY));
		}

		//draw segment from lens to viewPoint:
		//set.add(new Segment3DColor(lens,viewPoint,Color.DARK_GRAY));

		//draw focal point:
		Vecteur focalPoint=lens.addn(camX.scmul(-focal));
		set.add(new Segment3DColor(focalPoint.addn(camY.scmul(-focal/20)),focalPoint.addn(camY.scmul(focal/20)),Color.DARK_GRAY));
		set.add(new Segment3DColor(focalPoint.addn(camZ.scmul(-focal/20)),focalPoint.addn(camZ.scmul(focal/20)),Color.DARK_GRAY));

		return set;
	}




	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("camera");

		el.setIntAttribute("nbPixelsX", dimx);
		el.setIntAttribute("nbPixelsY", dimy);
		el.setDoubleAttribute("field", field_h);
		el.setAttribute("name", this.getName());
		el.setAttribute("filters", filters);
		el.setAttribute("gamma", gamma);
		el.setAttribute("volume", volume);
		el.setDoubleAttribute("focal", focal);
		el.setDoubleAttribute("na", na);

		//add lens:
		XMLElement elLens=lens.toXML();
		elLens.setName("lens");
		el.addChild(elLens);
		//add view point:
		XMLElement elViewPoint=viewPoint.toXML();
		elViewPoint.setName("viewPoint");
		el.addChild(elViewPoint);

		//XMLElement elColor = new XMLElement();
		//elColor.setName("color");
		//elColor.setIntAttribute("mode", colorMode);
		//el.addChild(elColor);

		return el;
	}





	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		dimx=xml.getIntAttribute("nbPixelsX",10);
		dimy=xml.getIntAttribute("nbPixelsY",10);
		field_h=xml.getDoubleAttribute("field",30);
		filters=xml.getStringAttribute("filters");
		gamma=xml.getDoubleAttribute("gamma",1);
		volume=xml.getStringAttribute("volume","World");
		focal=xml.getDoubleAttribute("focal",1);
		na=xml.getDoubleAttribute("na",0);

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("lens"))
			{
				lens.updateFromXML(el);
			}
			if (el.getName().contains("viewPoint"))
			{
				viewPoint.updateFromXML(el);
			}
			if (el.getName().contains("Object"))
			{
				Volume o=new Volume(this);
				o.updateFromXML(el);
				this.addSon(o);
			}
			//	if (el.getName().contains("color"))
			//		{
			//		colorMode=el.getIntAttribute("mode",0);
			//		}
		}
	}


	public Camera getAcopy()
	{
		Camera cam=new Camera();
		cam.viewPoint=this.viewPoint.copy();
		cam.lens=this.lens.copy();
		cam.focal=this.focal;
		cam.dimx=this.dimx;
		cam.dimy=this.dimy;
		cam.filters=this.filters;
		cam.field_h=this.field_h;
		cam.gamma=this.gamma;
		cam.configureGeometry();
		return cam;
	}



	public Vecteur getViewPoint()
	{
		return viewPoint;
	}


	public Vecteur getLens()
	{
		return lens;
	}

	//
	//public int getColorMode()
	//{
	//return colorMode;
	//}


	public int getDimx()
	{
		return dimx;
	}


	public void setDimx(int dimx) {
		this.dimx = dimx;
	}

	public void setDimy(int dimy) {
		this.dimy = dimy;
	}

	public int getDimy()
	{
		return dimy;
	}


	public double getField()
	{
		return field_h;
	}


	public void setField(double field) {
		this.field_h = field;
	}


	public String getFilters() {
		return filters;
	}

	public void setFilters(String filters) {
		this.filters = filters;
	}

	public Vecteur getCamX()
	{
		return camX;
	}


	public Vecteur getCamY()
	{
		return camY;
	}


	public Vecteur getCamZ()
	{
		return camZ;
	}


	public double getFocal()
	{
		return focal;
	}


	public double getHalfCamSize()
	{
		return camSize_h;
	}

	public void setPos(Vecteur pos)
	{
		lens.copy(pos);
	}
	public Vecteur getPos()
	{
		return lens;
	}


	public double getX() {
		return lens.x();
	}

	public void setX(double x) {
		lens.setX( x);
	}

	public double getY() {
		return lens.y();
	}

	public void setY(double y) {
		lens.setY (y);
	}

	public double getZ() {
		return lens.z();
	}

	public void setZ(double z) {
		lens.setZ (z);
	}

	public void setViewPoint(Vecteur v)
	{
		viewPoint.copy(v);
	}



	public double getViewX() {
		return viewPoint.x();
	}

	public void setViewX(double x) {
		viewPoint.setX( x);
	}

	public double getViewY() {
		return viewPoint.y();
	}

	public void setViewY(double y) {
		viewPoint.setY (y);
	}

	public double getViewZ() {
		return viewPoint.z();
	}

	public void setViewZ(double z) {
		viewPoint.setZ (z);
	}

	public double getGamma() {
		return gamma;
	}

	public void setGamma(double gamma) {
		this.gamma = gamma;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}


	public double getNa() {
		return na;
	}

	public void setNa(double na) {
		this.na = na;
	}

	public void setFocal(double focal) {
		this.focal = focal;
	}

	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		for (int k=0;k<3;k++) 
		{
			double r=lens.coord(k);
			if (r < cornerMin.coord(k)) cornerMin.coordAffect(k,r);
			if (r > cornerMax.coord(k)) cornerMax.coordAffect(k,r);
		}			
	}




}
