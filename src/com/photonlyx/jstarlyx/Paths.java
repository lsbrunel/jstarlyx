package com.photonlyx.jstarlyx;

import java.awt.Color;
import java.io.File;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.txt.TextFiles;

public class Paths extends Vector<Path>
{
	private Scene scene;	
	private static String file_paths="paths.csv";

	public Paths(Scene scene)
	{
		this.scene=scene;
	}

	public void load()
	{
		String[][] array=TextFiles.readCSV(scene.getPath()+File.separator+file_paths,false,"\t",true);
		if (array==null) return;
		System.out.println(" read paths in file "+scene.getPath()+File.separator+file_paths);
		Path p=null;
		for (String[] l:array)
		{
			if (l[0].compareTo("path_nb")==0) continue;//jump first line
			Interaction in=new Interaction(l);
			if (in.getType().compareTo("PHOTON_CREATION")==0) 
			{
				p=new Path();
				this.add(p);
			}
			if (p!=null) p.add(in);
		}
		System.out.println(this.size()+" paths loaded");
	}

	/**
	 * get a 3D representation of the paths
	 * @return
	 */
	public Object3DSet getView(int dotSize,boolean seeSegments,Color color)
	{
		Object3DSet set=new Object3DSet();
		for (Path p:this)
		{
			set.add(p.getView(dotSize,seeSegments,color));
		}
		return set;
	}


	//
	//	/**
	//	 * filter the paths thas has or has not weight=0 at the end of the path
	//	 * @param has
	//	 * @param type
	//	 * @return
	//	 */
	//	public Paths filter(boolean has,boolean onlyPathsWithNonZeroWeight)
	//	{
	//		Paths set=new Paths(scene);
	//		boolean ok;
	//		for (Path p:this)
	//		{
	//			ok=false;
	//			if (onlyPathsWithNonZeroWeight)
	//			{
	//				if (p.getLastInteraction().getW()!=0) ok=true;
	//			}
	//			else
	//			{
	//				ok=true;
	//			}
	//			if (!has) ok=!ok;
	//			if (ok) set.add(p);
	//		}
	//		return set;
	//	}

	/**
	 * filter the paths thas has or has not the interaction type at the end of the path
	 * @param has
	 * @param type
	 * @return
	 */
	public Paths filter(boolean has,String type)
	{
		Paths set=new Paths(scene);
		boolean ok;
		for (Path p:this)
		{
			ok=false;
			if (type.compareTo("all")==0)
			{
				ok=true;			
			}
			else if (type.compareTo("w!=0")==0)
			{
				ok=(p.getLastInteraction().getW()!=0);
			}
			else
			{
				ok=(p.getLastInteraction().getType().compareTo(type)==0);							
			}
			if (!has) ok=!ok;
			if (ok) set.add(p);
		}
		return set;
	}

	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		for (Path p:this)
		{
			sb.append(p.toString()+"\n");
		}
		return sb.toString();
	}

}
