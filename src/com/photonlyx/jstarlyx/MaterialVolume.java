package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

public class MaterialVolume extends Node implements XMLstorable
{
	private String k="";//name of spectrum of K
	private String ka="";//name of spectrum of KA
	private String g="";//name of spectrum of G
	private String phaseFunction="";//name of the phase function curve

	public MaterialVolume()
	{
	}
	public MaterialVolume(Node father)
	{
		super(father);
	}

	public MaterialVolume(String name)
	{
		this.setName(name);
	}


	public String getK() {
		return k;
	}


	public void setK(String k) {
		this.k = k;
	}


	public String getKa() {
		return ka;
	}


	public void setKa(String ka) {
		this.ka = ka;
	}


	public String getG() {
		return g;
	}


	public void setG(String g) {
		this.g = g;
	}

	
	
	public String getPhaseFunction() {
		return phaseFunction;
	}
	public void setPhaseFunction(String phaseFunction) {
		this.phaseFunction = phaseFunction;
	}
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Material-Volume");
		el.setAttribute("name", this.getName());
		el.setAttribute("k", k);
		el.setAttribute("ka", ka);
		el.setAttribute("g", g);
		el.setAttribute("phase_function", phaseFunction);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		this.setK(xml.getStringAttribute("k",""));
		this.setKa(xml.getStringAttribute("ka",""));
		this.setG(xml.getStringAttribute("g",""));
		this.setG(xml.getStringAttribute("phase_function",""));
	}






}
