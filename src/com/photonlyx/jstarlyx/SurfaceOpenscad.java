package com.photonlyx.jstarlyx;


import java.awt.Color;
import java.io.File;

import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Triangle3D;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Text3DColor;
import com.photonlyx.toolbox.threeD.gui.Triangle3DColor;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Util;

import nanoxml.XMLElement;

public class SurfaceOpenscad extends Node
{
	private String materials="";
	private TriangleMeshFrame mesh=new TriangleMeshFrame();
	private String path;//path to the geometry file (stl or obj)
	private String code;//openscad code
	private String file="";
	private int store_impacts=0;
	private static Color c=new Color(128,128,140);

	public SurfaceOpenscad(String path)
	{
		this.path=path;
	}

	public SurfaceOpenscad(Node father,String path)
	{
		super(father);
		this.path=path;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String s) {
		this.code = s;
	}

	public String getMaterials() {
		return materials;
	}

	public void setMaterials(String s) {
		this.materials = s;
	}

	public int getStore_impacts() {
		return store_impacts;
	}

	public void setStore_impacts(int store_impacts) {
		this.store_impacts = store_impacts;
	}

	private void launchOpenscad(String openscadfilename)
	{
		String stlfilename=this.getName()+".stl";
		Process p=Util.runCommand("openscad --export-format asciistl -q -o "+path+File.separator+stlfilename+" "+path+File.separator+openscadfilename);
	}


	@Override
	public Object3DColorSet getObject3D()
	{
		String openscadfilename;
		if (file.length()>0)
		{
			//use attribute FILE
			openscadfilename=file;
		}
		else
		{
			//the openscad filename if the object name
			openscadfilename=this.getName()+".scad";
			//save the code in the disk with this file name:
			TextFiles.saveString(path+File.separator+openscadfilename,code,false);
		}
		//if (!new File(path+File.separator+this.getName()+".scad").exists()) 
			launchOpenscad(openscadfilename);
		this.readMeshFile();
		Object3DColorSet setc=new Object3DColorSet();		
		for (Triangle3D t:mesh.getTriangles()) 
		{
			Triangle3DColor tc=new Triangle3DColor(t,Color.LIGHT_GRAY,true);
			setc.add(tc);
		}
		//name
		//get BV:
		Vecteur cornerMin=new Vecteur();
		Vecteur cornerMax=new Vecteur();
		mesh.checkOccupyingCube(cornerMin,cornerMax);
		Vecteur textPos=cornerMax;
		Text3DColor t=new Text3DColor(this.getName(),textPos,c);
		setc.add(t);
		//		setc.add(mesh.getTriangles());		
		return setc;
	}

	private void readMeshFile()
	{
		String stlfilename=this.getName()+".stl";
		{
			StringBuffer sb=TextFiles.readFile(path+File.separator+stlfilename,true,false);
			mesh.readSTL(sb.toString());
			//mesh.readSTLinBinaryFile(Global.path, file);
		}
	}

	public String getObjfile()
	{
		return this.getName()+".stl";
	}


	public void setObjfile(String objfile)
	{

	}


	public TriangleMeshFrame getSet() {
		return mesh;
	}

	public void setSet(TriangleMeshFrame set) {
		this.mesh = set;
	}



	public void updateBoundariesVolume(Vecteur cornerMin, Vecteur cornerMax) 
	{
		mesh.checkOccupyingCube(cornerMin, cornerMax);
	}



	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("openscad");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("MATERIALS", materials);
		el.setAttribute("CODE",code);
		//el.setAttribute("FILE",file);
		if (store_impacts==1)  el.setAttribute("STORE_IMPACTS",store_impacts);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;

		this.setName(xml.getStringAttribute("NAME",""));
		materials=xml.getStringAttribute("MATERIALS","");
		code=xml.getStringAttribute("CODE","");
		//file=xml.getStringAttribute("FILE","");
		store_impacts=xml.getIntAttribute("STORE_IMPACTS");
	}





}
