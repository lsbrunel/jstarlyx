package com.photonlyx.jstarlyx;


import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

public class Grating extends Node implements XMLstorable
{
	private double a;//pair of lines / mm
	private int order;

	public Grating()
	{
	}
	public Grating(Node father)
	{
		super(father);
	}

	public Grating(String name)
	{
		this.setName(name);
	}

	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	
	
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("grating");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("A", a);
		el.setAttribute("ORDER", order);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
		a=xml.getDoubleAttribute("A",600);
		order=xml.getIntAttribute("order",1);
	}






}
