package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

/**
 * scatterer defined by Mie phase function
 * @author laurent
 *
 */
public class Mie extends Node implements XMLstorable
{
	private String phi="0.01";//volume fraction (not in %)
	private String d_um="1";//sphere diameter (in um)
	private String nr="1.5";//real part of refractive index spectrum of the spherical particle
	private String ni="0";//imaginary part refractive index spectrum of the spherical particle
	private String nang="90";//nb of angles between 0 and 90 in Mie phase fct calculation
	private String sigma_d_um="0";//polydispersity
	private String distrib_shape="normal";//shape of the size distribution curve, normal (gauss) or log-normal

	public Mie()
	{
	}

	public Mie(Node father)
	{
		super(father);
	}

	public Mie(String name)
	{
		this.setName(name);
	}


	public String getPhi() {
		return phi;
	}
	public void setPhi(String phi) {
		this.phi = phi;
	}

	public String getD_um() {
		return d_um;
	}
	public void setD_um(String d_um) {
		this.d_um = d_um;
	}

	public String getNr() {
		return nr;
	}

	public void setNr(String nr) {
		this.nr = nr;
	}

	public String getNi() {
		return ni;
	}

	public void setNi(String ni) {
		this.ni = ni;
	}


	public String getNang() {
		return nang;
	}
	public void setNang(String nang) {
		this.nang = nang;
	}
	public void setSigma_d_um(String sigma_d_um) {
		this.sigma_d_um = sigma_d_um;
	}
	public String getDistrib_shape() {
		return distrib_shape;
	}
	public void setDistrib_shape(String distrib_shape) {
		this.distrib_shape = distrib_shape;
	}
	


	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Mie");
		el.setAttribute("name", this.getName());
		el.setAttribute("phi", phi);
		el.setAttribute("d_um", d_um);
		el.setAttribute("nr", nr);
		el.setAttribute("ni", ni);
		el.setAttribute("nang",nang);
		el.setAttribute("sigma_d_um", sigma_d_um);
		el.setAttribute("distrib_shape", distrib_shape);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		this.setPhi(xml.getStringAttribute("phi",""));
		this.setD_um(xml.getStringAttribute("d_um",""));
		this.setNr(xml.getStringAttribute("nr","1.33"));
		this.setNi(xml.getStringAttribute("ni","0"));
		this.setNang(xml.getStringAttribute("nang",""));
		this.setSigma_d_um(xml.getStringAttribute("sigma_d_um",""));
		this.setDistrib_shape(xml.getStringAttribute("distrib_shape",""));
	}






}
