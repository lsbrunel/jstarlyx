package com.photonlyx.jstarlyx;


import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

/**
 * material surface emitting with Chandrasekhar luminance field (for validation)
 * @author laurent
 *
 */
public class EmissionNada extends Node implements XMLstorable
{
	private String eta0="0"; 
	private String j="1"; 

	public EmissionNada()
	{
	}
	public EmissionNada(Node father)
	{
		super(father);
	}

	public EmissionNada(String name)
	{
		this.setName(name);
	}


	public String getEta0() {
		return eta0;
	}
	public void setEta0(String eta0) {
		this.eta0 = eta0;
	}
	public String getJ() {
		return j;
	}
	public void setJ(String j) {
		this.j = j;
	}
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("emission_nada");
		el.setAttribute("NAME", this.getName());
		el.setAttribute("ETA0", eta0);
		el.setAttribute("J", j);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
		this.setEta0(xml.getStringAttribute("ETA0","0"));
		this.setJ(xml.getStringAttribute("J","1"));
	}






}
