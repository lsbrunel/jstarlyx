package com.photonlyx.jstarlyx;


import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

public class Mirror extends Node implements XMLstorable
{
	//private String name=""; //name

	public Mirror()
	{
	}
	public Mirror(Node father)
	{
		super(father);
	}

	public Mirror(String name)
	{
		this.setName(name);
	}

	
	
	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("mirror");
		el.setAttribute("NAME", this.getName());
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) return;
		this.setName(xml.getStringAttribute("NAME",""));
	}






}
