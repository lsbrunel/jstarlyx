package com.photonlyx.jstarlyx.sceneGenerators;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Vector;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.Node;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.toolbox.math.geometry.TriangleMeshFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;

/**
transform a scene of rendering with one camera in another one with many cameras making a circular travelling
 */

public  class GenerateRotation 
{
	private Scene scene;
	private int nbAngles=10;


	public GenerateRotation()
	{
		scene=new Scene();
		//saveScene();
	}



	public Scene getScene() {
		return scene;
	}






	public int getNbCameras() {
		return nbAngles;
	}


	public void setNbCameras(int nbCameras) {
		this.nbAngles = nbCameras;
	}

	public static void generate(Scene scene,int nbAngles)
	{	
		Vector<Node> nodes=scene.getNodesOfClass("Surface");
		if (nodes.size()>1)
		{
			System.out.println("Please put only one surface in the scene");
			return;
		}
		if (nodes.size()==0)
		{
			System.out.println("Please put at least one surface in the scene");
			return;
		}
		Surface surface0=(Surface)nodes.elementAt(0);
		String surface0Filename=surface0.getFile();
		Camera cam0=(Camera)scene.getNodeOfClassContaining("Camera");
		String cam0name=cam0.getName();
		double step_deg=(360.0/nbAngles);//angular step in deg
		DecimalFormat df=new DecimalFormat("000");
		
		//delete and create working directory:
		File newDir=new File(scene.getPath()+File.separator+"rotation");
		String new_dir=newDir.getName();
		System.out.println("Create the directory:"+new_dir);
		if (newDir.exists())deleteDirectory(newDir);
		newDir.mkdir();
			
		//generate the batch
		String batch="";
		batch+="cd "+new_dir+"\n";
		
		for (int i=0;i<nbAngles;i++)
		{
			double angle_deg=0+i*step_deg;//rad

			//load the mesh: 
			TriangleMeshFrame mesh=new TriangleMeshFrame();		
			mesh.readOBJ(scene.getPath(),surface0Filename);
			//rotate:
			mesh.getFrame()._rotate(new Vecteur(0,0,Math.PI/180.0*angle_deg));
			mesh.updateGlobal();
			//save the new mesh:
			String newSurfaceName=surface0Filename.replace(".obj","")+"_"+df.format(angle_deg)+".obj";
			surface0.setObjfile(newSurfaceName);
			mesh.saveToOBJfile(scene.getPath()+File.separator+new_dir,newSurfaceName);
//			//copy scene file to new dir :
//			try	{Files.copy(new File(scene.getPath()+File.separator+scene.getFileName()).toPath(), 
//					new File(scene.getPath()+File.separator+new_dir+File.separator+scene.getFileName()).toPath());}catch (Exception e)  {e.printStackTrace();}
			
			//save the new scene file:
			cam0.setName(cam0name+"_"+df.format(angle_deg));
			String newSceneFilename=scene.getFileName().replace(".xml","")+"_"+df.format(angle_deg)+".xml";
			scene.save(scene.getPath()+File.separator+new_dir,newSceneFilename);
			
			batch+="starlyx "+newSceneFilename+"\n";
		}
		batch+="cd ..\n";
		TextFiles.saveString(scene.getPath()+File.separator+"runstarlyx",batch,true);
	}
	
	
	
	
/**
 * delete dir and all contents
 * @param file
 */
	public static void deleteDirectory(File file)
	{
		for (File subfile : file.listFiles()) 
		{
			//  recursively call function to empty subfolder
			if (subfile.isDirectory()) deleteDirectory(subfile);
			subfile.delete();
		}
	}

	public static void main(String args[])
	{
		//String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2022_11_Guerlain_pétales_orchidées/projet_starlyx";
		String path=".";

		Scene scene=new Scene();
		scene.setFileName("scene.xml");
		scene.setPath(path);
		scene.loadFromFile();
		int nbAngles;
		if (args.length==0)
		{
			nbAngles=36;
		}
		else
		{
			nbAngles=Integer.parseInt(args[0]);
		}

		generate(scene,nbAngles);

	}





}
