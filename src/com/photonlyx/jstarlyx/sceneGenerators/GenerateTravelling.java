package com.photonlyx.jstarlyx.sceneGenerators;

import java.io.File;
import java.text.DecimalFormat;

import com.photonlyx.jstarlyx.Camera;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Messager;

import nanoxml.XMLElement;

/**
transform a scene of rendering with one camera in another one with many cameras making a circular travelling
*/

public  class GenerateTravelling 
{
	private Scene scene;
	private int nbCameras=10;


	public GenerateTravelling()
	{
		scene=new Scene();
		//saveScene();
	}



	public Scene getScene() {
		return scene;
	}






	public int getNbCameras() {
		return nbCameras;
	}


	public void setNbCameras(int nbCameras) {
		this.nbCameras = nbCameras;
	}
	
	public static void generateTravelling(Scene scene,int nbCameras)
	{
		
		Camera cam0=(Camera)scene.getNodeOfClassContaining("Camera");
		if (cam0==null)
		{
			Messager.messErr("Can't find object of name "+"Camera");
			return;
		}
		double x=cam0.getPos().x();
		double y=cam0.getPos().y();
		double z=cam0.getPos().z();
		double r=Math.sqrt(Math.pow(x,2)+Math.pow(y,2));//radius of the circular travelling
		double angle0=Math.atan(y/x);//starting angle is where the cam of refrence scene is
		double step=Math.PI/180.0*(360.0/nbCameras);//angular step
		String name=cam0.getName();
		scene.removeSon(cam0);
		DecimalFormat df=new DecimalFormat("00");
		for (int i=0;i<nbCameras;i++)
		{
			double angle=angle0+i*step;
			double x1=r*Math.cos(angle);
			double y1=r*Math.sin(angle);
			Camera cam=cam0.getAcopy();
			cam.setPos(new Vecteur(x1,y1,z));
			cam.setName(name+"_travelling_"+df.format(i));
			scene.add(cam);
		}
		
		XMLElement xml=scene.toXML();
		String s=XMLFileStorage.toStringWithIndentation(xml);
		String f1=scene.getFileName().replace(".xml","")+"_travelling.xml";
		TextFiles.saveString(scene.getPath()+File.separator+f1,s);
	}
		
		
		
	public static void main(String args[])
	{
		//String path="/home/laurent/fabserver/cloud1/photonlyx/projets/2022_11_Guerlain_pétales_orchidées/projet_starlyx";
		String path=".";
		
		Scene scene=new Scene();
		scene.setFileName("scene.xml");
		scene.setPath(path);
		scene.loadFromFile();
		int nbCameras;
		if (args.length==0)
		{
			nbCameras=36;
		}
		else
		{
			nbCameras=Integer.parseInt(args[0]);
		}
	
		generateTravelling(scene,nbCameras);

	}





}
