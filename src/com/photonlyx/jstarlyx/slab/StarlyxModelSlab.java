package com.photonlyx.jstarlyx.slab;

import java.io.File;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.photonlyx.jstarlyx.Dielectric;
import com.photonlyx.jstarlyx.HenyeyGreensteinK;
import com.photonlyx.jstarlyx.Lambert;
import com.photonlyx.jstarlyx.Scene;
import com.photonlyx.jstarlyx.SourceLaser;
import com.photonlyx.jstarlyx.Spectrum;
import com.photonlyx.jstarlyx.Surface;
import com.photonlyx.jstarlyx.Volume;
import com.photonlyx.toolbox.io.XMLFileStorage;
import com.photonlyx.toolbox.math.geometry.Box;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3D;
import com.photonlyx.toolbox.math.geometry.Quadrilatere3DFrame;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.txt.TextFiles;
import com.photonlyx.toolbox.util.Global;
import com.photonlyx.toolbox.util.Util;

public class StarlyxModelSlab 
{
	private Scene scene;
	private double e=1; //slab thickness
	private double l=100; //transverse size
	private double wavelength_nm=650;
	private double n;//refractive index of slab volume
	private double k=1;//scattering coef of slab 
	private double ka=0;//absorption coef of slab 
	private double g=0;//assymetry coef of slab
	private String sensorTName="Tsensor";
	private double T;//sensorT signal
	private double Tsigma;//sensorT signal error
	
	
	
	
	private static DecimalFormat df = new DecimalFormat("0.00000000000",new DecimalFormatSymbols(Locale.ENGLISH));

	
	public StarlyxModelSlab ()
	{
		String appliName="StarlyxModelSlab";
		Util.getOrCreateApplicationUserFolder (appliName,true);
		scene=new Scene(System.getProperty("user.home")+File.separator+Global.photonlyxToolBoxDir+File.separator+appliName);	
		buildModel();
	}
	
	private void  buildModel()
	{
		scene.free();
		scene.setAlgorithm("direct");
		
		//scene.setNbPhotons(1000);
		//scene.setVerbose(0);
		
		Spectrum spred=new Spectrum();
		spred.setName("red");
		spred.setData((int)wavelength_nm+" 1");
		scene.add(spred);

		SourceLaser laser=new SourceLaser();
		laser.getPos().copy(new Vecteur(-200,0,0.1));
		laser.getDir().copy(new Vecteur(100,0,0));
		laser.setPower(1);
		laser.setDiameter(5);
		laser.setName("laser0");
		laser.setSpectrum("red");
		scene.add(laser);
		
		//volume material for glass
		HenyeyGreensteinK hg=new HenyeyGreensteinK();
		hg.setName("hg1");
		hg.setKs(""+df.format(k));
		hg.setKa(""+df.format(ka));
		hg.setG(""+df.format(g));
		scene.add(hg);

		//slab volume:
		Volume v=new Volume();
		v.setName("lame");
		v.setSurfaces("slab");
		v.setN(df.format(n));
		v.setMaterials("hg1");
		scene.add(v);

		//slab surface:
		Surface surf=new Surface(scene.getPath());
		surf.setName("slab");
		surf.setMaterials("");
		surf.setFile("slab.obj");
		scene.add(surf);		
		Box b=new Box(new Vecteur(0,-l/2,-l/2),e,l,l);
		TextFiles.saveString(scene.getPath()+File.separator+"slab.obj",b.saveToOBJ(),false);
		
		//T sensor
		Surface surf1=new Surface(scene.getPath());
		surf1.setName(sensorTName);
		surf1.setMaterials("absorber");
		surf1.setFile(sensorTName+".obj");
		scene.add(surf1);		
		Quadrilatere3D q=new Quadrilatere3D(new Vecteur(e+10,l/2,l/2),new Vecteur(e+10,-l/2,l/2),new Vecteur(e+10,-l/2,-l/2),new Vecteur(e+10,l/2,-l/2));
		Quadrilatere3DFrame q1=new Quadrilatere3DFrame(q);
		TextFiles.saveString(scene.getPath()+File.separator+sensorTName+".obj",q1.getTriangles().saveToOBJ(),false);
		
	
		//surface material for the product and the glass
		Dielectric dielectric=new Dielectric();
		dielectric.setName("dielectric1");
		scene.add(dielectric);

		//surface material for the T sensor:
		Lambert absorber=new Lambert();
		absorber.setName("absorber");
		absorber.setAlbedo("0");
		scene.add(absorber);

	}

	public boolean calc()
	{
		buildModel();
		String xml=XMLFileStorage.toStringWithIndentation(getScene().toXML());
		//System.out.println(xml);
		TextFiles.saveString(scene.getPath()+File.separator+scene.getFileName(),xml,true);
		//run starlyx:
		if (scene.runStarlyx()==null) return false;
		String[][] array=TextFiles.readCSV(scene.getPath()+File.separator+"sensors.csv", true, "\t",false);
		if (array==null) return false;
		for (String[] l:array)
		{
			//for (String s:l) System.out.print(s+" ");System.out.println();
			if (l[0].compareTo(sensorTName+"_weight")==0) T=Double.parseDouble(l[1]);
		if (l[0].compareTo(sensorTName+"_sigma")==0) Tsigma=Double.parseDouble(l[1]);
		}
		return true;
	}

	

	public Scene getScene() 
	{
		return scene;
	}


	public double getE() {
		return e;
	}

	public void setE(double e) {
		this.e = e;
	}

	public double getL() {
		return l;
	}

	public void setL(double l) {
		this.l = l;
	}

	public double getN() {
		return n;
	}

	public void setN(double n) {
		this.n = n;
	}

	public double getK() {
		return k;
	}

	public void setK(double k) {
		this.k = k;
	}

	public double getKa() {
		return ka;
	}

	public void setKa(double ka) {
		this.ka = ka;
	}

	public double getG() {
		return g;
	}

	public void setG(double g) {
		this.g = g;
	}

	public double getT() {
		return T;
	}

	public double getTsigma() {
		return Tsigma;
	}

	public static void main(String[] args)
	{
		StarlyxModelSlab starlyxModelSlab =new StarlyxModelSlab();
		starlyxModelSlab.calc();
	}


	
}
