package com.photonlyx.jstarlyx.slab.gui;

import java.awt.Color;

import com.photonlyx.jstarlyx.slab.ExponentialIntegrals;
import com.photonlyx.jstarlyx.slab.StarlyxModelSlab;
import com.photonlyx.toolbox.chart.ChartPanel;
import com.photonlyx.toolbox.chart.Plot;
import com.photonlyx.toolbox.gui.WindowApp;
import com.photonlyx.toolbox.math.signal.Signal1D1DLogx;

/**
 * Validation of starlyx comparing with slab analytical solution
 * @author laurent
 *
 */
public class StarlyxValidationSlabApp extends WindowApp
{
	private ChartPanel cp=new ChartPanel();
	
	private double E=1;//slab thickness (mm)
	private double ka=0;//absorption coef (mm-1)
	private double l=100;//transverse length (mm)
	private double n=1;//refractive index
	private double emin=0.001;//min optical thickness
	private double emax=0.3;//max optical thickness
	private int nbpts=6;
	private int nbPhotons=10000;
	
	public StarlyxValidationSlabApp()
	{
		super("com_photonlyx_jstarlyx_slab",false,true,true);

		cp.hidePanelSide();
		cp.getChart().setLogx(true);
		cp.getChart().setFormatTickx("0.000");
		cp.getChart().setXlabel("Optical thickness");
		cp.getChart().setYlabel("T");
		cp.getChart().setTitle("");
		cp.getChart().setFontName("Nebraska");
		cp.getChart().setFontSize(16);
		cp.getSplitPane().setDividerSize(1);
		

		this.add(cp, "");
		
		cp.getChart().removeAllPlots();
		
		
		
		//analytical solution:
		Signal1D1DLogx T_over_e_analytical=new Signal1D1DLogx(emin,emax,nbpts);
		for (int i=0;i<nbpts;i++)
		{
			double e=T_over_e_analytical.x(i);//optical thickness
			double k=e/E;
			double alpha=k/(k+ka);
			T_over_e_analytical.setValue(i,Tanalytical(alpha,e));
		}

		int thick=7;
		
		Plot p1=new Plot(T_over_e_analytical,"T Hishimaru",Color.DARK_GRAY);
		p1.setDotStyle(Plot.SQUARE);
		p1.setDotSize(12);
		p1.setLineThickness(thick);
		cp.getChart().add(p1);
		
		
		//Bouguet Beer Lambert
		Signal1D1DLogx bl=new Signal1D1DLogx(emin,emax,nbpts);
		for (int i=0;i<bl.getDim();i++)
		{
			double e=bl.x(i);//optical thickness
			bl.setValue(i,Math.exp(-e));
		}

		Plot p2=new Plot(bl,"exp(-e) Bouguet-Beer-Lambert",Color.DARK_GRAY);
		p2.setDotStyle(Plot.TRIANGLE);
		p2.setDotSize(15);
		p2.setLineThickness(thick);
		cp.getChart().add(p2);
	

		
		
		thick=3;
		
		//get plot
		int max_nb_scat=-1;
		{
		Plot p=getPlotStarlyx(max_nb_scat);
		p.setColor(Color.RED);
		p.setLabel("Starlyx infinite scattering");
		p.setDotSize(1);
		p.setLineThickness(thick);
		cp.getChart().add(p);
		}
		
		{
		max_nb_scat=1;
		Plot p=getPlotStarlyx(max_nb_scat);
		p.setColor(Color.ORANGE);
		p.setLabel("Starlyx 1 scattering");
		p.setDotStyle(Plot.ROUND);
		p.setEmpty(true);
		p.setDotSize(1);
		p.setLineThickness(thick);
		cp.getChart().add(p);
		}
		
		{
		max_nb_scat=0;
		Plot p=getPlotStarlyx(max_nb_scat);
		p.setColor(Color.CYAN);
		p.setLabel("Starlyx 0 scattering");
		p.setDotStyle(Plot.TRIANGLE);
		p.setEmpty(true);
		p.setDotSize(1);
		p.setLineThickness(thick);
		cp.getChart().add(p);
		}

		
		

		cp.update();


	}
	
	
public  Plot getPlotStarlyx(int max_nb_scat)
{
	StarlyxModelSlab starlyxModelSlab =new StarlyxModelSlab();

	starlyxModelSlab.getScene().setNbPhotons(nbPhotons);
	starlyxModelSlab.getScene().setMax_nb_events(max_nb_scat);
	//starlyxModelSlab.getScene().setMax_path_length(-1);
	starlyxModelSlab.getScene().setVerbose(0);
	starlyxModelSlab.getScene().setNbThreads(6);
	
	Signal1D1DLogx T_over_e=new Signal1D1DLogx(emin,emax,nbpts);
	Signal1D1DLogx T_over_e_sigma=new Signal1D1DLogx(emin,emax,nbpts);
	for (int i=0;i<T_over_e.getDim();i++)
	{
		double e=T_over_e.x(i);//optical thickness
		starlyxModelSlab.setE(E);
		starlyxModelSlab.setL(l);
		starlyxModelSlab.setK(e/E);
		starlyxModelSlab.setKa(ka);
		starlyxModelSlab.setN(n);
		starlyxModelSlab.calc();		
		T_over_e.setValue(i, starlyxModelSlab.getT());
		T_over_e_sigma.setValue(i, starlyxModelSlab.getTsigma()*2);
	}

	Plot p=new Plot(T_over_e,"T starlyx",Color.BLACK);
	p.setDotStyle(Plot.CROSS);
	p.setDotSize(5);
	p.setError(T_over_e_sigma);
	return p;
}




	/**
	 * analytical solution of slab transmission
	 * @param alpha albedo=k/(k+ka)
	 * @param e optical thickness
	 * @return
	 */
	private double Tanalytical(double alpha, double e)
	{
		//gamma(0,e)=-Ei(-x):
		double gamma_e=-ExponentialIntegrals.Exponential_Integral_Ei(-e);
		double gamma=0.5772156649;// Euler Mascheroni constant
		double t=(1+alpha/2*(gamma+Math.log(e)))*Math.exp(-e)+alpha*(1-e)/2*gamma_e;
		return t;
	}

	public static void main(String[] args)
	{
		StarlyxValidationSlabApp a =new StarlyxValidationSlabApp();
	}


}
