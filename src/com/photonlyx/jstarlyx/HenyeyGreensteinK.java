package com.photonlyx.jstarlyx;

import com.photonlyx.toolbox.io.XMLstorable;

import nanoxml.XMLElement;

/**
 * scatterer defined by Henyey-Greenstein phase function and scat coef K in mm-1
 * @author laurent
 *
 */
public class HenyeyGreensteinK extends Node implements XMLstorable
{
	private String ks="";//name of spectrum of K
	private String ka="";//name of spectrum of KA
	private String g="";//name of spectrum of G

	public HenyeyGreensteinK()
	{
	}
	public HenyeyGreensteinK(Node father)
	{
		super(father);
	}

	public HenyeyGreensteinK(String name)
	{
		this.setName(name);
	}


	public String getKs() {
		return ks;
	}


	public void setKs(String s) {
		this.ks = s;
	}


	public String getKa() {
		return ka;
	}


	public void setKa(String ka) {
		this.ka = ka;
	}


	public String getG() {
		return g;
	}


	public void setG(String g) {
		this.g = g;
	}




	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("Henyey-Greenstein");
		el.setAttribute("name", this.getName());
		el.setAttribute("ks", ks);
		el.setAttribute("ka", ka);
		el.setAttribute("g", g);
		return el;
	}


	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}

		this.setName(xml.getStringAttribute("name",""));
		this.setKs(xml.getStringAttribute("ks",""));
		this.setKa(xml.getStringAttribute("ka",""));
		this.setG(xml.getStringAttribute("g",""));
	}






}
