package com.photonlyx.jstarlyx;

import java.util.Enumeration;

import com.photonlyx.toolbox.io.XMLstorable;
import com.photonlyx.toolbox.math.geometry.Object3DSet;
import com.photonlyx.toolbox.math.geometry.Segment3D;
import com.photonlyx.toolbox.math.geometry.Vecteur;
import com.photonlyx.toolbox.threeD.gui.Object3DColorSet;
import com.photonlyx.toolbox.threeD.gui.Segment3DColor;

import nanoxml.XMLElement;

public class SourceSun extends Node implements XMLstorable
{
	private Vecteur dir=new Vecteur(0.1,0.1,-1);
	private double radiance=1.5e7;
	private double angle=0;//degrees
	private String type="sun";
	private String spectrum="";
	private String volume="World";

	public SourceSun()
	{
		this.setName("sourceSun0");
	}
	public SourceSun(Node father)
	{
		super(father);
		this.setName("sourceSun0");
	}


	@Override
	public Object3DColorSet getObject3D()
	{
		Object3DColorSet set=new Object3DColorSet();
		set.add(new Segment3DColor(new Vecteur(),dir.scmul(2)));
		return set;
	}



	public XMLElement toXML()
	{
		XMLElement el = new XMLElement();
		el.setName("source");

		el.setAttribute("NAME", this.getName());
		el.setAttribute("SPECTRUM", spectrum);
		el.setDoubleAttribute("RADIANCE", radiance);
		el.setAttribute("TYPE", type);
		el.setAttribute("VOLUME", volume);
		el.setAttribute("ANGLE", angle);

		//add dir:
		XMLElement elDir=dir.toXML();
		elDir.setName("dir");
		el.addChild(elDir);

		return el;
	}




	public void updateFromXML(XMLElement xml)
	{
		if (xml==null) 
		{
			return;
		}
		this.setName(xml.getStringAttribute("name",""));
		radiance=xml.getDoubleAttribute("RADIANCE",1.5e7);
		type=xml.getStringAttribute("TYPE","sun");
		spectrum=xml.getStringAttribute("SPECTRUM","");
		volume=xml.getStringAttribute("VOLUME","World");
		angle=xml.getDoubleAttribute("ANGLE",0);

		Enumeration<XMLElement> enumSons = xml.enumerateChildren();
		while (enumSons.hasMoreElements())
		{
			XMLElement el=enumSons.nextElement();
			if (el.getName().contains("dir"))
			{
				dir.updateFromXML(el);
			}

		}

	}

	public double getX() {
		return dir.x();
	}

	public void setX(double x) {
		dir.setX( x);
	}

	public double getY() {
		return dir.y();
	}

	public void setY(double y) {
		dir.setY (y);
	}

	public double getZ() {
		return dir.z();
	}

	public void setZ(double z) {
		dir.setZ (z);
	}
	public double getPower() {
		return radiance;
	}
	public void setPower(double d) {
		this.radiance = d;
	}
	public String getSpectrum() {
		return spectrum;
	}
	public void setSpectrum(String spectrum) {
		this.spectrum = spectrum;
	}
	public void setDir(Vecteur dir) {
		this.dir = dir;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}




}
