package com.photonlyx.jstarlyx;

import java.awt.Color;
import java.util.Vector;

import com.photonlyx.toolbox.math.geometry.PolyLine3D;
import com.photonlyx.toolbox.threeD.gui.PolyLine3DColor;

public class Path extends Vector<Interaction>
{

	public 	PolyLine3DColor getView()
	{

		return getView(0,true,Color.BLACK);
	}

	public 	PolyLine3DColor getView(int dotSize,boolean seeSegments,Color color)
	{
		PolyLine3D p=new PolyLine3D();
		p.setDotSize(dotSize);
		p.setSeeSegments(seeSegments);
		for (Interaction in:this)
		{
			p.addPoint(in.getPos());
		}
		PolyLine3DColor pc=new PolyLine3DColor(p,color);
		return pc;
	}

	/**
	 * get the final weight of the path
	 * @return
	 */
	public float getWeight() 
	{
		return getLastInteraction().getW();
	}

	public Interaction getLastInteraction() 
	{
		return this.elementAt(this.size()-1);
	}


	public String toString()
	{
		StringBuffer sb=new StringBuffer();
		for (Interaction in:this)
		{
			sb.append(in.getType()+": "+in.getPos().toString()+" ");
		}
		return sb.toString();
	}
}
